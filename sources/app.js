import "./styles/app.css";
import {JetApp} from "webix-jet";
import AuthService from "./services/auth";
import {createState} from "jet-restate";

export default class MRPApp extends JetApp {
	constructor(config) {
		const state = createState({
			selected_requisicion_material: 0,
			selected_requisicion_material_detalle: 0,
			selected_demanda_calculada: 0,
			selected_demanda_calculada_detalle: 0,
			selected_demanda_manual: 0,
			selected_articulo: 0,
			selected_articulo_detalle: 0,
			selected_analisis_articulo: 0,
			selected_orden_compra_detalle: 0,
			selected_conversion: 0,
			selected_lead_time: 0,
			selected_control_fecha: 0,
			selected_proveedor_inter: 0,
			selected_admin_partida: 0,
			selected_corrida_mrp: 0,

			filters_articulo: {},
			filters_analisis_articulo: {},
			filters_demandas_manuales: {},
			filters_demandas_calculadas: {},
			filters_demandas_calculadas_detalle: {},
			filters_requisiciones_material: {},
			filters_control_fechas: {},
			filters_parametros: {},
			filters_proveedor_inter: {},
			filters_admin_partidas: {},
			filters_corrida_mrp: {},
			filters_corrida_mrp_detalle: {},

			selected_tab_articulos: "",
			selected_tab_demandas_calculadas: "",

			selected_analisis_articulo_text: ""
		});

		let theme = "teacher_dashboard";
		try{
			theme = webix.storage.local.get("theme_students_dashboard");
		}
		catch(err){
			webix.message("You blocked cookies. The theme won't be restored after page reloads.","debug");
		}

		super(
			webix.extend(
				{
					id: "BEMI",
					version: 1.0,
					start: "/home",
					debug: !PRODUCTION,
					theme: theme || "",
					state
				},
				config,
				true
			)
		);
		this.Mobile = !!(webix.env.mobile || screen.width < 1366);
		this.Size = webix.env.mobile ? {databar: -1} : {databar: 265};
		this.MasterUrl = MASTER_URL;
		// this.MasterUrl = "/";

		/* error tracking */
		this.attachEvent("app:error:resolve", function (name, error) {
			console.log(error);
		});
	}

	Init(node) {
		const rights = {
			articulos: "articulos",
			demandas_calculadas: "demandas_calculadas",
			demandas_manuales: "demandas_manuales",
			analisis_articulos: "analisis_articulos",
			requisiciones_material: "requisiciones_material",
			admin_partidas: "admin_partidas",
			reportes: "reportes",
			parametros: "parametros",
			corrida_mrp: "corrida_mrp",
			file_export: "file_export"
		};
		this.setService("auth", new AuthService(rights, "", this));
		this.render(node);
	}
}

export {MRPApp};