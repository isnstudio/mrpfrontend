webix.protoUI({
	name: "progress_bar",
	$cssName: "template",
	$init: function (config) {
		config.id = "progress";
	},
	defaults: {
		height: 40,
		template:function(obj){
			var html = "<div class='progress_bar_element'>";
			html += "<div title='"+(parseInt(obj.progress*100)+"%")+"' class='progress_result ' style='width:"+(obj.progress*100+"%")+"'></div>";
			html += "</div>";
			return html;
		}
	}
}, webix.ui.template);
