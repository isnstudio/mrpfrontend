webix.protoUI({
	name: "textarea_field",
	$cssName: "textarea",
	$init: function (config) {
		if (!config.name) config.name = config.id;
		if (!config.placeholder) config.placeholder = config.label;
		if (!config.height) config.height = 200;
	},
	defaults: {
		labelPosition: "top",
		invalidMessage: "Requerido",
	}
}, webix.ui.textarea);