webix.protoUI({
	name: "multi_server_autocomplete",
	$cssName: "multiselect",
	$init: function (config) {
		config.height = 60;
		if (typeof (config.url) === "undefined") {
			alert("A multi server autocomplete component was defined without the url property");
			return;
		}
		if (typeof (config.id) === "undefined") {
			alert("A multi server autocomplete component was defined without the id property");
			return;
		}
		if (typeof (config.name) === "undefined") {
			alert("A multi server autocomplete component was defined without the name property");
			return;
		}
		this.attachEvent("onChange", function (newValue, oldValue, source) {
			if (newValue !== "") {
				const element_id = this.data.id;
				const element_name = this.data.name;
				const options = this.getList();
				const found = options.find(function (obj) {
					return obj.id === newValue;
				});
				if (found.length === 0 && typeof newValue !== "object") {
					var values;
					if(this.$scope._parent.$$("form"))
						values = this.$scope._parent.$$("form").getValues();
					else values = this.$scope.$$("filter_form").getValues();
					if(values[element_name + "__display_webix"]) {
						var var_repr = {id: values[element_name], value: values[element_name + "__display_webix"]};
						$$(element_id).setValue(var_repr);
					} else {
						// eslint-disable-next-line no-console
						console.log("No existe display webix en " + element_name);
					}
				}
			}
		});
	},
	url_setter: function (value) {
		var sugg = {
			template(obj, type) {
				return obj.value;
			},
			body: {
				url: value,
				dataFeed: function (svalue) {
					this.clearAll();
					let url = value;
					url += svalue ? "?param=" + svalue : "";
					this.load(url);
				},
				template(obj, type) {
					return obj.value;
				}
			}
		};
		$$(this.data.id).define("suggest", sugg);
		$$(this.data.id).refresh();
		return value;
	},
	defaults: {
		labelPosition: "top",
		height: 60
	}
}, webix.ui.multicombo);