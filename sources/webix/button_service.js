import SaveService from "../services/save";
import RemoveService from "../services/remove";

webix.protoUI({
	name: "button_service",
	$cssName: "button",
	$init: function (config) {
		if (typeof (config.url) === "undefined") {
			alert("A button service component was defined without the url property");
		}
		if (typeof (config.message) === "undefined") {
			alert("A button service component was defined without the message property");
		}
		if (typeof (config.method) === "undefined") {
			alert("A button service component was defined without the method property");
		}
		config.id = "btn_save";
		if(!config.css)
			config.css = config.method === "delete" ? "webix_danger" : "webix_primary";
		config.height = 40;
		if (!config.label) config.label = "Guardar";
		if (!config.new_values) config.new_values = {};
		if (config.show_detail === undefined) config.show_detail = true;
		config.click = function () {
			if ("btn_save_extra_values" in this.$scope) config.new_values = this.$scope.btn_save_extra_values;
			config.context = this._parent_cell.$scope;
			config.form = config.context.$$("form");
			config.validate = config.form.validate();
			request(config);
		};
	},
}, webix.ui.button);

function request(config) {
	const keys = Object.keys(config.new_values);
	var get_values = config.form.getValues();
	keys.forEach(k => get_values[k] = config.new_values[k]);

	Object.keys(get_values).forEach(k => {
		if(config.form.elements[k])
			if(config.form.elements[k].config.view === "multicombo")
				get_values[k] = config.form.elements[k].config.value;
		if (get_values[k] === "") delete get_values[k];
	});
	const request_options = {
		url: config.url,
		get_values: get_values,
		validate: config.validate,
		message: config.message,
		path: config.path,
		button: config.context.$$("btn_save"),
		context: config.context,
		method: config.method,
		show_detail: config.show_detail
	};
	if(config.method !== "delete") {
		config.context.SaveService = new SaveService(config.context.app);
		config.context.SaveService.save(request_options);
	} else {
		config.context.RemoveService = new RemoveService(config.context.app);
		config.context.RemoveService.remove(request_options);
	}
}