webix.protoUI({
	name: "breadcrumb",
	$cssName: "list",
	$init: function (config) {
		config.id = "crumbs";
		this.attachEvent("onItemClick", function(id){
			const link = this.getItem(id).link;
			if (link) this.$scope.show(link);
		});
	},
	defaults: {
		layout: "x",
		css: "crumbs",
		scroll: false,
		type: {
			width: "auto",
			template: "<span class='action'>#value#</span>"
		}
	}
}, webix.ui.list);