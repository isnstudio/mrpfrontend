webix.protoUI({
	name: "datepicker_iso",
	$cssName: "datepicker",
	$init: function (config) {
		if (typeof (config.name) === "undefined") {
			alert("A datepicker component was defined without the name property");
			return;
		}
		if (config.id.startsWith("$")) config.id = "input_" + config.name;
		if (!config.label) config.label = "";
		if (!config.placeholder) config.placeholder = config.label;
		if (!config.invalidMessage) config.invalidMessage = "Requerido";
		config.labelPosition = "top";
		config.height = 60;
		config.type = "calendar";
		config.stringResult = true;
		if (config.timepicker === true) {
			webix.i18n.parseFormat = "%Y-%m-%d %H:%i";
			webix.i18n.setLocale();
			config.format = "%Y-%m-%d %H:%i";
		} else {
			webix.i18n.parseFormat = "%Y-%m-%d";
			webix.i18n.setLocale();
			config.format = "%Y-%m-%d";
		}
	},
}, webix.ui.datepicker);