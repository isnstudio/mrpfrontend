webix.protoUI({
	name: "combo_field",
	$cssName: "text",
	$init: function (config) {
		if (typeof (config.name) === "undefined") {
			alert("A combo component was defined without the name property");
			return;
		}
		if (config.id.startsWith("$")) config.id = "input_" + config.name;
		if (!config.label) config.label = "-";
		if (!config.placeholder) config.placeholder = "Clic para seleccionar";
		if (!config.invalidMessage) config.invalidMessage = "Requerido";
		config.labelPosition = "top";
		config.height = 60;
	}
}, webix.ui.combo);