webix.protoUI({
	name: "datefield",
	$cssName: "datepicker",
	$init: function (config) {
		config.name = config.id;
		config.placeholder = config.label;
		config.height = 60;
	},
	defaults: {
		labelPosition: "top",
		invalidMessage: "Requerido",
		format: "%Y-%m-%d",
	}
}, webix.ui.datepicker);