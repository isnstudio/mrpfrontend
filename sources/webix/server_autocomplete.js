import Errors from "../helpers/errors";

webix.protoUI({
	name: "server_autocomplete",
	$cssName: "combo",
	$init: function (config) {
		if (typeof (config.name) === "undefined") {
			alert("A server autocomplete component was defined without the name property");
			return;
		}
		if (typeof (config.url) === "undefined") {
			alert("A server autocomplete component was defined without the url property");
			return;
		}
		if (config.id.startsWith("$")) config.id = "input_" + config.name;
		if (!config.placeholder) config.placeholder = "Clic para seleccionar";
		if (!config.invalidMessage) config.invalidMessage = "Requerido";
		config.labelPosition = "top";
		config.height = 60;
		this.attachEvent("onChange", function (newValue, oldValue, source) {
			if (newValue !== "") {
				const element_id = this.data.id;
				const element_name = this.data.name;
				const options = this.getList();
				const found = options.find(function (obj) {
					return obj.id == newValue;
				});
				if (found.length === 0 && typeof newValue !== "object") {
					var values;
					if(this.$scope._parent.$$("form"))
						values = this.$scope._parent.$$("form").getValues();
					else values = this.$scope.$$("filter_form").getValues();
					if(values[element_name + "__display_webix"]) {
						var var_repr = {id: values[element_name], value: values[element_name + "__display_webix"]};
						$$(element_id).setValue(var_repr);
					} else {
						// eslint-disable-next-line no-console
						console.log("No existe display webix en " + element_name);
					}
				}
			}
		});
	},
	url_setter: function (value) {
		var sugg = {
			template(obj) {
				return obj.value;
			},
			keyPressTimeout: 500,
			body: {
				url: value,
				dataFeed: function (svalue) {
					this.clearAll();
					let url = value;
					if (url.includes("/?")) {
						url += svalue ? "&param=" + svalue : "";
					} else {
						url += svalue ? "?param=" + svalue : "";
					}
					this.load(url);
				},
				template(obj) {
					return obj.value;
				},
				on: {
					onLoadError(xhr) {
						const error = new Errors();
						error.show_error(xhr);
					}
				}
			}
		};
		this.define("suggest", sugg);
		this.refresh();
		return value;
	}
}, webix.ui.combo);