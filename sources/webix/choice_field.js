webix.protoUI({
	name: "choice_field",
	$cssName: "combo",
	$init: function (config) {
		if (!config.name) config.name = config.id;
		config.height = 60;
	},
	defaults: {
		labelPosition: "top",
		invalidMessage: "Requerido",
		placeholder: "Clic para seleccionar"
	}
}, webix.ui.combo);