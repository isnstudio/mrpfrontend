webix.protoUI({
	name: "text_field",
	$cssName: "text",
	$init: function (config) {
		if (typeof (config.name) === "undefined") {
			alert("A text component was defined without the name property");
			return;
		}
		if (config.id.startsWith("$")) config.id = "input_" + config.name;
		if (!config.label) config.label = "";
		if (!config.placeholder) config.placeholder = config.label;
		if (!config.invalidMessage) config.invalidMessage = "Requerido";
		if(!config.labelPosition)
			config.labelPosition = "top";
		config.height = 60;
	},
}, webix.ui.text);