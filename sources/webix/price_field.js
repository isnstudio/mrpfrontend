webix.protoUI({
	name: "price_field",
	$cssName: "text",
	$init: function (config) {
		if (typeof (config.name) === "undefined") {
			alert("A decimal field component was defined without the name property");
			return;
		}
		if (config.id.startsWith("$")) config.id = "input_" + config.name;
		config.placeholder = config.label;
		config.height = 60;
		config.value = 0;
	},
	defaults: {
		labelPosition: "top",
		format: "$1,111.00",
		invalidMessage: "Requerido",
	}
}, webix.ui.text);