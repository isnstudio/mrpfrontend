webix.protoUI({
	name: "number_field",
	$cssName: "text",
	$init: function (config) {
		if (typeof (config.name) === "undefined") {
			alert("A number field component was defined without the name property");
			return;
		}
		if (config.id.startsWith("$")) config.id = "input_" + config.name;
		config.placeholder = config.label;
		config.height = 60;
		if(!config.value)
			config.value = 0;
		this.attachEvent("onChange", function (newValue) {
			if (newValue === "" || newValue === null) this.setValue(0);
		});
	},
	defaults: {
		labelPosition: "top",
		format: "111",
		invalidMessage: "Requerido",
	}
}, webix.ui.text);