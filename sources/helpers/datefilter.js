function getDate(value) {
	if (!value) return "";
	let date = null;
	let parts = value.match(/[0-9]+/g);
	if (!parts || !parts.length) return "";
	if (parts.length < 3) {
		parts.reverse();
		date = new Date(parts[0], (parts[1] || 1) - 1, 1);
	} else {
		date = webix.i18n.dateFormatDate(value.replace(/^[>< =]+/, ""));
	}
	return date;
}

export function filterDate(value, date) {
	const input = getDate(value);
	if (!input) {
		return false;
	}
	let operator = value.match(/[><]+/g);
	operator = operator ? operator[0] : null;
	if (!operator) {
		let parts = value.match(/[0-9]+/g);

		if (parts.length < 3) {
			return date.getYear() === input.getYear() && (parts[1] ? (date.getMonth() === input.getMonth()) : true);
		} else {
			return webix.Date.equal(webix.Date.datePart(input), webix.Date.datePart(date));
		}
	} else {
		switch (operator) {
			case "<":
				return date < input;
			case ">":
				return date > input;
		}
	}
}