import {JetView} from "webix-jet";
import {getFilterState, setFilterState} from "./state";

export default class TableHelper extends JetView {

	dataLoading(table, url, view, filters, selected, filter_name,  filter_default, default_count=20) {
		let view_filter= getFilterState(view, filter_name);
		let count = view_filter["count"] ? view_filter["count"] : view.getParam("count");
		let start;
		if("start" in view_filter)
			start = view_filter["start"];
		else start = view.getParam("start");

		let page;
		if("page" in view_filter)
			page = view_filter["page"];
		else page = view.getParam("page");

		const querystring = view_filter["querystring"];
		filter_default = filter_default ? filter_default : "";
		// Data loading
		const data_url = new URL(url);

		if (start) {
			data_url.searchParams.append("start", start);
		} else {
			data_url.searchParams.append("start", 0);
		}
		if (count) {
			data_url.searchParams.append("count", count);
		} else {
			data_url.searchParams.append("count", default_count);
		}
		if (querystring) {
			data_url.searchParams.append("filter", querystring);
			if(filters)
				filters.$$("cleanfilter").show();
		} else {
			if(filters)
				filters.$$("cleanfilter").hide();
			data_url.searchParams.append("filter", filter_default);
		}

		if(!this.app.Mobile)
			table.showProgress({type:"icon"});

		var xhr = webix.ajax().sync().get(data_url.href);
		const json_response = JSON.parse(xhr.response);
		let pager;
		if(view.Table)
			pager = view.Table.$$("pager");

		if(page)
			if(pager)
				pager.define({page: page});

		table.parse(json_response);
		if (selected) {
			if (table.exists(selected)) {
				table.select(selected);
			}
		}
	}

	hideContents(datatable, details, selected_id) {
		if (selected_id) {
			if (datatable.exists(selected_id)) {
				datatable.select(selected_id);
			} else {
				details.forEach((detail) => {
					detail.hideContents();
				});
			}
		} else {
			datatable.unselect();
			details.forEach((detail) => {
				detail.hideContents();
			});
		}
	}

	pagerParams(view, datatable, filter_name, that) {
		setTimeout(function () {
			let pager = datatable.getPager();
			let view_filter = getFilterState(that, filter_name);
			pager.config.page = view_filter["page"] || 0;
			pager.attachEvent("onBeforePageChange", function (new_page) {
				let start = new_page * pager.data.size;
				let count = pager.data.size;
				let params = getFilterState(that, filter_name);
				params["start"] = start;
				params["count"] = count;
				params["page"] = new_page;
				setFilterState(that, filter_name, params);
				view.$scope.setParam("start", start, true);
				view.$scope.setParam("count", count, true);
				view.$scope.setParam("page", new_page, true);

			});
		}, 500);
	}

	afterRender(view, message_filters) {
		const message = message_filters ? "<p>No hay información para mostrar, verifique los filtros</p>"
			: "<p>No hay información para mostrar<p/>";
		const that = view;
		if (!that.count()) {
			that.showOverlay(message);
		} else {
			that.hideOverlay();
		}
		setTimeout(function () {
			that.enable();
		}, 1000);
	}

	afterSelect(details, header, headers_collapse, id) {
		details.forEach((detail) => {
			detail.showDetail(id);
		});
		if (!this.app.Mobile) {
			header.expand();
			if(headers_collapse)
				headers_collapse.forEach((header) => header.collapse());
		}
	}
}