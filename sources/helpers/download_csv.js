import Errors from "./errors.js";

export default class Download_csv {

	download_csv(url, get_values, button, context, validate, name) {
		if (validate) {
			get_values = get_values ? get_values : {};
			if(button) {
				webix.extend(context.$$("save"), webix.ProgressBar);
				button.disable();
			}
			webix.ajax().headers({
				"Content-type":"application/json"
			}).get(context.app.MasterUrl + url, get_values).then(function (res) {
				webix.message("Descargando...");
				var csv = (res.text());

				var downloadLink = document.createElement("a");
				var blob = new Blob(["\ufeff", csv]);
				var url = URL.createObjectURL(blob);
				downloadLink.href = url;
				downloadLink.download = name + ".csv";

				document.body.appendChild(downloadLink);
				downloadLink.click();
				document.body.removeChild(downloadLink);
			}).fail(function (res) {
				const error = new Errors();
				error.show_error(res);
			}).finally(function () {
				if(button) {
					button.enable();
				}
				return true;
			});
		} else {
			webix.message({
				text: "Faltan campos por llenar",
				type: "error",
				expire: -1,
				id: "messageLead"
			});
		}
	}

	download_file(url, get_values, context, validate, name, format) {
		if (validate) {
			window.open(url);
		} else {
			webix.message({
				text: "Faltan campos por llenar",
				type: "error",
				expire: -1,
				id: "messageLead"
			});
		}
	}
}