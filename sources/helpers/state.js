export function getFilterState(context, filter_name) {
	let copy = {};
	Object.assign(copy, context.app.config.state[filter_name]);
	return copy;
}

export function setFilterState(context, filter_name, new_values) {
	context.app.config.state[filter_name] = new_values;
}
