export default class Errors {

	show_error(error) {
		if (error.status === 400) {
			var data = JSON.parse(error.response);
			if ("non_field_errors" in data) {
				for (const error of data["non_field_errors"]) {
					webix.message({
						text: error,
						type: "error",
						expire: -1,
					});
				}
			} else {
				Object.keys(data).forEach(element => {
					if(element.code === 403)
						webix.message({
							text: element.body,
							type: "error",
							expire: -1,
						});
					else data[element].forEach(error => {
						const message = element.replaceAll("_", " ") + ": " + error;
						webix.message({
							text: message,
							type: "error",
							expire: -1,
						});
					});
				});
			}
		}

		if (error.status === 405) {
			const response = JSON.parse(error.response);
			webix.message({
				text: response.detail,
				type: "error",
				expire: -1,
			});
		}

		if (error.status === 404) {
			webix.message({
				text: "Uno de los datos no se encuentra en la base de datos.",
				type: "error",
				expire: -1,
			});
		}
		if (error.status === 403) {
			webix.message({
				text: "No estas autorizado a realizar esta acción.",
				type: "error",
				expire: -1,
			});
		}
		if (error.status === 500) {
			webix.message({
				text: "ha ocurrido un error interno del servidor.",
				type: "error",
				expire: -1,
			});
		}
	}
}