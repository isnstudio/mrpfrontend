import {JetView} from "webix-jet";
import {getFilterState, setFilterState} from "./state";

export default class Filters extends JetView {

	filterParams(context, filter_name, context_table) {
		let filter_state = getFilterState(context, filter_name);
		let querystring = "";
		const values = context.$$("filterform").getValues();
		values.querystring = "";
		if (Object.keys(values).length !== 0) {
			for (let k in values) {
				if (values[k] || values[k] === 0) {
					filter_state[k] = values[k];
					querystring += k + "==" + values[k] + ";";
				}
			}
		}

		let table = context_table ? context_table : context._parent.Table.$$("data");
		let current_querystring = context._parent.getParam("filter");
		if (querystring !== current_querystring) {
			table.clearAll();
		}

		filter_state["querystring"] = querystring;
		filter_state["page"] = 0;
		filter_state["start"] = 0;
		filter_state["count"] = 0;
		setFilterState(context, filter_name, filter_state);
		context._parent.setParam("page", 0, false);
		context._parent.setParam("start", 0, false);
	}

	cleanFilter(context, filter_name) {
		setFilterState(context, filter_name, {});
		context._parent.Table.$$("data").clearAll();
		context._parent.setParam("page", 0, false);
		context._parent.setParam("start", 0, false);
		context.$$("filter_form").clear();
	}
}