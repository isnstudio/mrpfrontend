function findItem(collection,input){
	return collection.find(obj => obj.name.toLowerCase().indexOf(input) !== -1);
}

export function collectionFilter(collection, input, objprop){
	const items = findItem(collection, input);
	let result = false;

	for (let i = 0; i < items.length; i++){
		if (objprop == items[i].id){
			result = true;
			break;
		}
		else {
			result = false;
		}
	}
	return result;
}
