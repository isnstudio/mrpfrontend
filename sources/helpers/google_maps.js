export function addShape(shape, map) {
	let options = {
		strokeColor: "#0044f5",
		strokeOpacity: 1,
		strokeWeight: 4,
		fillColor: "#4A4E58",
		fillOpacity: .8
	};
	let google_shape = null;
	if (shape.type === "polygon") {
		options["paths"] = shape.coordinates;
		google_shape = new google.maps.Polygon(options);
	}
	if (shape.type === "polyline") {
		options["path"] = shape.coordinates;
		google_shape = new google.maps.Polyline(options);
	}
	if (google_shape) google_shape.setMap(map);
	return google_shape;
}

export function measureShape(shape, aft2f, am2f, pmf, pftf, amf, aft) {
	const altura_metros = amf.getValue();
	const altura_pies = aft.getValue();
	const pm = google.maps.geometry.spherical.computeLength(shape.google_shape.getPath());
	const pft = metersToFeet(pm);

	let am2 = 0;
	let ft2 = 0;
	if (shape.type === "polygon") {
		am2 = google.maps.geometry.spherical.computeArea(shape.google_shape.getPath());
		ft2 = metersToFeet(am2);
	}
	if (shape.type === "polyline") {
		am2 = (altura_metros * pm).toFixed(2);
		ft2 = (altura_pies * pft).toFixed(2);
	}
	am2f.setValue(am2);
	aft2f.setValue(ft2);
	pmf.setValue(pm);
	pftf.setValue(pft);
}

export function measureShapeDeficiency(shape, m2f, ft2f, mf, ftf) {
	if (shape.type === "polygon") {
		let m2 = google.maps.geometry.spherical.computeArea(shape.google_shape.getPath());
		let ft2 = metersToFeet(m2);
		m2f.setValue(m2);
		ft2f.setValue(ft2);
	}
	if (shape.type === "polyline") {
		const m = google.maps.geometry.spherical.computeLength(shape.google_shape.getPath());
		const ft = metersToFeet(m);
		mf.setValue(m);
		ftf.setValue(ft);
	}
}

export function metersToFeet(meters) {
	return parseFloat((meters * 3.280839895).toFixed(2));
}

export function feetToMeter(feet) {
	return parseFloat((feet / 3.280839895).toFixed(2));
}

export function resetStrokes(shapes) {
	shapes.forEach(function (shape, index) {
		shape.polygon.setOptions({
			strokeColor: "#0044f5",
			zIndex: 1
		});
	});
}


export function set_bounds(coordinates, map) {
	const bounds = new google.maps.LatLngBounds();
	coordinates.forEach(function (coord, index) {
		bounds.extend(coord);
	});
	// map.setCenter(bounds.getCenter());
	map.fitBounds(bounds);
}

export function startSetMarker(map, marker, location, drag, text) {
	if (marker) {
		marker.setPosition(location);
	} else {
		let draggable = false;
		let label = "";
		if (text !== undefined) label = text;
		marker = new google.maps.Marker({
			position: location,
			map: map,
			draggable: draggable,
			label: label
		});
	}
	return marker;
}

export function startSetMarkerGeolocated(map, marker, location, geocoder, direccion_input, drag) {
	if (marker) {
		marker.setPosition(location);
	} else {
		let draggable = false;
		if (drag === true) draggable = drag;
		marker = new google.maps.Marker({
			position: location,
			map: map,
			draggable: draggable
		});
		if (drag) {
			marker.addListener("dragend", (e) => {
				geocodePosition(geocoder, e.latLng, direccion_input);
			});
		}
	}
	if (geocoder) {
		geocodePosition(geocoder, location, direccion_input);
	}
	return marker;
}

export function geocodePosition(geocoder, location, direccion_input) {
	geocoder.geocode({
		latLng: location
	}, function (responses) {
		if (responses && responses.length > 0) {
			const response = responses[0];
			direccion_input.setValue(response.formatted_address);
		} else {
			webix.message("No se pudo determinar la locación");
		}
	});
}

export function getCoordinates(google_shape, shape_type) {
	let coordinates = null;
	if (shape_type === "polygon" || shape_type === "polyline") {
		coordinates = [];
		for (let idx = 0; idx < google_shape.getPath().getLength(); idx++) {
			const {lat, lng} = google_shape.getPath().getAt(idx);
			coordinates.push({
				lat: lat(),
				lng: lng()
			});
		}
		coordinates = JSON.stringify(coordinates);
	}
	if (shape_type === "marker"){
		coordinates = {
			"lat": google_shape.position.lat(),
			"lng": google_shape.position.lng(),
		};
		coordinates = JSON.stringify(coordinates);
	}
	return coordinates;
}

export function set_drawingManager(drawingManager, drawingModes, map) {
	if (!drawingManager){
		let options = {
			strokeColor: "#0044f5",
			strokeOpacity: 1,
			strokeWeight: 4,
			fillColor: "#4A4E58",
			fillOpacity: .8,
			editable: true,
		};
		drawingManager = new google.maps.drawing.DrawingManager({
			drawingMode: null,
			drawingControl: true,
			drawingControlOptions: {
				position: google.maps.ControlPosition.TOP_CENTER,
				drawingModes: drawingModes,
			},
			polylineOptions: options,
			polygonOptions: options,
		});
		drawingManager.setMap(map);
	}
	return drawingManager;
}

export function set_drawingManagerMode(drawingManager, modes){
	drawingManager.setDrawingMode(null);
	drawingManager.setOptions({
		drawingControlOptions: {
			position: google.maps.ControlPosition.TOP_CENTER,
			drawingModes: modes
		}
	});
}