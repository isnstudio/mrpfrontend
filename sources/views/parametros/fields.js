import {JetView,} from "webix-jet";
import "webix/text_field";
import "webix/server_autocomplete";

export default class ProveedorInterFieldsView extends JetView {
	config() {
		const url_instance = {
			view: "text_field",
			name: "url_instance",
			id: "input_url_instance",
			label: "url de instancia",
			required: true
		};

		const proveedor = {
			view: "server_autocomplete",
			name: "proveedor",
			label: "Proveedor",
			url: this.app.MasterUrl + "api/cxp/proveedor/select_list/",
			required: true
		};


		const responsive_view = {
			margin: 10,
			rows: [
				proveedor,
				url_instance
			]
		};

		const standard_view = {
			margin: 10,
			rows: [
				{
					cols: [
						{
							margin: 10,
							rows: [
								proveedor,
							]
						},
						{
							margin: 10,
							rows: [
								url_instance
							]
						}
					]
				},
			],
		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);
		return {
			margin: 10,
			rows: [
				main_section,
			]
		};
	}
}