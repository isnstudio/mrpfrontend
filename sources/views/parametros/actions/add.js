import {JetView} from "webix-jet";
import ProveedorInterFieldsView from "../fields";
import "webix/breadcrumb";
import "webix/button_service";

export default class  ProveedorInterAddView extends JetView {
	config() {
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Proveedores intercompañia", link: "parametros"},
				{value: "Agregar"},
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button_service",
					label: "Guardar",
					height: 40,
					path: "/home/parametros/",
					method: "post",
					message: "Proveedor intercompañia agregado",
					url:  this.app.MasterUrl + "api/mrp/proveedor-intercompania/",
				},
				{
					view: "button",
					value: "Regresar",
					css: "webix_secondary",
					height: 40,
					click: () => {
						this.show("parametros");
					}
				}
			]
		};

		const fields = ProveedorInterFieldsView;

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 24,
			rows: [
				fields,
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					form,
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}
}
