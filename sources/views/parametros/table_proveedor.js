import {JetView} from "webix-jet";
import Table from "../../helpers/table";
import SaveService from "../../services/save";

export default class ProveedorInterTableView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		this.SaveService = new SaveService(this.app);
		const datatable = {
			view: "datatable",
			id: "data_proveedor",
			select: true,
			pager: "pager_proveedor",
			scroll: "xy",
			editable: true,
			rowHeight: 40,
			columns: [
				{
					id: "id",
					header: "Folio",
					minWidth: 80,
					fillspace: true,
				},
				{
					id: "proveedor__display_webix",
					header: "Proveedor",
					adjust: true,
				},
				{
					id: "url_instance",
					header: "Url instancia",
					adjust: true,
				},
				{
					id: "action1",
					header: "",
					width: 150,
					template: function () {
						return "<div class=\"webix_view webix_control webix_el_button larger\">" +
							"<div class=\"webix_el_box\">" +
							"<button type=\"button\" class=\"webix_button edit_btn\">Editar</button>" +
							"</div>" +
							"</div>";
					}
				}
			],
			onClick: {
				edit_btn: (ev, id) => {
					this.show("/home/parametros.actions.edit/" + id.row);
				}
			},
		};


		return {
			rows: [
				{
					view: "button",
					css: "webix_default",
					height: 40,
					label: "Agregar proveedor intercompañia",
					type: "icon",
					icon: "mdi mdi-plus",
					click: () => this.show("parametros.actions.add")
				},
				datatable,
				{
					view: "pager",
					id: "pager_proveedor",
					size: 20,
					group: 20,
					count: 20,
					template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
				},
			],
		};
	}

	init() {
		const datatable = this.$$("data_proveedor");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}
}