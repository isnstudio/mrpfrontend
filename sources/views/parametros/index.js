import {JetView} from "webix-jet";

import TableView from "./table";
import ProveedorInterTableView from "./table_proveedor";
import FilterListView from "./sidebar/filters";
import Table from "../../helpers/table";
import "webix/breadcrumb";

var SELECTED_TAB = "data";

export default class ParametrosView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		this.Table = new TableView(this.app);
		this.TableProveedor = new ProveedorInterTableView(this.app);
		this.Filters = new FilterListView(this.app);

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Almacenes"}
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		const tabs = {
			view: "tabview",
			cells: [
				{
					header: "Almacenes",
					body: this.Table
				},
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					{
						view: "accordion",
						rows: [
							this.Filters,
							search,
							tabs
						]
					}
				]
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						tabs
					]
				},
				{
					width: 384,
					view: "accordion",
					rows: [
						this.Filters
					]
				},
			]
		};
	}

	ready(view) {
		const that = this;
		const datagrid = view.$scope.Table.$$("data");
		this.TableHelper.pagerParams(view, datagrid,"filters_parametros", this);

		if(this.app.Mobile) {
			this.Filters.$$("form_header").collapse();
		} else this.Filters.$$("form_header").expand();

		this.on(this.app.config.state.$changes, "filters_parametros",  () => {
			this.dataLoading();
		});

		this.Table.$$("data").attachEvent("onViewShow", function () {
			SELECTED_TAB = "data";
			that.$$("addButton").hide();
		});
	}

	dataLoading() {
		if(SELECTED_TAB === "data") {
			const url = this.app.MasterUrl + "api/mrp/almacen/wlist/?order_by=-id";
			const table = this.Table.$$("data");
			const filters = this.Filters;
			this.TableHelper.dataLoading(table, url, this, filters, 0, "filters_parametros");
		}
	}
}
