import {JetView} from "webix-jet";
import Filters from "../../../helpers/filters";
import "webix/text_field";

export default class FilterListView extends JetView {
	config() {
		this.Filters = new Filters(this.app);

		const form = {
			view: "form",
			localId: "filterform",
			id: "filter_form",
			autoheight: true,
			rows: [
				{
					view: "text_field",
					name: "name",
					label: "Nombre",
					placeholder: "Nombre"
				},
				{
					view: "button",
					value: "submit",
					label: "Buscar",
					type: "icon", icon: "mdi mdi-filter",
					height: 40,
					click: () => {
						this.Filters.filterParams(this, "filters_parametros");
					}
				},
				{
					id: "cleanfilter",
					view: "button",
					value: "submit",
					label: "Limpiar filtro",
					type: "icon",
					icon: "mdi mdi-sync",
					height: 40,
					click: () => {
						this.Filters.cleanFilter(this, "filters_parametros");
					}
				},
				{}
			]
		};

		return {
			view: "accordionitem",
			id: "form_header",
			template: "Filtros",
			header: "Filtros",
			type: "header",
			collapsed: false,
			body: form,
		};
	}

	urlChange() {
		this.$$("filter_form").setValues(this.app.config.state.filters_parametros);
	}
}