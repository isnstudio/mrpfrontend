import {JetView} from "webix-jet";
import Table from "../../helpers/table";
import SaveService from "../../services/save";

export default class DemandaManualTableView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		this.SaveService = new SaveService(this.app);
		const that = this;
		const datatable = {
			view: "datatable",
			id: "data",
			select: true,
			pager: "pager",
			scroll: "xy",
			editable: true,
			rowHeight: 40,
			columns: [
				{
					id: "id",
					header: "Folio",
					minWidth: 80,
				},
				{
					id: "nombre",
					header: "Nombre",
					adjust: true,
				},
				{
					id: "excluir_stock",
					header: "Excluir stock",
					editor: "combo",
					options: [{id: "true", value: "Si"}, {id: "false", value: "No"}],
					adjust: true,
					css: "bold"
				},
				{
					fillspace: true
				}
			],
			on: {
				onAfterRender() {
					this.$scope.TableHelper.afterRender(this, true);
				},
				onAfterEditStop:function(cell, editor){
					const datatable = that.$$("data");
					const row = datatable.getItem(editor.row);
					if(!row.excluir_stock)
						row.excluir_stock = cell.old;
					const request_options = {
						url: that.app.MasterUrl + "api/mrp/almacen/" + row.id + "/",
						get_values: row,
						validate: true,
						message: "Almacén guardado",
						path: "",
						context: that,
						method: "put",
						prevent_reload: true
					};
					that.SaveService = new SaveService(that.app);
					that.SaveService.save(request_options);

				},
			}
		};


		return {
			rows: [
				datatable,
				{
					view: "pager",
					id: "pager",
					size: 20,
					group: 20,
					count: 20,
					template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
				},
			],
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}
}