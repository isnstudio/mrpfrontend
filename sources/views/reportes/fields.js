import {JetView,} from "webix-jet";
import "webix/datepicker_iso";

export default class FormFieldsView extends JetView {
	config() {
		const reporte = {
			view: "combo",
			id: "reporte",
			name: "reporte",
			label: "Reporte",
			labelPosition: "top",
			placeholder: "Reporte",
			options: [
				{
					id: 2,
					value: "Reporte de confiabilidad de lead times"
				},
				{
					id: 3,
					value: "Reporte de confiabilidad de proyección de ventas"
				},
				{
					id: 4,
					value: "Mensajes de excepción"
				},
				{
					id: 5,
					value: "Reporte de cortos"
				},
				{
					id: 6,
					value: "Reporte de futuros"
				}
			],
			value: 1,
			required: true
		};

		const fecha_inicial = {
			view: "datepicker_iso",
			name: "fecha_inicial",
			id: "fecha_inicial",
			label: "Fecha inicial",
			labelPosition: "top",
			placeholder: "Fecha inicial",
			required: true
		};

		const fecha_final = {
			view: "datepicker_iso",
			name: "fecha_final",
			id: "fecha_final",
			label: "Fecha final",
			labelPosition: "top",
			placeholder: "Fecha final",
			required: true
		};


		const responsive_view = {
			margin: 10,
			rows: [
				reporte,
				fecha_inicial,
				fecha_final
			]
		};

		const standard_view = {
			margin: 10,
			rows: [
				reporte,
				{
					cols: [
						{
							margin: 10,
							rows: [
								fecha_inicial
							]
						},
						{
							margin: 10,
							rows: [
								fecha_final
							]
						}

					]
				}
			],
		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);
		return {
			margin: 10,
			rows: [
				main_section
			]
		};
	}
}