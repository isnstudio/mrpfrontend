import {JetView} from "webix-jet";
import FormFieldsView from "./fields.js";
import "webix/breadcrumb";
import Download_csv from "helpers/download_csv";

export default class PersonView extends JetView {
	config() {
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Reportes"}
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button",
					value: "Descargar reporte",
					css: "webix_primary",
					height: 40,
					id: "save",
					click: () => {
						const button = this.$$("save");
						this.action(button);
					}
				}
			]
		};

		this.fields = new FormFieldsView(this.app);

		this.form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 20,
			rows: [
				this.fields,
				{}
			],
			rules: {}
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					rows: [
						search,
						buttons,
						this.form,
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							this.form,
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}

	ready() {
		const that = this;
		this.fields.$$("reporte").attachEvent("onChange", function (value) {
			if(value === "5" || value === "6") {
				hide_fields_date();
			} else show_fields_date();
		});
		function hide_fields_date () {
			that.fields.$$("fecha_inicial").hide();
			that.fields.$$("fecha_final").hide();
			that.fields.$$("fecha_inicial").setValue("");
			that.fields.$$("fecha_final").setValue("");
		}

		function show_fields_date () {
			that.fields.$$("fecha_inicial").show();
			that.fields.$$("fecha_final").show();
		}
	}

	action(button) {
		const reporte = this.fields.$$("reporte").getValue();
		var url = "";
		var name = "";
		switch (reporte) {
			case "1": {
				url = "mrp/reportes/inventory-values/";
				name = "Reporte de valores de inventario";
				break;
			}
			case "2": {
				url = "mrp/reportes/reliability-lead-times/";
				name = "Reporte de confiabilidad de lead times";
				break;
			}
			case "3": {
				url = "mrp/reportes/reliability-sales-projection/";
				name = "Reporte de confiabilidad de proyección de ventas";
				break;
			}
			case "4": {
				url = "mrp/reportes/exception-messages/";
				name = "Reporte de mensajes de excepción";
				break;
			}
			case "5": {
				url = "mrp/reportes/cortos/";
				name = "Reporte de cortos";
				break;
			}
			case "6": {
				url = "mrp/reportes/futuros/";
				name = "Reporte de futuros";
				break;
			}
			default: break;
		}
		const download_csv = new Download_csv();
		const get_values = this.$$("form").getValues();
		const validate = this.$$("form").validate();
		download_csv.download_csv(url, get_values, button, this, validate, name);
	}
}