import {JetView} from "webix-jet";

export default class ArticuloWDetailView extends JetView {
	config() {
		const details = {
			rows: [
				{
					view: "property",
					id: "detail_info",
					disabled: true,
					autoheight: true,
					nameWidth: 160,
					elements: [
						{label: "Folio", type: "text", id: "id"},
						{label: "Clave", type: "text", id: "clave"},
						{label: "Nombre", type: "text", id: "nombre"},
						{label: "Categoría", type: "text", id: "categorizacion__display_webix"},
						{label: "Marca", type: "text", id: "marca__display_webix"},
						{label: "UM", type: "text", id: "unidad_medida__display_webix"},
						{label: "AWD", type: "text", id: "average_weekly_demand"},
						{label: "MWD", type: "text", id: "current_manual_demand"},
					],
				},
				{
					localId: "detail_components",
					autoheight: true,
					css: "wbackground",
					rows: []
				},
				{
					localId: "spacer",
					css: "template-scroll-y",
					borderless: true,
				}
			]
		};

		if (this.app.Mobile)
			return details;

		return {
			view: "accordionitem",
			template: "Detalle",
			header: "Detalle",
			type: "header",
			id: "detalle_header",
			collapsed: true,
			hidden: true,
			body: details
		};
	}

	hideContents() {
		if (!this.app.Mobile) {
			this.$$("detalle_header").hide();
			this.$$("detail_components").hide();
			this.$$("detail_info").hide();
		}
		this.RefreshDetails(undefined);
	}

	showContents(id) {
		if (!this.app.Mobile) {
			this.$$("detalle_header").show();
			this.$$("detail_components").show();
			this.$$("detail_info").show();
		}
		this.RefreshDetails(id);
	}

	showDetail(id) {
		this.articulo_id = id ? id : null;
		const content = this.$$("detail_components");
		if (content) {
			let childs = content.getChildViews();
			let child_ids = [];
			for (let i = 0; i < childs.length; i++) {
				let child_id = childs[i].config.id;
				child_ids.push(child_id);
			}
			for (let childIdsKey in child_ids) {
				content.removeView(child_ids[childIdsKey]);
			}
		}
		if(this.articulo_id && this.getRoot()) {
			this.showContents(this.articulo_id);
		}
	}

	ready() {
		this.on(this.app.config.state.$changes, "selected_articulo", id => {
			if (id) {
				this.showDetail(id);
			} else {
				this.hideContents();
			}
		});
	}


	RefreshDetails(id) {
		if (id) {
			var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/articulo/" + id + "/wdetails/");
			const json_response = JSON.parse(xhr.response);
			if ("actions_detail" in json_response && "id" in json_response) {
				const content = this.$$("detail_components");
				if (content) {
					for (const action_key in json_response["actions_detail"]) {
						const action = json_response["actions_detail"][action_key];
						let url = "/home/articulos.actions." + action["action"] + "/" + json_response["id"];
						if (action["action"] === "detail") {
							url = "/home/articulos." + action["action"] + "/" + json_response["id"];
						}
						content.addView({
							view: "button",
							label: action["label"],
							height: 40,
							click: () => {
								this.show(url);
							}
						}, 0);
					}
				}
			}
			if ("id" in json_response) {
				const sets = this.$$("detail_info");
				if (sets) {
					sets.setValues(json_response);
					this._parent.Filters.$$("form_header").collapse();
				}
			}
		}
	}
}

