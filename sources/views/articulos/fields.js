import {JetView,} from "webix-jet";
import "webix/text_field";
import "webix/decimal_field";
import "webix/server_autocomplete";
import "webix/number_field";

export default class ArticuloFormFieldsView extends JetView {
	config() {
		const reorden = {
			view: "checkbox",
			name: "es_reorden",
			id: "input_es_reorden",
			label: "Reorden",
			labelPosition: "top",
			value: true
		};

		const reorden_manual = {
			view: "checkbox",
			name: "reorder_point_manual",
			id: "input_reorder_point_manual",
			label: "¿Reorder point manual?",
			labelPosition: "top",
			height: 70,
		};

		const lot_size = {
			view: "decimal_field",
			name: "lot_size",
			label: "Tamaño de lote"
		};

		const clase = {
			view: "server_autocomplete",
			id: "input_clase",
			name: "clase",
			label: "Clase",
			labelPosition: "top",
			placeholder: "Clase",
			url: this.app.MasterUrl + "api/inv/articulo-clase/select_list/",
		};

		const lead_time = {
			view: "decimal_field",
			name: "lead_time",
			label: "Lead time",
		};

		const demanda_semanal = {
			view: "decimal_field",
			name: "average_weekly_demand",
			label: "Average weekly demand",
		};

		const ltc = {
			view: "decimal_field",
			name: "lead_time_consumption",
			label: "Lead time consumption",
		};

		const safety_stock = {
			view: "decimal_field",
			name: "safety_stock",
			label: "Safety stock",
			hidden: true
		};

		const cantidad_reorden = {
			view: "decimal_field",
			name: "reorder_point",
			label: "Reorder point",
		};

		const nivel_maximo = {
			view: "decimal_field",
			name: "max_stock",
			label: "Nivel Maximo",
		};

		const nivel_maximo_manual = {
			view: "checkbox",
			name: "max_stock_manual",
			id: "input_max_stock_manual",
			label: "Nivel maximo manual",
			labelPosition: "top",
			height: 70
		};

		const articulo_conversion = {
			view: "checkbox",
			name: "articulo_conversion",
			id: "input_articulo_conversion",
			label: "Artículo conversión",
			labelPosition: "top",
			height: 70
		};

		const semanas_safety_stock = {
			view: "number_field",
			name: "semanas_safety_stock",
			label: "Semanas safety stock",
		};

		const tipo_demanda = {
			view: "server_autocomplete",
			id: "input_tipo_demanda",
			name: "tipo_demanda",
			label: "Tipo demanda",
			labelPosition: "top",
			placeholder: "Tipo demanda",
			url: this.app.MasterUrl + "api/mrp/articulo/choice_field/tipo_demanda/",
		};

		const responsive_view = {
			margin: 10,
			rows: [
				reorden,
				lot_size,
				clase,
				safety_stock,
				semanas_safety_stock,
				nivel_maximo,
				nivel_maximo_manual,
				cantidad_reorden,
				tipo_demanda
			]
		};

		const standard_view = {
			margin: 10,
			rows: [
				{
					cols: [
						{
							margin: 10,
							rows: [
								reorden,
								lot_size,
								lead_time,
								ltc,
								tipo_demanda,
								reorden_manual,
								cantidad_reorden,
							]
						},
						{
							margin: 10,
							rows: [
								clase,
								demanda_semanal,
								safety_stock,
								semanas_safety_stock,
								nivel_maximo,
								nivel_maximo_manual
							]
						}
					]
				},
			],

		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);

		return {
			margin: 10,
			rows: [
				main_section,
			]
		};
	}
}
