import {JetView} from "webix-jet";
import Table from "../../helpers/table";

export default class ArticuloTableView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		const datatable = {
			view: "datatable",
			id: "grid",
			localId: "data",
			select: true,
			pager: "pager",
			scroll: "xy",
			rowHeight: 40,
			columns: [
				{
					id: "id",
					header: "Folio",
					width: 80
				},
				{
					id: "clave",
					header: "Clave",
					adjust: true,
					minWidth: 150
				},
				{
					id: "descripcion",
					header: "Descripción",
					fillspace: true,
				},
				{
					id: "categorizacion__display_webix",
					header: "Categoria",
					adjust: true,
				},
				{
					id: "marca__display_webix",
					header: "Marca",
					adjust: true,
				},
				{
					id: "unidad_medida__display_webix",
					header: {"text": "UM", css: "right"},
					adjust: true,
					css: "right",
				},
			],
			on: {
				onAfterRender() {
					this.$scope.TableHelper.afterRender(this, true, "filters_articulo");
				},
				onAfterSelect: (id) => {
					if(!this.app.Mobile) {
						this._parent.UnitsDetailView.$$("detalle_header").expand();
					}
					this.app.config.state.selected_articulo = id;
				},
			}
		};


		return {
			rows: [
				datatable,
				{
					view: "pager",
					id: "pager",
					size: 20,
					group: 20,
					count: 20,
					template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
				},
			],
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}
}