import {JetView, plugins} from "webix-jet";
import ArticuloFormFieldsView from "../fields";
import "webix/breadcrumb";
import "webix/button_service";

import {DEFAULT_WEEKS_VALUE} from "../index";

export default class ArticuloEditView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["id"]);
		this.articulo_id = this.getParam("id");

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Artículos", link: "/home/articulos"},
				{value: "Folio #" + this.articulo_id, link: "/home/articulos.detail/" + this.articulo_id},
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button_service",
					label: "Guardar",
					height: 40,
					path: "/home/articulos.detail/",
					method: "put",
					message: "Artículo guardado",
					url:  this.app.MasterUrl + "api/mrp/articulo/" + this.articulo_id + "/",
				},
				{
					view: "button",
					value: "Regresar",
					css: "webix_secondary",
					height: 40,
					click: () => {
						this.show("/home/articulos.detail/" + this.articulo_id);
					}
				}
			]
		};

		this.fields = new ArticuloFormFieldsView(this.app);

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 24,
			rows: [
				this.fields,
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					form,
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384
							},
							buttons
						]
					}
				]
			}
		};
	}

	ready() {
		const that = this;
		const form = this.$$("form");
		if (this.articulo_id) {
			var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/articulo/" + this.articulo_id);
			this.articulo = JSON.parse(xhr.response);
			form.setValues(this.articulo);
		}


		for (var element in form.elements) {
			form.elements[element].disable();
		}
		this.fields.$$("input_clase").enable();
		this.fields.$$("input_lot_size").enable();
		this.fields.$$("input_reorder_point_manual").enable();
		this.fields.$$("input_semanas_safety_stock").enable();
		this.fields.$$("input_max_stock_manual").enable();
		if(!this.articulo.semanas_safety_stock)
			this.fields.$$("input_semanas_safety_stock").setValue(DEFAULT_WEEKS_VALUE);
		if(this.articulo.reorder_point_manual)
			this.fields.$$("input_reorder_point").enable();

		this.fields.$$("input_max_stock_manual").attachEvent("onChange", function (value) {
			if(value)
				that.fields.$$("input_max_stock").enable();
			else that.fields.$$("input_max_stock").disable();
		});

		this.fields.$$("input_reorder_point_manual").attachEvent("onChange", function (value) {
			if(value)
				that.fields.$$("input_reorder_point").enable();
			else {
				that.fields.$$("input_reorder_point").setValue(that.articulo.reorder_point);
				that.fields.$$("input_reorder_point").disable();
			}
		});
	}
}