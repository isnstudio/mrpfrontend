import {JetView, plugins} from "webix-jet";
import "webix/breadcrumb";
import "webix/button_service";

export default class CorridaMRPAddView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["id"]);
		this.articulo_id = this.getParam("id");

		this.back_url = "/home/articulos.detail/" + this.articulo_id;

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Artículos", link: "/home/articulos"},
				{value: "Folio #" + this.articulo_id, link: this.back_url},
				{value: "Cambiar tipo de demanda"}
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button_service",
					label: "Cambiar",
					height: 40,
					path: this.back_url + "/",
					method: "put",
					message: "Tipo de demanda cambiada",
					url:  this.app.MasterUrl + "api/mrp/articulo/"+ this.articulo_id +"/cambiar_tipo_demanda/",
				},
				{
					view: "button",
					value: "Regresar",
					css: "webix_secondary",
					height: 40,
					click: () => {
						this.show(this.back_url);
					}
				}
			]
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 20,
			rows: [
				{
					template: "¿Estás seguro de cambiar el tipo de demanda del artículo?",
					type: "label",
					borderless: true,
					css: "question"
				},
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					form,
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}
}