import {JetView, plugins} from "webix-jet";

export default class LeadTimeWDetailView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["id"]);
		this.articulo_id = this.getParam("id");
		return {
			rows: [
				{
					view: "property",
					id: "lead_detail_info",
					disabled: true,
					autoheight: true,
					nameWidth: 160,
					elements: [
						{label: "Folio", type: "text", id: "id"},
						{label: "Proveedor", type: "text", id: "proveedor__display_webix"},
						{label: "Costo", type: "text", id: "costo_proveedor"},
						{label: "Moneda", type: "text", id: "get_moneda_display"},
					],
				},
				{
					localId: "detail_components",
					autoheight: true,
					css: "wbackground",
					rows: []
				},
				{
					localId: "spacer",
					css: "template-scroll-y",
					borderless: true,
				}
			]
		};
	}

	hideContents() {
		if (!this.app.Mobile) {
			this.$$("detail_components").hide();
			this.$$("lead_detail_info").hide();
			this._parent.$$("lead_time_header").hide();
		}
		this.RefreshDetails(undefined);
	}

	showContents(id) {
		if (!this.app.Mobile) {
			this.$$("detail_components").show();
			this.$$("lead_detail_info").show();
			this._parent.$$("lead_time_header").show();
		}
		this.RefreshDetails(id);
	}

	showDetail(id) {
		this.equivalencia_id = id ? id : null;
		const content = this.$$("detail_components");
		if (content) {
			let childs = content.getChildViews();
			let child_ids = [];
			for (let i = 0; i < childs.length; i++) child_ids.push(childs[i].config.id);
			for (let childIdsKey in child_ids) content.removeView(child_ids[childIdsKey]);
		}
		if (this.equivalencia_id && this.getRoot()) this.showContents(this.equivalencia_id);
	}

	ready() {
		var that = this;
		setTimeout(function(){
			that._parent.lead_times_ids.forEach((id) => {
				if (that.app.config.state.selected_lead_time != id){
					that.app.config.state.selected_lead_time = 0;
				}
			});
		}, 100);

		this.on(this.app.config.state.$changes, "selected_lead_time", id => {
			if (id) {
				this.showDetail(id);
			}
			else {
				this.hideContents();
			}
		});
	}

	RefreshDetails(id) {
		if (id) {
			var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/lead-time/" + id + "/wdetails/");
			const json_response = JSON.parse(xhr.response);
			if ("acciones" in json_response && "id" in json_response) {
				const content = this.$$("detail_components");
				if (content) {
					for (const action_key in json_response["acciones"]) {
						const action = json_response["acciones"][action_key];
						let css = "webix_secondary";
						let url = "/home/articulos.actions.detalles." + action["accion"] + "/" + this.articulo_id + "/" + json_response["id"];
						if (action["accion"] === "delete") css = "webix_danger";
						content.addView({
							view: "button",
							label: action["label"],
							css: css,
							height: 40,
							click: () => {
								this.show(url);
							}
						}, 0);
					}
				}
			}
			if ("id" in json_response) {
				const sets = this.$$("lead_detail_info");
				if (sets) {
					sets.setValues(json_response);
					if(this._parent.Filters)
						this._parent.Filters.$$("form_header").collapse();
				}
			}
		}
	}
}