import {JetView, plugins} from "webix-jet";
import DetalleFieldsView from "./fields";
import "webix/breadcrumb";
import "webix/button_service";
import "webix/server_autocomplete";
import "webix/decimal_field";

export default class LeadColocarCostoView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["articulo_id", "lead_time_id"]);
		this.articulo_id = this.getParam("articulo_id");
		this.lead_time_id = this.getParam("lead_time_id");
		this.back_url = "/home/articulos.detail/" + this.articulo_id;

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				{
					view: "breadcrumb",
					data: [
						{value: "Artículos", link: "/home/articulos"},
						{value: "#" + this.articulo_id, link: this.back_url},
						{value: "Colocar costo lead#" + this.lead_time_id}
					],
				}
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button_service",
					label: "Guardar",
					height: 40,
					path: this.back_url,
					method: "put",
					message: "Se colocó costo del producto#" + this.lead_time_id,
					url: this.app.MasterUrl + "api/mrp/lead-time/" + this.lead_time_id + "/editar_costo_proveedor/",
				},
				{
					view: "button",
					value: "Regresar",
					id: "btn_return",
					css: "webix_secondary",
					height: 40,
					click: () => this.show(this.back_url)
				}
			]
		};

		this.fields = new DetalleFieldsView(this.app);

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 24,
			rows: [
				{
					view: "text",
					name: "id",
					label: "Folio",
					labelPosition: "top",
					disabled: true
				},
				{
					view: "text",
					name: "proveedor__display_webix",
					label: "Proveedor",
					labelPosition: "top",
					disabled: true
				},
				{
					view: "server_autocomplete",
					name: "moneda",
					label: "Moneda",
					url: this.app.MasterUrl + "api/mrp/lead-time/choice_field/moneda/"
				},
				{
					view: "decimal_field",
					name: "costo_proveedor",
					label: "Costo proveedor"
				}
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					form,
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384
							},
							buttons
						]
					}
				]
			}
		};
	}

	ready() {
		const form = this.$$("form");
		var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/lead-time/" + this.lead_time_id);
		const data = JSON.parse(xhr.response);
		form.setValues(data);
	}
}