import {JetView} from "webix-jet";
import Table from "../../../../helpers/table";

export default class DetalleTableView extends JetView {
	config() {
		var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/lead-time/lead_times_active/");
		this.headers = JSON.parse(xhr.response);
		const columns = [
			{
				id: "id",
				header: "Folio",
				adjust: true
			},
			{
				id: "proveedor",
				header: "Proveedor",
				adjust: true,
			},
			{
				id: "aduana",
				header: "Aduana",
				fillspace: true
			},
		];
		Object.keys(this.headers).forEach(key => {
			columns.push({
				id: key,
				header: {text: this.headers[key], css: "right"},
				adjust: true,
				minWidth: 100,
				css: "right"
			});
		});
		columns.push({
			id: "lead_time_total",
			header: {text: "Lead time", css: "right"},
			adjust: true,
			minWidth: 100,
			css: "right"
		});
		columns.push({
			id: "minimum_order_quantity",
			header: {text: "MOQ", css: "right"},
			css: "right",
			adjust: true
		});


		this.TableHelper = new Table(this.app);
		return {
			view: "datatable",
			id: "data",
			select: false,
			scroll: "xy",
			rowHeight: 40,
			columns: columns,
			on: {
				onAfterRender() {
					this.$scope.TableHelper.afterRender(this, true);
				}
			}
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}
}