import {JetView} from "webix-jet";
import "webix/text_field";
import "webix/server_autocomplete";
import "webix/multi_server_autocomplete";
import "webix/decimal_field";
import "webix/price_field";

export default class DetalleFieldsView extends JetView {
	config() {
		var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/lead-time/lead_times_active/");
		this.headers = JSON.parse(xhr.response);

		const tipo = {
			view: "checkbox",
			name: "tipo",
			id: "input_tipo",
			label: "¿Es intercompañia?",
			labelPosition: "top"
		};

		let lead_times = [];
		Object.keys(this.headers).forEach(key => {
			lead_times.push({
				view: "text",
				type: "number",
				labelPosition: "top",
				name: key,
				label: this.headers[key],
				value: 0
			});
		});

		const proveedor = {
			view: "server_autocomplete",
			name: "proveedor",
			label: "Proveedor",
			url: this.app.MasterUrl + "api/cxp/proveedor/select_list/"
		};

		const aduana = {
			view: "server_autocomplete",
			name: "aduana",
			label: "Aduana",
			url: this.app.MasterUrl + "api/cat/sat/aduana/select_list/"
		};

		const minimum_order_quantity = {
			view: "text",
			type: "number",
			labelPosition: "top",
			name: "minimum_order_quantity",
			label: "MOQ",
			value: 0
		};
		if(!this.app.Mobile)
			lead_times.push(minimum_order_quantity);

		const interfaz_externa = {
			view: "server_autocomplete",
			name: "interfaz_externa",
			label: "Interfaz externa",
			url: this.app.MasterUrl + "api/mrp/interfaz-externa/select_list/"
		};

		const proveedor_intercompania = {
			view: "server_autocomplete",
			name: "proveedor_intercompania_id",
			label: "Proveedor intercompañia",
			url: this.app.MasterUrl + "api/mrp/interfaz-externa/select_list_intercompania/",
			disabled: true
		};

		const proveedor_intercompania_display = {
			view: "text",
			id: "proveedor_intercompania_display",
			name: "proveedor_intercompania_display",
			hidden: true
		};

		const responsive_view = {
			margin: 10,
			rows: [
				tipo,
				{
					hidden: true,
					id: "intercompañia_section",
					rows: [
						interfaz_externa,
						proveedor_intercompania
					]
				},
				{
					id: "proveedor_section",
					rows: [proveedor]
				},
				aduana,
				minimum_order_quantity,
				{
					cols: lead_times
				},
				proveedor_intercompania_display
			]
		};

		const standard_view = {
			margin: 10,
			rows: [
				{
					margin: 10,
					cols: [
						tipo,
						aduana,
					]
				},
				{
					id: "proveedor_section",
					margin: 10,
					cols: [
						proveedor,
					]
				},
				{
					id: "intercompañia_section",
					hidden: true,
					margin: 10,
					cols: [
						interfaz_externa,
						proveedor_intercompania,
					]
				},
				{
					margin: 10,
					cols: lead_times
				},
				proveedor_intercompania_display
			],
		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);
		return {
			margin: 10,
			rows: [
				main_section,
			]
		};
	}
}