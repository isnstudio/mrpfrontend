import {JetView, plugins} from "webix-jet";
import "webix/breadcrumb";
import "webix/button_service";

export default class LeadTimePrincipalView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["articulo_id", "lead_time_id"]);
		this.articulo_id = this.getParam("articulo_id");
		this.lead_time_id = this.getParam("lead_time_id");
		this.back_url = "/home/articulos.detail/" + this.articulo_id;

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				{
					view: "breadcrumb",
					data: [
						{value: "Artículos", link: "/home/articulos"},
						{value: "#" + this.articulo_id, link: this.back_url},
						{value: "Principal - Lead time #" + this.lead_time_id}
					],
				}
			]
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 20,
			rows: [
				{
					template: "¿Estás seguro de hacer principal el lead time?",
					type: "label",
					borderless: true,
					css: "question"
				},
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button_service",
					label: "Principal",
					height: 40,
					path: `/home/articulos.detail/${this.articulo_id}/`,
					method: "put",
					css: "webix_success",
					message: `Lead time #${this.lead_time_id} ahora es el principal`,
					url: `${this.app.MasterUrl}api/mrp/lead-time/${this.lead_time_id}/principal/`,
				},
				{
					view: "button",
					value: "Regresar",
					id: "btn_return",
					css: "webix_secondary",
					height: 40,
					click: () => this.show(this.back_url)
				}
			]
		};


		if (this.app.Mobile) {
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					cols: [
						{
							rows: [
								search,
								buttons,
								form,
							]
						}
					]
				}
			};
		}

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}
}