import {JetView, plugins} from "webix-jet";
import DetalleFieldsView from "./fields";
import "webix/breadcrumb";
import "webix/button_service";

export default class LeadTimeEditView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["articulo_id", "lead_time_id"]);
		this.articulo_id = this.getParam("articulo_id");
		this.lead_time_id = this.getParam("lead_time_id");
		this.back_url = "/home/articulos.detail/" + this.articulo_id;

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				{
					view: "breadcrumb",
					data: [
						{value: "Artículos", link: "/home/articulos"},
						{value: "#" + this.articulo_id, link: this.back_url},
						{value: "Editar lead time #" + this.lead_time_id}
					],
				}
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button_service",
					label: "Guardar",
					height: 40,
					path: `/home/articulos.detail/${this.articulo_id}/`,
					method: "put",
					message: "Se editó el lead time #" + this.lead_time_id,
					url: `${this.app.MasterUrl}api/mrp/lead-time/${this.lead_time_id}/`,
					new_values: {
						articulo: this.articulo_id
					}
				},
				{
					view: "button",
					value: "Regresar",
					id: "btn_return",
					css: "webix_secondary",
					height: 40,
					click: () => this.show(this.back_url)
				}
			]
		};

		this.fields = new DetalleFieldsView(this.app);

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 24,
			rows: [
				this.fields,
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					form,
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384
							},
							buttons
						]
					}
				]
			}
		};
	}

	ready() {
		const that = this;
		const form = this.$$("form");
		var xhr = webix.ajax().sync().get(`${this.app.MasterUrl}api/mrp/lead-time/${this.lead_time_id}/`);
		that.lead_time = JSON.parse(xhr.response);
		form.setValues(that.lead_time);
		filter_proveedor_intercompania(that.lead_time.interfaz_externa);
		this.fields.$$("input_interfaz_externa").disable();

		this.fields.$$("input_tipo").attachEvent("onChange", function (value) {
			if(value) {
				that.fields.$$("intercompañia_section").show();
				that.fields.$$("proveedor_section").hide();
			} else {
				that.fields.$$("intercompañia_section").hide();
				that.fields.$$("proveedor_section").show();
			}
		});

		this.fields.$$("input_tipo").setValue(that.lead_time.proveedor ? 0 : 1);
		if(!that.lead_time.proveedor)
			that.fields.$$("input_proveedor_intercompania_id").enable();

		this.fields.$$("input_tipo").hide();

		this.fields.$$("input_interfaz_externa").attachEvent("onChange", function (value) {
			filter_proveedor_intercompania(value);
		});


		function filter_proveedor_intercompania(value) {
			that.fields.$$("input_proveedor_intercompania_id")
				.define("url", that.app.MasterUrl + "api/mrp/interfaz-externa/select_list_intercompania/" +
					"?interfaz_externa=" + value + "&app=cxp&model=proveedor");
			that.fields.$$("input_proveedor_intercompania_id").enable();
		}

		this.fields.$$("input_proveedor_intercompania_id").attachEvent("onChange", function () {
			const display = that.fields.$$("input_proveedor_intercompania_id").getText();
			that.fields.$$("proveedor_intercompania_display").setValue(display);
		});
	}
}