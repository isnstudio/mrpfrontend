import {JetView, plugins} from "webix-jet";
import DetalleFieldsView from "./detalles/fields";
import "webix/breadcrumb";
import "webix/button_service";

export default class AddLeadTimeView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["id"]);
		this.articulo_id = this.getParam("id");
		this.back_url = "/home/articulos.detail/" + this.articulo_id;

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Artículos", link: "/home/articulos"},
				{value: "Folio #" + this.articulo_id, link: this.back_url},
				{value: "Agregar lead time"}
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button_service",
					label: "Guardar",
					height: 40,
					path: "/home/articulos.detail/" + this.articulo_id + "/",
					method: "post",
					message: "Lead time guardado",
					url:  this.app.MasterUrl + "api/mrp/articulo/" + this.articulo_id + "/lead_time_create/",
					new_values: {
						articulo: this.articulo_id
					}
				},
				{
					view: "button",
					value: "Regresar",
					id: "btn_return",
					css: "webix_secondary",
					height: 40,
					click: () => {
						this.show(this.back_url);
					}
				}
			]
		};

		this.fields = new DetalleFieldsView(this.app);

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 24,
			rows: [
				this.fields,
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					form,
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384
							},
							buttons
						]
					}
				]
			}
		};
	}

	ready() {
		const that = this;
		this.fields.$$("input_tipo").attachEvent("onChange", function (value) {
			if(value) {
				that.fields.$$("intercompañia_section").show();
				that.fields.$$("proveedor_section").hide();
				that.fields.$$("input_interfaz_externa").setValue("");
				that.fields.$$("input_proveedor_intercompania_id").setValue("");
				that.fields.$$("input_proveedor").setValue("");
			} else {
				that.fields.$$("intercompañia_section").hide();
				that.fields.$$("proveedor_section").show();
				that.fields.$$("input_interfaz_externa").setValue("");
				that.fields.$$("input_proveedor_intercompania_id").setValue("");
				that.fields.$$("input_proveedor").setValue("");
			}
		});

		this.fields.$$("input_interfaz_externa").attachEvent("onChange", function (value) {
			that.fields.$$("input_proveedor_intercompania_id")
				.define("url", that.app.MasterUrl + "api/mrp/interfaz-externa/select_list_intercompania/" +
					"?interfaz_externa=" + value + "&app=cxp&model=proveedor");
			that.fields.$$("input_proveedor_intercompania_id").enable();
		});

		this.fields.$$("input_proveedor_intercompania_id").attachEvent("onChange", function () {
			const display = that.fields.$$("input_proveedor_intercompania_id").getText();
			that.fields.$$("proveedor_intercompania_display").setValue(display);
		});
	}
}