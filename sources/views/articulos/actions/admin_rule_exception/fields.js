import {JetView,} from "webix-jet";
import "webix/text_field";
import "webix/decimal_field";
import "webix/server_autocomplete";
import "webix/number_field";

export default class AdminRuleExceptionFieldsView extends JetView {
	config() {
		const jalar = {
			template: "Jalar", type: "section", id: "jalar_section"
		};

		const jalar_rule = {
			view: "text",
			name: "jalar.rule",
			value: 1,
			hidden: true
		};

		const jalar_last_twenty_six_weeks = {
			view: "decimal_field",
			name: "jalar.last_twenty_six_weeks",
			label: "26 semanas"
		};

		const jalar_last_twelve_weeks = {
			view: "decimal_field",
			name: "jalar.last_twelve_weeks",
			label: "12 semanas"
		};

		const jalar_last_four_weeks = {
			view: "decimal_field",
			name: "jalar.last_four_weeks",
			label: "4 semanas"
		};

		const jalar_active_rule = {
			view: "checkbox",
			name: "jalar.active_rule",
			id: "input_jalar.active_rule",
			label: "¿Regla activa?",
			labelPosition: "top",
			value: true
		};

		const empujar = {
			template: "Empujar", type: "section", id: "empujar_section"
		};

		const empujar_rule = {
			view: "text",
			name: "empujar.rule",
			value: 2,
			hidden: true
		};

		const empujar_last_twenty_six_weeks = {
			view: "decimal_field",
			name: "empujar.last_twenty_six_weeks",
			label: "26 semanas"
		};

		const empujar_last_twelve_weeks = {
			view: "decimal_field",
			name: "empujar.last_twelve_weeks",
			label: "12 semanas"
		};

		const empujar_last_four_weeks = {
			view: "decimal_field",
			name: "empujar.last_four_weeks",
			label: "4 semanas"
		};

		const empujar_active_rule = {
			view: "checkbox",
			name: "empujar.active_rule",
			id: "input_empujar.active_rule",
			label: "¿Regla activa?",
			labelPosition: "top",
			value: true
		};


		const responsive_view = {
			margin: 10,
			rows: [
				jalar,
				jalar_rule,
				jalar_last_twenty_six_weeks,
				jalar_last_twelve_weeks,
				jalar_last_four_weeks,
				jalar_active_rule
			]
		};

		const standard_view = {
			margin: 10,
			rows: [
				{
					cols: [
						{
							margin: 10,
							rows: [
								jalar,
								jalar_rule,
								jalar_last_twenty_six_weeks,
								jalar_last_twelve_weeks,
								jalar_last_four_weeks,
								jalar_active_rule
							]
						},
						{
							margin: 10,
							rows: [
								empujar,
								empujar_rule,
								empujar_last_twenty_six_weeks,
								empujar_last_twelve_weeks,
								empujar_last_four_weeks,
								empujar_active_rule
							]
						}
					]
				},
			],

		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);

		return {
			margin: 10,
			rows: [
				main_section,
			]
		};
	}

}