import {JetView, plugins} from "webix-jet";
import AdminRuleExceptionFieldsView from "./fields";
import "webix/breadcrumb";
import "webix/button_service";

export default class ArticuloAdminRuleExceptionView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["id"]);
		this.articulo_id = this.getParam("id");
		this.back_url = "/home/articulos.actions.admin_rule_exception/" + this.articulo_id;

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Artículos", link: "/home/articulos"},
				{value: "Folio #" + this.articulo_id, link: "/home/articulos.detail/" + this.articulo_id},
				{value: "Administrar reglas de excepción", link: this.back_url},
				{value: "Crear"}
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button_service",
					label: "Guardar",
					height: 40,
					path: this.back_url + "/",
					method: "post",
					message: "Reglas guardadas",
					url:  this.app.MasterUrl + "api/mrp/mrp-articulo-rule-exception-message/",
					new_values: {
						articulo: this.articulo_id
					}
				},
				{
					view: "button",
					value: "Regresar",
					css: "webix_secondary",
					height: 40,
					click: () => {
						this.show(this.back_url);
					}
				}
			]
		};

		this.fields = new AdminRuleExceptionFieldsView(this.app);

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 24,
			rows: [
				this.fields,
			],
			complexData: true
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					form,
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384
							},
							buttons
						]
					}
				]
			}
		};
	}
}