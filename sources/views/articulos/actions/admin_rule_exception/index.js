import {JetView, plugins} from "webix-jet";
import AdminRuleExceptionFieldsView from "./fields";
import "webix/breadcrumb";
import "webix/button_service";

export default class ArticuloAdminRuleExceptionView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["id"]);
		this.articulo_id = this.getParam("id");
		this.back_url = "/home/articulos.detail/" + this.articulo_id;

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Artículos", link: "/home/articulos"},
				{value: "Folio #" + this.articulo_id, link: this.back_url},
				{value: "Administrar reglas de excepción"}
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button",
					value: "Crear",
					css: "webix_primary",
					id: "button_create",
					height: 40,
					hidden: true,
					click: () => {
						this.show("/home/articulos.actions.admin_rule_exception.create/" + this.articulo_id);
					}
				},
				{
					view: "button",
					value: "Editar",
					css: "webix_primary",
					id: "button_edit",
					height: 40,
					hidden: true,
					click: () => {
						this.show("/home/articulos.actions.admin_rule_exception.edit/" + this.articulo_id);
					}
				},
				{
					view: "button",
					value: "Regresar",
					css: "webix_secondary",
					height: 40,
					click: () => {
						this.show(this.back_url);
					}
				}
			]
		};

		this.fields = new AdminRuleExceptionFieldsView(this.app);

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 24,
			rows: [
				this.fields,
			],
			complexData: true
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					form,
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384
							},
							buttons
						]
					}
				]
			}
		};
	}

	ready() {
		const form = this.$$("form");
		if (this.articulo_id) {
			var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/mrp-articulo-rule-exception-message/get_articulo_rules/?articulo_id=" + this.articulo_id);
			this.rule_exception = JSON.parse(xhr.response);
			form.setValues(this.rule_exception[0]);
		}

		if(this.rule_exception[0].empujar || this.rule_exception[0].jalar) {
			this.$$("button_edit").show();
			this.$$("button_create").hide();
		} else {
			this.$$("button_edit").hide();
			this.$$("button_create").show();
		}


		for (var element in form.elements) {
			form.elements[element].disable();
		}
	}
}