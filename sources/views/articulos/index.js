import {JetView} from "webix-jet";
import Table from "../../helpers/table";
import FilterListView from "./sidebar/filters";
import ArticuloListDetailView from "./sidebar/detail_list";
import ArticuloTableView from "./table";
import "webix/breadcrumb";

export let DEFAULT_WEEKS_VALUE;
DEFAULT_WEEKS_VALUE = 4;

export default class ArticulosView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		this.Table = new (ArticuloTableView)(this.app);
		this.Filters = new FilterListView(this.app);
		if (!this.app.Mobile)
			this.UnitsDetailView = new ArticuloListDetailView(this.app);

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Artículos"},
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb,
			]
		};

		if (this.app.Mobile)
			return {
				type: "space",
				rows: [
					{
						view: "toolbar",
						paddingX: 10,
						height: 44,
						cols: [
							breadcrumb,
						]
					},
					{
						view: "template",
						template: "Filtros",
						type: "header"
					},
					this.Filters,
					this.Table,
					{$subview: true, popup: true}
				]
			};

		if (this.app.Mobile)
			return {
				rows: [
					{
						view: "accordion",
						rows: [
							this.Filters,
							search,
							this.Table,
							{$subview: true, popup: true}
						]
					}
				]
			};



		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						this.Table,
					]
				},
				{
					width: 384,
					view: "accordion",
					rows: [
						this.Filters,
						this.UnitsDetailView,
					]
				},
			]
		};
	}

	ready(view) {
		const datagrid = view.$scope.Table.$$("data");
		this.TableHelper.pagerParams(view, datagrid, "filters_articulo", this);

		if(this.app.Mobile) {
			this.Filters.$$("form_header").collapse();
		}

		this.on(this.app.config.state.$changes, "filters_articulo",  () => {
			this.dataLoading();
		});
	}

	dataLoading() {
		const url = this.app.MasterUrl + "api/mrp/articulo/wlist/?order_by=-id";
		const table = this.Table.$$("data");
		const filters = this.Filters;
		let selected_articulo = this.app.config.state.selected_articulo;
		let filter_default = "";
		if(!("es_reorden" in this.app.config.state.filters_articulo))
			filter_default = "es_reorden==1;";
		this.TableHelper.dataLoading(table, url, this, filters, selected_articulo, "filters_articulo" , filter_default);
	}
}
