import {JetView, plugins} from "webix-jet";
import ArticuloFormFieldsView from "./fields";
import DetalleTableView from "./actions/detalles/table";
import ArticuloListDetailView from "../demandas_calculadas/sidebar/detail_list";
import DetalleWDetailView from "./actions/detalles/detail_list";
import "webix/breadcrumb";

import {DEFAULT_WEEKS_VALUE} from "./index";

export default class ArticuloDetailView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["id"]);
		this.articulo_id = this.getParam("id");
		this.back_url = "/home/articulos";
		this.lead_times_ids = [];

		const selected_tab = this.app.config.state.selected_tab_articulos;

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Artículos", link: this.back_url},
				{value: "#" + this.articulo_id, link: "/home/articulos.detail/" + this.articulo_id},
			],
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button",
					value: "Regresar",
					id: "btn_return",
					css: "webix_secondary",
					height: 40,
					click: () => {
						this.show(this.back_url);
					}
				}
			]
		};

		this.Table = new DetalleTableView(this.app);
		this.UnitsDetailsView = new DetalleWDetailView(this.app);

		if (!this.app.Mobile)
			this.ArticuloUnitsDetailView = new ArticuloListDetailView(this.app);

		function folio_principal(value, config) {
			if (config.principal) return {"font-weight": 700, "background-color": "#B2DFDB" };
		}

		var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/lead-time/lead_times_active/");
		this.headers = JSON.parse(xhr.response);
		const columns = [
			{
				id: "id",
				header: "Folio",
				cssFormat: folio_principal,
				minWidth: 90,
				fillspace: true
			},
			{
				id: "proveedor",
				header: "Proveedor",
				cssFormat: folio_principal,
				adjust: true,
			},
			{
				id: "aduana",
				header: "Aduana",
				cssFormat: folio_principal,
				adjust: true,
				minWidth: 100
			},
		];
		Object.keys(this.headers).forEach(key => {
			columns.push({
				id: key,
				header: {text: this.headers[key], css: "right"},
				cssFormat: folio_principal,
				adjust: true,
				minWidth: 100,
				css: "right"
			});
		});
		columns.push({
			id: "lead_time_total",
			header: {text: "Lead time", css: "right"},
			cssFormat: folio_principal,
			adjust: true,
			minWidth: 100,
			css: "right"
		});
		columns.push({
			id: "minimum_order_quantity",
			header: {text: "MOQ", css: "right"},
			cssFormat: folio_principal,
			css: "right",
			adjust: true
		});

		const tabs = {
			id: "tab_view",
			view: "tabview",
			cells: [
				{
					header: "Lead times",
					body: {
						view: "datatable",
						id: "lead_time_datatable",
						select: true,
						scroll: "xy",
						rowHeight: 40,
						columns: columns,
						on: {
							onAfterSelect: (id) => this.app.config.state.selected_lead_time = id
						}
					}
				},
				{
					header: "Conversiones",
					body: {
						view: "datatable",
						id: "conversion_datatable",
						select: true,
						scroll: "xy",
						rowHeight: 40,
						columns: [
							{
								id: "unidad_medida_conversion__display_webix",
								header: "Unidad Medida",
								adjust: true,
								minWidth: 150
							},
							{
								id: "get_operacion_display",
								header: "Operación",
								adjust: true,
								minWidth: 150
							},
							{
								id: "factor",
								header: {"text": "Factor", css: "right"},
								fillspace: true,
								minWidth: 200,
								css: "right",
							},
						]
					}
				},
			],
			value: selected_tab
		};

		this.fields = new ArticuloFormFieldsView(this.app);

		const form = {
			view: "form",
			id: "form",
			rows: [
				this.fields
			],
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					type: "space",
					cols: [
						{
							rows: [
								{
									view: "toolbar",
									paddingX: 10,
									height: 44,
									cols: [
										breadcrumb
									]
								},
								buttons,
								form,
								tabs
							]
						}
					]
				}
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						{
							view: "toolbar",
							paddingX: 10,
							height: 44,
							cols: [
								breadcrumb
							]
						},
						{
							view: "scrollview",
							scroll: "y",
							body: {
								rows: [
									form,
									tabs
								]
							},
						}
					],
				},
				{
					width: 384,
					view: "accordion",
					rows: [
						{
							view: "template",
							template: "Acciones",
							type: "header",
						},
						buttons,
						this.ArticuloUnitsDetailView,
						{
							id: "lead_time_header",
							view: "template",
							template: "Lead time acciones",
							type: "header"
						},
						{
							view: "scrollview",
							body: {
								height: 320,
								rows: [
									this.UnitsDetailsView,
								]
							}
						}
					]
				}
			]
		};
	}

	ready() {
		const that = this;
		const form = this.$$("form");
		const lead_time_datatable = this.$$("lead_time_datatable");
		const conversion_datatable = this.$$("conversion_datatable");
		for (let element in form.elements) form.elements[element].disable();
		const lead_time_tab_button = document.querySelector("[button_id=\"lead_time_datatable\"]");
		const conversion_tab_button = document.querySelector("[button_id=\"conversion_datatable\"]");
		lead_time_tab_button.addEventListener("click", function () {
			conversion_datatable.clearSelection();
		});
		conversion_tab_button.addEventListener("click", function () {
			that.app.config.state.selected_lead_time = 0;
			lead_time_datatable.clearSelection();
		});
		if (this.articulo_id) {
			var xhr = webix.ajax().sync().get(`${this.app.MasterUrl}api/mrp/articulo/${this.articulo_id}/`);
			this.articulo = JSON.parse(xhr.response);
			form.setValues(this.articulo);

			if(!this.articulo.semanas_safety_stock)
				this.fields.$$("input_semanas_safety_stock").setValue(DEFAULT_WEEKS_VALUE);

			var xhr_lead_times = webix.ajax().sync().get(`${this.app.MasterUrl}api/mrp/articulo/${this.articulo_id}/lead_times/`);
			this.lead_times = JSON.parse(xhr_lead_times.response);
			Object.keys(this.lead_times).forEach(element => {
				this.lead_times_ids.push(
					this.lead_times[element].id
				);
			});
			lead_time_datatable.parse(this.lead_times);
			var xhr_conversiones = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/articulo-conversion/wlist_unpaginated/?articulo_id=" + this.articulo_id);
			const data_conversiones = JSON.parse(xhr_conversiones.response);

			conversion_datatable.parse(data_conversiones);

			const content = that.$$("buttons");
			if (content) {
				for (const action_key in that.articulo["actions"]) {
					const action = that.articulo["actions"][action_key];
					let url = "/home/articulos.actions." + action["action"] + "/" + that.articulo_id;
					content.addView({
						view: "button",
						label: action["label"],
						height: 40,
						click: () => that.show(url)
					}, 0);
				}
			}
		}

		this.fields.$$("input_safety_stock").show();


		this.$$("lead_time_datatable").attachEvent("OnViewShow", function () {
			that.app.config.state.selected_tab_articulos = "lead_time_datatable";
		});

		this.$$("conversion_datatable").attachEvent("OnViewShow", function () {
			that.app.config.state.selected_tab_articulos = "conversion_datatable";
		});

		that.app.config.state.selected_demanda_calculada = that.articulo_id;
		that.ArticuloUnitsDetailView.TendencySection.$$("tendency_section").hide();
		that.ArticuloUnitsDetailView.$$("detail_components").hide();
		that.ArticuloUnitsDetailView.$$("detalle_header").expand();
	}
}