import {JetView} from "webix-jet";
import "webix/breadcrumb";
import "webix/button_service";
import "webix/server_autocomplete";
import "webix/progress_bar";
import "webix/number_field";
import "webix/decimal_field";
import Errors from "../../helpers/errors";

const FILTER_COLS = "B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W";

const COLUMN_FECHA_LLEGADA = 18;
const COLUMN_FECHA_LLEGADA_CONFIRMADA = 19;
const COLUMN_CONTENEDOR = 20;
const COLUMN_COMMENTS = 21;
const COLUMN_FORWARDER = 22;
const COLUMN_FECHA_EMBARQUE = 23;
const COLUMN_FOLIO = 1;
const COLUMN_INTERCOMPANIA = 17;
const COLUMN_CANTIDAD = 9;

export default class AdministrarPartidasSSView extends JetView {
	config() {
		this.back_url = "/home/admin_partidas";
		this.dirty_rows = [];

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Administración de partidas", link: this.back_url},
				{value: "SpreadSheet"},
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		function getJsDateFromExcel(excelDate) {
			if(excelDate === "")
				return excelDate;
			if(typeof excelDate !== "number" && excelDate.includes("-"))
				return excelDate;
			const date = new Date(Math.round((excelDate - 25569) * 86400 * 1000));
			return new Date(date.getTime() + date.getTimezoneOffset() * 60000);
		}

		function getConfirmadaParsed(confirmada) {
			if(confirmada === "")
				return confirmada;
			return confirmada === "Confirmada" ? 1 :  0;
		}

		const that = this;
		const spreadsheet = {
			view: "spreadsheet",
			id: "ssheet",
			toolbar: [
				{
					rows: [{ $button: "excel-export" }],
				},
				{view: "ssheet-separator"},
				{
					cols:[
						{
							view: "button",
							label: "Procesar partidas",
							id: "procesar_partidas",
							width: 250,
							hidden: true,
							click: () => {
								if ($$("progress_modal"))
									$$("progress_modal").close();
								webix.ui({
									view: "window",
									head: "Progreso",
									id: "progress_modal",
									left: 10,
									top: 40,
									width: window.innerWidth - 30,
									body: {
										rows:[
											{
												view: "progress_bar"
											}
										]
									},
								}).show();
								function process_rows(index) {
									if(index < that.dirty_rows.length)  {
										that.dirty_rows[index][COLUMN_FECHA_LLEGADA_CONFIRMADA] =
											getConfirmadaParsed(that.dirty_rows[index][COLUMN_FECHA_LLEGADA_CONFIRMADA]);
										that.dirty_rows[index][COLUMN_FECHA_LLEGADA] = getJsDateFromExcel(that.dirty_rows[index][COLUMN_FECHA_LLEGADA]);
										that.dirty_rows[index][COLUMN_FECHA_EMBARQUE] = getJsDateFromExcel(that.dirty_rows[index][COLUMN_FECHA_EMBARQUE]);
										webix.ajax().headers({"Content-type":"application/json"}).put(that.app.MasterUrl +
											"api/mrp/orden-compra-detalle/update_oc_detalle_spreadsheet/", that.dirty_rows[index])
											.then(function () {
												let progress = Number(index / that.dirty_rows.length).toFixed(2);
												$$("progress").setValues({progress: progress});
												process_rows(index + 1);
											}).fail(function (res) {
												const error = new Errors();
												error.show_error(res);
												process_rows(index + 1);
											});
									} else {
										let progress = Number(index / that.dirty_rows.length).toFixed(2);
										$$("progress").setValues({progress: progress});
										setTimeout(function () {
											$$("progress_modal").close();
										},200);
										that.dirty_rows = [];
									}
								}
								process_rows(0);
							}
						},
					]
				},
			],
			on:{
				onBeforeValueChange: function (row, column, value) {
					if(column === COLUMN_CONTENEDOR) {
						return value ? value : false;
					}
					if(column === COLUMN_FECHA_LLEGADA) {
						return value ? value : false;
					}
					if(column === COLUMN_COMMENTS) {
						return value ? value : false;
					}
					if(column === COLUMN_FECHA_EMBARQUE) {
						return value ? value : false;
					}
					if(column === COLUMN_FORWARDER) {
						return value ? value : false;
					}
					if(column === COLUMN_FECHA_LLEGADA_CONFIRMADA) {
						return value ? value : false;
					}
					if(that.newRow) {
						return value ? value : false;
					}
					return false;
				},
				onCellChange: function (a) {
					const dirty_row = this.getRow(a);
					dirty_row.id = dirty_row[COLUMN_FOLIO] + " - " + dirty_row[COLUMN_INTERCOMPANIA];
					let found = that.dirty_rows.find(r => r.id === dirty_row.id);
					if(found) {
						const dirty_index = that.dirty_rows.indexOf(found);
						that.dirty_rows[dirty_index] = dirty_row;
					} else that.dirty_rows.push(dirty_row);

					this.$$("procesar_partidas").show();
				},
				onContextMenuConfig:function(ev){
					if (ev.area === "column" || ev.area === "row") {
						that.menu = false;
						return false;	
					}
					if (ev.area === "data") {
						that.menu = true;
						ev.data = [
							{ id:"ver_historial", value: "Ver historial" },
							{ id:"division_partida", value: "Dividir partida" },
							{ id:"descartar_partida", value: "Descartar partida" },
							{ id:"agregar_partida", value: "Agregar partida a OC" },
						];
					}
				}
			}
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					spreadsheet
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							spreadsheet
						]
					},
				]
			}
		};
	}

	init() {
		const ssheet = this.$$("ssheet");
		this.webix.extend(ssheet, webix.OverlayBox);
		this.webix.extend(ssheet, webix.ProgressBar);

		const that = this;
		window.onbeforeunload = function (event) {
			if(that.dirty_rows.length)
				event.returnValue = "Tiene información sin guardar, ¿Seguro de salir?";
		};

		this.app.attachEvent("app:guard", function(url, view, nav){
			if(that.dirty_rows.length)
				if(url !== "/home/admin_partidas") {
					const redirect = window.confirm("Tiene información sin guardar, ¿Seguro de salir?");
					nav.redirect = redirect ? url : "/home/admin_partidas";
					return redirect;
				}
		});
	}

	ready() {
		const that = this;
		const ssheet = this.$$("ssheet");
		ssheet.showProgress();
		setTimeout(function () {
			that.loadSpreadSheetData();
			that.lockCells();
			that.afterLoad();
			ssheet.hideProgress();
		}, 200);
	}

	loadSpreadSheetData() {
		var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/orden-compra-detalle/spreadsheet_list/");
		this.data = JSON.parse(xhr.response);
		this.master_url = this.app.MasterUrl;

		const data = this.data.data;
		this.column_fecha_llegada = data.filter(c => c[1] === COLUMN_FECHA_LLEGADA);
		this.column_fecha_llegada.shift();

		this.column_fecha_embarque = data.filter(c => c[1] === COLUMN_FECHA_EMBARQUE);
		this.column_fecha_embarque.shift();

		this.column_fecha_llegada_confirmada = data.filter(c => c[1] === COLUMN_FECHA_LLEGADA_CONFIRMADA);
		this.column_fecha_llegada_confirmada.shift();
		var base_data = {
			data: data,
			"sizes": [
				[0,2,550],
				[0,3,280],
				[0,4,180],
				[0,5,180],
				[0,6,180],
				[0,7,180],
				[0,8,550],
				[0,9,180],
				[0,10,180],
				[0,11,180],
				[0,12,180],
				[0,13,180],
				[0,14,180],
				[0,15,180],
				[0,16,180],
				[0,17,180],
				[0,18,180],
				[0,19,280],
				[0,20,280],
				[0,21,280],
				[0,22,180],
				[0,23,180],
			],
		};
		this.$$("ssheet").data_setter(base_data);
	}

	afterLoad() {
		var text_highlight = document.getElementsByClassName("webix_text_highlight_value");
		if (text_highlight.length) {
			text_highlight[0].style.zIndex = 2;
		}
		const that = this;
		const last_row = this.data.data[this.data.data.length - 1][0];
		this.$$("ssheet").freezeRows(1);

		FILTER_COLS.split(",").forEach(function (col, index) {
			const current_col = index + 2;
			that.$$("ssheet").setCellFilter(1, current_col, col + "2:" + col + last_row);
			that.$$("ssheet").lockCell(1, current_col, false);
		});

		this.column_fecha_llegada.forEach(fecha => {
			that.$$("ssheet").setFormat(fecha[0], fecha[1], "yyyy-mm-dd");
		});

		this.column_fecha_embarque.forEach(fecha => {
			that.$$("ssheet").setFormat(fecha[0], fecha[1], "yyyy-mm-dd");
		});

		this.column_fecha_llegada_confirmada.forEach(fecha => {
			$$("ssheet").setCellEditor(fecha[0], fecha[1],{editor:"ss_richselect", options:["Confirmada", "No confirmada"]});
		});
		that.$$("ssheet").refresh();


		const menu = this.$$("ssheet").$$("context");

		menu.attachEvent("onBeforeShow", function (obj) {
			if(that.menu) {
				let pos = that.$$("ssheet").$$("cells").locate(obj);
				that.context_row = that.$$("ssheet").getRow(pos.row);
			}
		});

		menu.attachEvent("onItemClick", function(id){
			if(id === "ver_historial") {
				var xhr = webix.ajax().sync().get(that.app.MasterUrl +
					"api/mrp/orden-compra-detalle/oc_detalle_logs/?id="+ that.context_row[COLUMN_FOLIO] +
					"&intercompania=" + that.context_row[COLUMN_INTERCOMPANIA]);
				const json_response = JSON.parse(xhr.response);
				if ($$("historial"))
					$$("historial").close();
				webix.ui({
					view: "window",
					move: true,
					head: "Historial",
					id: "historial",
					left: window.innerWidth - 660, top: 40,
					width: 484,
					height: 510,
					body: {
						rows: [
							{
								view: "toolbar", height: 56, padding: {left: 6, right: 14},
								cols: [
									{view: "icon", icon: "wxi-close", click: () => $$("historial").close()}
								]
							},
							{
								view: "scrollview",
								body: {
									rows: [
										{
											view: "timeline",
											id: "timeline",
											scheme:{
												$init:function(obj){
													obj.value = "<b>Comentario:</b> " + obj.comentarios +
														"<br> <b>Fecha Llegada: </b>" + obj.fecha_llegada +
														"<br> <b>Tracking number: </b>" + obj.tracking_number +
														"<br> <b>Logística: </b>" + obj.logistica +
														"<br> <b>Fecha de embarque: </b>" + obj.fecha_embarque;
													obj.date = obj.created_on;
													obj.details = obj.creado_por;
												}
											},
											type: {
												height: 180,
											}
										},
										{
											id: "empty_timeline",
											hidden: true,
											template: (data) => {
												if(data[0]) {
													return "<p style='text-align: center; color: #9F6000; background-color: #FEEFB3;'>"
														+ data[0].mensaje +"</p>";
												}
											}
										}
									]
								}
							}
						]
					},
				}).show();
				if(!json_response[0].log_id) {
					$$("timeline").hide();
					$$("empty_timeline").show();
					$$("empty_timeline").setValues(json_response);
				} else $$("timeline").data_setter(json_response);
			}
			if(id === "division_partida") {
				let xhr = webix.ajax().sync().get(
					that.app.MasterUrl + "api/mrp/orden-compra-detalle/init_warning_mercancia_recepcion/" +
					"?id=" + that.context_row[COLUMN_FOLIO] + "&intercompania=" + that.context_row[COLUMN_INTERCOMPANIA],
					{}
				);
				if ($$("division_partida"))
					$$("division_partida").close();
				webix.ui({
					view: "window",
					move: true,
					head: "Dividir partida",
					id: "division_partida",
					left: window.innerWidth - 660, top: 40,
					width: 584,
					height: 300,
					body: {
						rows: [
							{
								view: "form",
								id: "form",
								padding: 20,
								rows: [
									{
										view: "number_field",
										name: "cantidad",
										label: "Cantidad nueva",
										required: true
									},
									{
										view: "server_autocomplete",
										name: "plaza",
										label: "Plaza",
										labelPosition: "top",
										placeholder: "Plaza",
										required: true,
										url: that.app.MasterUrl + "api/cat/plaza/select_list/",
									},
									{
										view: "label",
										id: "warning_data",
										name: "warning_data",
										label: "",
										disabled: true,
										required: false
									},
									{}
								],
								rules: {}
							},
							{
								cols: [
									{
										view: "button",
										label: "Cancelar",
										height: 40,
										click: () => {
											$$("division_partida").close();
										}
									},
									{
										view: "button",
										label: "Dividir partida",
										height: 40,
										click: () => {
											if($$("form").validate()){
												const cantidad = $$("input_cantidad").getValue();
												const plaza = $$("input_plaza").getValue();
												const data = {
													id: that.context_row[COLUMN_FOLIO],
													intercompania: that.context_row[COLUMN_INTERCOMPANIA],
													cantidad_nueva_partida: cantidad,
													plaza: plaza
												};
												webix.message("Dividiendo partida");
												var xhr = webix.ajax().post(that.app.MasterUrl +
													"api/mrp/orden-compra-detalle/division_partida/", data).then(() => {
													that.refresh();
													$$("division_partida").close();
												}).fail(function (res) {
													const error = new Errors();
													error.show_error(res);
												});
											}
										}
									}
								]
							}
						]
					},
				}).show();
				setTimeout(function(){
					let warning_data = $$("warning_data");
					if (xhr.response){
						let response = JSON.parse(xhr.response);
						warning_data.show();
						warning_data.setValue(
							response.warning[0]
						);
					} else {
						warning_data.hide();
					}
				}, 500);
			}
			if(id === "descartar_partida") {
				if ($$("descartar_partida"))
					$$("descartar_partida").close();
				webix.ui({
					view: "window",
					move: true,
					head: "Descartar partida",
					id: "descartar_partida",
					left: window.innerWidth - 660, top: 40,
					width: 584,
					height: 200,
					body: {
						rows: [
							{
								view: "form",
								id: "form_descartar_partida",
								padding: 20,
								rows: [
									{
										template: "¿Estás seguro de descartar la partida?",
										type: "label",
										borderless: true,
										css: "question"
									},
									{}
								],
								rules: {}
							},
							{
								cols: [
									{
										view: "button",
										label: "Cancelar",
										height: 40,
										click: () => {
											$$("descartar_partida").close();
										}
									},
									{
										view: "button",
										label: "Descartar partida",
										height: 40,
										click: () => {
											if($$("form_descartar_partida").validate()){
												const data = {
													id: that.context_row[COLUMN_FOLIO],
													intercompania: that.context_row[COLUMN_INTERCOMPANIA]
												};
												webix.message("Descartando partida");
												var xhr = webix.ajax().put(that.app.MasterUrl +
													"api/mrp/orden-compra-detalle/descartar/", data).then(() => {
													that.refresh();
													$$("descartar_partida").close();
												}).fail(function (res) {
													const error = new Errors();
													error.show_error(res);
												});
											}
										}
									}
								]
							}
						]
					},
				}).show();
			}
			if(id === "agregar_partida"){
				let xhr = webix.ajax().sync().get(
					that.app.MasterUrl + "api/mrp/orden-compra-detalle/init/" +
					"?id=" + that.context_row[COLUMN_FOLIO] + "&intercompania=" + that.context_row[COLUMN_INTERCOMPANIA],
					{}
				);
				if ($$("agregar_partida")){
					$$("agregar_partida").close();
				}
				webix.ui({
					view: "window",
					move: true,
					head: "Agregar partida a OC",
					id: "agregar_partida",
					left: window.innerWidth - 660, top: 40,
					width: 584,
					height: 400,
					body: {
						rows: [
							{
								view: "form",
								id: "form_agregar_partida",
								padding: 20,
								rows: [
									{
										template: "",
										type: "label",
										borderless: true,
										css: "question"
									},
									{
										view: "number_field",
										name: "cantidad_nueva_partida",
										label: "Cantidad nueva",
										required: true
									},
									{
										view: "server_autocomplete",
										name: "plaza",
										label: "Plaza",
										labelPosition: "top",
										placeholder: "Plaza",
										url: that.app.MasterUrl + "api/cat/plaza/select_list/",
									},
									{
										view: "decimal_field",
										name: "costo",
										label: "Costo",
										required: true
									},
									{}
								],
								rules: {}
							},
							{
								cols: [
									{
										view: "button",
										label: "Cancelar",
										height: 40,
										click: () => {
											$$("agregar_partida").close();
										}
									},
									{
										view: "button",
										label: "Agregar partida",
										height: 40,
										click: () => {
											if($$("form_agregar_partida").validate()){
												let data = {
													id: that.context_row[COLUMN_FOLIO],
													cantidad_nueva_partida: $$("input_cantidad_nueva_partida").getValue(),
													costo: $$("input_costo").getValue(),
													intercompania: that.context_row[COLUMN_INTERCOMPANIA],
													plaza: $$("input_plaza").getValue()
												};
												webix.message("Agregando partida");
												webix.ajax().post(
													that.app.MasterUrl + "api/mrp/orden-compra-detalle/agregar_nueva_partida/", data
												).then(() => {
													that.refresh();
													$$("agregar_partida").close();
												}).fail(function (res) {
													const error = new Errors();
													error.show_error(res);
												});
											}
										}
									}
								]
							}
						]
					},
				}).show();
				setTimeout(function(){
					$$("form_agregar_partida").setValues(
						JSON.parse(xhr.response)
					);
				}, 500);
			}
		});
	}

	lockCells() {
		const last_row = this.data.data[this.data.data.length - 1][0];
		const last_col = this.data.data[this.data.data.length - 1][1];
		this.$$("ssheet").lockCell({row: 1, column: 1}, {row: last_row, column: last_col}, true);

		this.$$("ssheet").lockCell({row: 2, column: COLUMN_FECHA_LLEGADA}, {row: last_row, column: COLUMN_FECHA_LLEGADA}, false);

		this.$$("ssheet").lockCell({row: 2, column: COLUMN_CONTENEDOR}, {row: last_row, column: COLUMN_CONTENEDOR}, false);

		this.$$("ssheet").lockCell({row: 2, column: COLUMN_COMMENTS}, {row: last_row, column: COLUMN_COMMENTS}, false);

		this.$$("ssheet").lockCell({row: 2, column: COLUMN_FORWARDER}, {row: last_row, column: COLUMN_FORWARDER}, false);

		this.$$("ssheet").lockCell({row: 2, column: COLUMN_FECHA_EMBARQUE}, {row: last_row, column: COLUMN_FECHA_EMBARQUE}, false);

		this.$$("ssheet").lockCell({row: 2, column: COLUMN_FECHA_LLEGADA_CONFIRMADA}, {row: last_row, column: COLUMN_FECHA_LLEGADA_CONFIRMADA}, false);
	}

	insertNewRow(res, cantidad) {
		const new_row_id = this.context_row.id + 1;
		this.$$("ssheet").insertRow(new_row_id);
		this.newRow = true;
		this.$$("ssheet").lockCell(this.context_row.id, COLUMN_CANTIDAD, false);
		this.$$("ssheet").setCellValue(this.context_row.id, COLUMN_CANTIDAD,
			(this.context_row[COLUMN_CANTIDAD] - cantidad));
		this.$$("ssheet").lockCell(this.context_row.id, COLUMN_CANTIDAD, true);

		const keys = Object.keys(this.context_row);

		keys.forEach(k => {
			if(!isNaN(k)){
				if(k === COLUMN_CANTIDAD.toString()) {
					this.$$("ssheet").setCellValue(new_row_id, k, cantidad);
				} else if(k === COLUMN_FOLIO.toString())
					this.$$("ssheet").setCellValue(new_row_id, k, res[1].id);
				else this.$$("ssheet").setCellValue(new_row_id, k, this.context_row[k]);
			}
		});

		this.lockCells();
		this.newRow = false;
	}
}