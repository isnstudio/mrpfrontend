import {JetView} from "webix-jet";

import LoginFormView from "./form";

export default class LoginView extends JetView {
	config() {
		return {
			width: screen.width,
			css: {
				"background-image": "url(/data/images/bg.jpg)",
				"transition": "background .2s ease",
				"position": "fixed",
				"top": "0",
				"left": "0",
				"right": "0",
				"bottom": "0",
				"background-size": "cover",
				"background-repeat": "no-repeat",
				"background-position": "center"
			},
			cols: [
				LoginFormView
			]
		};
	}
}
