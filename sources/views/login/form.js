import {JetView} from "webix-jet";
import "webix/text_field";

export default class LoginTableView extends JetView {
	config() {
		return {
			rows: [{}, {
				cols: [{}, {
					view: "form", localId: "form", css: {
						"opacity": ".9 !important",
						"background-color": "hsl(202deg 10% 41%) !important",
						"border-radius": "30px !important"
					}, borderless: true, width: 400, rows: [{
						cols: [{
							template: "<img alt='logo' src='/data/images/logo.png' height='40'>", height: 50, width: 300
						}, {
							css: {
								"font-size": "16px", "color": "#ca011b"
							}, template: "<br><b>MRP</b>", height: 50
						},]
					}, {
						view: "text_field",
						name: "username",
						id: "username",
						label: "<span class=\"webix_icon mdi mdi-account\" style='font-size: 30px;padding: 5px;width: 50%; color: white'></span>",
						placeholder: "username",
						inputHeight: 40,
						css: "input-login",
						align: "left",
						labelPosition: "left"
					}, {
						view: "text_field",
						name: "password",
						type: "password",
						id: "password",
						css: "input-login",
						label: "<span class=\"webix_icon mdi mdi-lock\" style='font-size: 30px;padding: 5px;width: 50%; color: white'></span>",
						placeholder: "password",
						inputHeight: 40,
						labelPosition: "left"
					}, {
						cols: [{}, {
							template: "Login", width: 50, css: {
								"color": "white",
								"opacity": ".9 !important",
								"background-color": "hsl(202deg 10% 41%) !important",
								"padding": "10px"
							}, borderless: true, click: () => {
								this.login();
							}
						}, {
							view: "icon",
							icon: "mdi mdi-arrow-right",
							localId: "addButton",
							height: 50,
							hotkey: "enter",
							value: "submit",
							click: () => {
								this.login();
							},
						}, {}]
					},], rules: {
						username: webix.rules.isNotEmpty, password: webix.rules.isNotEmpty
					}
				}, {}]
			}, {},]
		};
	}

	login() {
		if (this.$$("form").validate()) {
			const form_values = this.$$("form").getValues();
			const authService = this.app.getService("auth");
			const username = form_values.username;
			const password = form_values.password;
			authService.login(username, password).then(() => {
				if (this.getUrl()[0].page === "login") {
					this.show("/home");
				} else {
					this.app.refresh();
				}
			}, () => this.IncorrectLogin());
		}
	}

	IncorrectLogin() {
		webix.message({
			text: "Error al iniciar sesión, verifique sus credenciales", type: "error", expire: -1, id: "messageLogin"
		});
	}
}
