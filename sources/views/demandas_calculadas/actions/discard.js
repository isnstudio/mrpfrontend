import {JetView, plugins} from "webix-jet";
import "webix/breadcrumb";
import "webix/button_service";

export default class DemandaCalculadaDiscardView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["id", "demanda_calculada_id"]);
		this.articulo_id = this.getParam("id");
		this.demanda_calculada_id = this.getParam("demanda_calculada_id");
		this.back_url = "/home/demandas_calculadas.detail/" + this.articulo_id;

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Demandas calculadas", link: "demandas_calculadas"},
				{value: "Folio #" + this.demanda_calculada_id, link: this.back_url},
				{value: "Descartar"},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 20,
			rows: [
				{
					template: "¿Estás seguro de descartar la demanda calculada?",
					type: "label",
					borderless: true,
					css: "question"
				},
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button_service",
					label: "Descartar",
					css: "webix_danger",
					path: this.back_url,
					method: "put",
					message: "Demanda calculada #" + this.demanda_calculada_id + " descartada",
					url: this.app.MasterUrl + "api/mrp/calculated-demand/" + this.demanda_calculada_id + "/discard/"
				},
				{
					view: "button",
					value: "Regresar",
					css: "webix_secondary",
					height: 40,
					click: () => this.show(this.back_url)
				}
			]
		};


		if (this.app.Mobile) {
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					cols: [
						{
							rows: [
								search,
								buttons,
								form,
							]
						}
					]
				}
			};
		}

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}
}