import {JetView, plugins} from "webix-jet";

export default class DemandasCalculadasWDetailView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["id"]);
		this.articulo_id = this.getParam("id");
		return {
			rows: [
				{
					localId: "detail_components",
					autoheight: true,
					css: "wbackground",
					rows: [],
					height: 60
				}
			]
		};
	}

	hideContents() {
		if (!this.app.Mobile) {
			this.$$("detail_components").hide();
			this._parent.$$("demanda_calculada_header").hide();
		}
		this.RefreshDetails(undefined);
	}

	showContents(id) {
		if (!this.app.Mobile) {
			this.$$("detail_components").show();
			this._parent.$$("demanda_calculada_header").show();
		}
		this.RefreshDetails(id);
	}

	showDetail(id) {
		this.demanda_calculada_id = id ? id : null;
		const content = this.$$("detail_components");
		if (content) {
			let childs = content.getChildViews();
			let child_ids = [];
			for (let i = 0; i < childs.length; i++) child_ids.push(childs[i].config.id);
			for (let childIdsKey in child_ids) content.removeView(child_ids[childIdsKey]);
		}
		if (this.demanda_calculada_id && this.getRoot()) this.showContents(this.demanda_calculada_id);
	}

	ready() {
		this.on(this.app.config.state.$changes, "selected_demanda_calculada_detalle", id => {
			if (id) this.showDetail(id); else this.hideContents();
		});
	}

	RefreshDetails(id) {
		if (id) {
			var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/calculated-demand/" + id + "/wdetails/");
			const json_response = JSON.parse(xhr.response);
			if ("actions" in json_response && "id" in json_response) {
				const content = this.$$("detail_components");
				if (content) {
					for (const action_key in json_response["actions"]) {
						const action = json_response["actions"][action_key];
						let css = "webix_secondary";
						let url = "/home/demandas_calculadas.actions." + action["action"] + "/" + this.articulo_id + "/" + json_response["id"];
						content.addView({
							view: "button",
							label: action["label"],
							css: css,
							height: 40,
							click: () => {
								this.show(url);
							}
						}, 0);
					}
				}
			}
			if(!json_response["actions"].length)
				this.hideContents();
		}
	}
}