import {JetView} from "webix-jet";
import Filters from "../../../helpers/filters";
import "webix/text_field";
import "webix/datepicker_iso";
import "webix/server_autocomplete";

export default class FilterListView extends JetView {
	config() {
		this.Filters = new Filters(this.app);

		const form = {
			view: "form",
			localId: "filterform",
			id: "filter_form",
			autoheight: true,
			rows: [
				{
					view: "server_autocomplete",
					name: "cliente",
					label: "Cliente",
					url: this.app.MasterUrl + "api/cxc/cliente/select_list/",
				},
				{
					view: "server_autocomplete",
					name: "vendedor",
					label: "Vendedor",
					url: this.app.MasterUrl + "api/cat/user/select_list/",
				},
				{
					view: "datepicker_iso",
					name: "date",
					label: "Fecha"
				},
				{
					view: "button",
					value: "submit",
					label: "Buscar",
					type: "icon", icon: "mdi mdi-filter",
					height: 40,
					click: () => {
						this.Filters.filterParams(this, "filters_demandas_calculadas_detalle", this._parent.$$("datatable_calculadas"));
					}
				},
				{
					id: "cleanfilter",
					view: "button",
					value: "submit",
					label: "Limpiar filtro",
					type: "icon",
					icon: "mdi mdi-sync",
					height: 40,
					click: () => {
						this.Filters.cleanFilter(this, "filters_demandas_calculadas_detalle");
						cleanFilterAfter();
					}
				},
				{}
			]
		};

		return {
			view: "accordionitem",
			id: "form_header",
			template: "Filtros",
			header: "Filtros",
			type: "header",
			collapsed: false,
			body: form,
		};

		function cleanFilterAfter() {
			setTimeout(function(){
				this.$$("input_es_reorden").setValue(1);
			},100);
		}
	}

	urlChange() {
		var filters = this.app.config.state.filters_demandas_calculadas_detalle;
		this.$$("filter_form").setValues(filters);
	}
}