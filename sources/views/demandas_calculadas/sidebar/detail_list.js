import {JetView} from "webix-jet";
import TendencySectionView from "./tendency_section";

export default class ArticuloWDetailView extends JetView {
	config() {
		this.TendencySection = new TendencySectionView(this.app);

		var tendency_detail = {
			view: "chart",
			id: "tendency_detail",
			type: "bar",
			barWidth: 20,
			radius: 2,
			borderless: true,
			hidden: true,
			series: [
				{
					value: "#quantity#",
					color: "#d06d99",
					tooltip: {
						template: "#quantity#"
					}
				},
				{
					type: "line",
					value: "#predicted_result#",
					item: {
						borderColor: "#ff5700",
						color: "#ff5700",
						type: "r",
						radius: 1
					},
					line: {
						color: "#ff5700",
						width: 1
					},
					tooltip: {
						template: "#complete_date#"
					}
				}
			]
		};

		const details = {
			rows: [
				{
					view: "property",
					id: "detail_info",
					disabled: true,
					autoheight: true,
					nameWidth: 160,
					elements: [
						{label: "Número de parte", type: "text", id: "clave"},
						{label: "Nombre", type: "text", id: "nombre"},
						{label: "Categoría", type: "text", id: "categorizacion__display_webix"},
						{label: "Marca", type: "text", id: "marca__display_webix"},
						{label: "UM", type: "text", id: "unidad_medida__display_webix"},
						{label: "AWD", type: "text", id: "average_weekly_demand"},
						{label: "Tamaño de lote", type: "text", id: "lot_size"},
						{label: "Clase", type: "text", id: "clase__display_webix"},
						{label: "Safety stock", type: "text", id: "safety_stock"},
						{label: "Reorder point", type: "text", id: "reorder_point"},
						{label: "Nivel máximo", type: "text", id: "max_stock"},
					],
				},
				{
					id: "detail_components",
					autoheight: true,
					css: "wbackground",
					rows: []
				},
				this.TendencySection,
				tendency_detail,
				{
					localId: "spacer",
					css: "template-scroll-y",
					borderless: true,
				},
			]
		};

		if (this.app.Mobile)
			return details;

		return {
			view: "accordionitem",
			template: "Detalle",
			header: "Detalle",
			type: "header",
			id: "detalle_header",
			collapsed: true,
			hidden: true,
			body: details
		};
	}

	hideContents() {
		if (!this.app.Mobile) {
			this.$$("detalle_header").hide();
			this.$$("detail_components").hide();
			this.$$("detail_info").hide();
		}
		this.RefreshDetails(undefined);
	}

	showContents(id) {
		if (!this.app.Mobile) {
			this.$$("detalle_header").show();
			this.$$("detail_components").show();
			this.$$("detail_info").show();
		}
		this.RefreshDetails(id);
	}

	showDetail(id) {
		this.articulo_id = id ? id : null;
		const content = this.$$("detail_components");
		if (content) {
			let childs = content.getChildViews();
			let child_ids = [];
			for (let i = 0; i < childs.length; i++) {
				let child_id = childs[i].config.id;
				child_ids.push(child_id);
			}
			for (let childIdsKey in child_ids) {
				content.removeView(child_ids[childIdsKey]);
			}
		}
		if(this.articulo_id && this.getRoot()) {
			this.showContents(this.articulo_id);
		}
	}

	ready() {
		this.on(this.app.config.state.$changes, "selected_demanda_calculada", id => {
			if (id) {
				this.showDetail(id);
			} else {
				this.hideContents();
			}
		});
	}


	RefreshDetails(id) {
		if (id) {
			var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/articulo/" + id + "/wdetails/");
			const json_response = JSON.parse(xhr.response);
			if ("actions_detail" in json_response && "id" in json_response) {
				const content = this.$$("detail_components");
				if (content) {
					for (const action_key in json_response["actions_detail"]) {
						const action = json_response["actions_detail"][action_key];
						let url = "/home/demandas_calculadas.actions." + action["action"] + "/" + json_response["id"];
						if (action["action"] === "detail") {
							url = "/home/demandas_calculadas." + action["action"] + "/" + json_response["id"];
						}
						content.addView({
							view: "button",
							label: action["label"],
							height: 40,
							click: () => {
								this.show(url);
							}
						}, 0);
					}
				}
			}
			if ("id" in json_response) {
				const sets = this.$$("detail_info");
				if (sets) {
					sets.setValues(json_response);
					if(this._parent.Filters)
						this._parent.Filters.$$("form_header").collapse();
				}

				var xhr_tendency = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/articulo/" + id + "/tendency/");
				this.tendency = JSON.parse(xhr_tendency.response);
				const detail_tendency = this.TendencySection.$$("detail_tendency");
				detail_tendency.setValues(this.tendency);
			}
		}
	}
}

