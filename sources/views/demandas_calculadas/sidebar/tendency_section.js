import {JetView} from "webix-jet";
import "webix/combo_field";

export default class TendencySectionView extends JetView {
	config() {
		return {
			id: "tendency_section",
			rows: [
				{
					id: "tendency_header",
					view: "template",
					template: "Tendencia",
					type: "header",
				},
				{
					autoheight: true,
					view: "property",
					id: "detail_tendency",
					disabled: true,
					nameWidth: 240,
					elements: [
						{label: "Demanda semanal promedio", type: "text", id: "awd"},
						{label: "Pendiente", type: "text", id: "slope"},
						{label: "Crecimiento en periodo", type: "text", id: "growth"},
						{label: "Correlación", type: "text", id: "correlation"}
					],
				},
				{
					view: "form",
					localId: "filterform",
					id: "filter_tendency",
					autoheight: true,
					rows: [
						{
							view: "combo_field",
							name: "periods",
							label: "Periodos",
							options: [
								{value: "4", id: 4},
								{value: "12", id: 12},
								{value: "26", id: 26},
							],
						},
					]
				},
			]
		};
	}

	ready() {
		this.$$("input_periods").setValue(26);
	}
}