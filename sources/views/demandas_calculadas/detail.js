import {JetView, plugins} from "webix-jet";
import Table from "../../helpers/table";
import DemandaCalculadaFieldsView from "./fields";
import DemandasCalculadasWDetailView from "./actions/detail_list";
import ArticuloListDetailView from "./sidebar/detail_list";
import "webix/breadcrumb";
import "webix/combo_field";
import FilterListView from "./sidebar/filters_detail";

var SELECTED_TAB = "";
const DEFAULT_PERIOD = 26;


export default class DemandaCalculadaDetailView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		this.use(plugins.UrlParam, ["id"]);
		this.articulo_id = this.getParam("id");
		this.back_url = "/home/demandas_calculadas";
		SELECTED_TAB = this.app.config.state.selected_tab_demandas_calculadas;

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Demandas calculadas", link: "demandas_calculadas"},
				{value: "Folio #" + this.articulo_id},
				{value: "Detalle"},
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button",
					value: "Regresar",
					css: "webix_secondary",
					height: 40,
					click: () => this.show(this.back_url)
				}
			]
		};

		var details_list = {
			view: "scrollview",
			id: "details_list",
			scroll: "y",
			padding: 24,
			autoheight: true,
			select: true,
			body: {
				rows: [
					{
						view: "datatable",
						id: "datatable_calculadas",
						pager: "pager",
						scroll: "xy",
						select: true,
						columns: [
							{
								id: "id",
								header: "Folio",
								adjust: true
							},
							{
								id: "cliente__display_webix",
								header: "Cliente",
								fillspace: true,
							},
							{
								id: "vendedor__display_webix",
								header: "Vendedor",
								fillspace: true,
							},
							{
								id: "valid__display_webix",
								header: "Estatus",
								fillspace: true,
							},
							{
								id: "date",
								header: {text: "Fecha", css: "right"},
								css: "right"
							},
							{
								id: "quantity",
								header: {text: "Cantidad", css: "right"},
								css: "right"
							},
						],
						on: {
							onAfterRender() {
								this.$scope.TableHelper.afterRender(this, true, "filters_demanda_calculada");
							},
							onAfterSelect: (id) => {
								this.app.config.state.selected_demanda_calculada_detalle = id;
							},
						},
					},
					{
						view: "pager", id: "pager",
						size: 20,
						group: 20,
						count: 20,
						template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
					},
				]
			},
		};

		this.Filters = new FilterListView(this.app);

		var tendency = {
			view: "chart",
			id: "tendency",
			type: "bar",
			barWidth: 15,
			radius: 2,
			borderless: true,
			xAxis: {
				template: "#date#",
				title: "Fecha"
			},
			yAxis: {
				start: 0,
				title: "Ventas"
			},
			legend: {
				values: [
					{text: "Ventas", color: "#d06d99"},
					{text: "Saldos", color: "#5ec5d9"},
					{text: "Tendencia", color: "#ff5700"},
				],
				valign: "middle",
				align: "right",
				layout: "y"
			},
			series: [
				{
					value: "#quantity#",
					color: "#d06d99",
					tooltip: {
						template: "#quantity#"
					}
				},
				{
					value: "#sumatoria_saldos#",
					color: "#5ec5d9",
					tooltip: {
						template: "#sumatoria_saldos#"
					}
				},
				{
					type: "line",
					value: "#predicted_result#",
					item: {
						borderColor: "#ff5700",
						color: "#ff5700",
						type: "r",
						radius: 1
					},
					line: {
						color: "#ff5700",
						width: 1
					},
					tooltip: {
						template: "#complete_date#"
					}
				}
			]
		};

		this.tabs = {
			view: "tabview",
			autoheight: true,
			cells: [
				{
					header: "Detalles",
					id: "detail_view",
					body: details_list
				},
				{
					header: "Tendencia",
					id: "tendency_view",
					body: tendency
				},
			],
			value: SELECTED_TAB
		};

		this.UnitsDetailsView = new DemandasCalculadasWDetailView(this.app);
		if (!this.app.Mobile)
			this.UnitsDetailView = new ArticuloListDetailView(this.app);

		this.fields = new DemandaCalculadaFieldsView(this.app);

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 24,
			rows: [
				this.fields
			],
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					form,
					this.tabs
				]
			};

		return {
			id: "view",
			type: "space",
			cols: [
				{
					rows: [
						search,
						form,
						this.tabs
					],
				},
				{
					view: "accordion",
					width: 384,
					rows: [
						{
							view: "template",
							template: "Acciones",
							type: "header",
						},
						buttons,
						{
							id: "demanda_calculada_header",
							view: "template",
							template: "Demanda calculada acciones",
							type: "header"
						},
						this.Filters,
						this.UnitsDetailsView,
						this.UnitsDetailView,
						{}
					]
				}
			]
		};
	}

	init() {
		const datatable = this.$$("datatable_calculadas");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}

	urlChange() {
		const form = this.$$("form");
		for (var element in form.elements) {
			form.elements[element].disable();
		}
		var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/articulo/" + this.articulo_id + "/");
		this.articulo = JSON.parse(xhr.response);
		form.setValues(this.articulo);
	}

	ready() {
		this.on(this.app.config.state.$changes, "filters_demandas_calculadas_detalle",  () => {
			this.dataLoading();
		});

		var LOADED = false;
		const that = this;
		this.webix.extend(this.UnitsDetailView.TendencySection.$$("tendency_section"), webix.ProgressBar);
		this.webix.extend(this.$$("tendency"), webix.ProgressBar);
		that.UnitsDetailView.TendencySection.$$("tendency_section").hide();
		this.$$("tendency").attachEvent("onViewShow", function () {
			that.app.config.state.selected_tab_demandas_calculadas = "tendency";
			if(!LOADED)
				loadTendency(DEFAULT_PERIOD);
			LOADED = true;
		});

		this.$$("details_list").attachEvent("onViewShow", function () {
			that.app.config.state.selected_tab_demandas_calculadas = "details_list";
			that.UnitsDetailView.TendencySection.$$("tendency_section").hide();
		});

		this.UnitsDetailView.TendencySection.$$("input_periods").attachEvent("onChange", function (period) {
			loadTendency(period);
		});

		if(SELECTED_TAB === "tendency")
			loadTendency(DEFAULT_PERIOD);

		this.app.config.state.selected_demanda_calculada = this.articulo_id;
		this.UnitsDetailView.$$("detalle_header").expand();
		this.UnitsDetailView.$$("detail_components").hide();
		this.UnitsDetailView.$$("spacer").hide();

		function loadTendency(period) {
			that.UnitsDetailView.TendencySection.$$("tendency_section").show();
			that.UnitsDetailView.TendencySection.$$("tendency_section").showProgress();
			that.$$("tendency").showProgress();
			webix.ajax().get(that.app.MasterUrl + "api/mrp/articulo/" + that.articulo_id + "/tendency/?periods=" + period).then(function (response_tendency) {
				that.tendency = response_tendency.json();
				webix.ajax().get(that.app.MasterUrl + "api/mrp/articulo/" + that.articulo_id + "/existencia_cantidad_semanal/?periods=" + period).then(function (response_saldos) {
					that.saldos = response_saldos.json();

					that.tendency.results.forEach(function (data, index) {
						data["sumatoria_saldos"] = that.saldos[index].sumatoria_saldos;
					});

					that.$$("tendency").clearAll();
					that.$$("tendency").parse(that.tendency.results);

					that.UnitsDetailView.TendencySection.$$("detail_tendency").setValues(that.tendency);
					that.UnitsDetailView.TendencySection.$$("tendency_section").hideProgress();
					that.$$("tendency").hideProgress();
				});
			});
		}
	}

	dataLoading() {
		const url = this.app.MasterUrl + "api/mrp/calculated-demand/wlist/?articulo_id=" +  this.articulo_id + "&count=100";
		const table = this.$$("datatable_calculadas");
		const filters = this.Filters;
		let selected_demanda_calculada_detalle = this.app.config.state.selected_demanda_calculada_detalle;
		this.TableHelper.dataLoading(table, url, this, filters, selected_demanda_calculada_detalle, "filters_demandas_calculadas_detalle");
	}
}