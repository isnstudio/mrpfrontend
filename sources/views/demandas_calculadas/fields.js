import {JetView,} from "webix-jet";
import "webix/text_field";
import "webix/decimal_field";
import "webix/server_autocomplete";

export default class ArticuloFormFieldsView extends JetView {
	config() {

		const cantidad_stock = {
			view: "decimal_field",
			name: "stock",
			label: "Cantidad de stock"
		};

		const semanas_inventario = {
			view: "decimal_field",
			name: "inventory_weeks",
			label: "Semanas de inventario",
		};

		const reorden = {
			view: "checkbox",
			name: "es_reorden",
			id: "input_es_reorden",
			label: "Reorden",
			labelPosition: "top",
			value: true
		};

		const responsive_view = {
			margin: 10,
			rows: [
				cantidad_stock,
				semanas_inventario,
				reorden
			]
		};

		const standard_view = {
			margin: 10,
			rows: [
				{
					cols: [
						{
							margin: 10,
							rows: [
								cantidad_stock,
								reorden
							]
						},
						{
							margin: 10,
							rows: [
								semanas_inventario
							]
						}
					]
				},
			],

		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);

		return {
			margin: 10,
			rows: [
				main_section,
			]
		};
	}
}
