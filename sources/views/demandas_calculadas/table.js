import {JetView} from "webix-jet";
import Table from "../../helpers/table";

export default class ArticuloTableView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		const datatable = {
			view: "datatable",
			id: "grid",
			localId: "data",
			select: true,
			pager: "pager",
			scroll: "xy",
			rowHeight: 40,
			columns: [
				{
					id: "id",
					header: "Folio",
					width: 80
				},
				{
					id: "clave",
					header: "Clave",
					fillspace: true,
				},
				{
					id: "unidad_medida__display_webix",
					header: {"text": "UM", css: "right"},
					adjust: true,
					css: "right",
				},
				{
					id: "average_weekly_demand",
					header: {"text": "AWD", css: "right"},
					adjust: true,
					css: "right",
					minWidth: 100
				},
				{
					id: "current_manual_demand",
					header: {"text": "MWD", css: "right"},
					adjust: true,
					css: "right",
					minWidth: 100
				},
				{
					id: "stock",
					header: {"text": "Stock", css: "right"},
					adjust: true,
					css: "right",
					minWidth: 100
				},
				{
					id: "inventory_weeks",
					header: {"text": "Semanas inventario", css: "right"},
					adjust: true,
					css: "right",
					minWidth: 180
				}
			],
			on: {
				onAfterRender() {
					this.$scope.TableHelper.afterRender(this, true, "filters_demandas_calculadas");
				},
				onAfterSelect: (id) => {
					if(!this.app.Mobile) {
						this._parent.UnitsDetailView.$$("detalle_header").expand();
						this._parent.UnitsDetailView.$$("tendency_detail").show();
					}
					this.app.config.state.selected_demanda_calculada = id;

					const DEFAULT_PERIOD = 26;
					const that = this;
					loadTendency(DEFAULT_PERIOD);
					this._parent.UnitsDetailView.TendencySection.$$("input_periods").attachEvent("onChange", function (period) {
						loadTendency(period);
					});

					function loadTendency(period) {
						var xhr_tendency = webix.ajax().sync().get(that.app.MasterUrl + "api/mrp/articulo/" + id + "/tendency/?periods=" + period);
						that.tendency = JSON.parse(xhr_tendency.response);
						that._parent.UnitsDetailView.$$("tendency_detail").clearAll();
						that._parent.UnitsDetailView.$$("tendency_detail").parse(that.tendency.results);

						that._parent.UnitsDetailView.TendencySection.$$("detail_tendency").setValues(that.tendency);
					}
				},
			}
		};


		return {
			rows: [
				datatable,
				{
					view: "pager",
					id: "pager",
					size: 20,
					group: 20,
					count: 20,
					template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
				},
			],
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}
}