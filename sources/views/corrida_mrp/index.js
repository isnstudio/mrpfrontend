import {JetView, plugins} from "webix-jet";

import TableView from "./table";
import ListView from "./responsive/table";
import FilterListView from "./sidebar/filters";
import CorridaMRPWDetailView from "./sidebar/detail_list";
import Table from "../../helpers/table";
import SaveService from "../../services/save";
import "webix/breadcrumb";

export default class CorridaMRPView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["id"]);
		this.corrida_mrp_id = this.getParam("id");
		this.TableHelper = new Table(this.app);
		this.Table = new (this.app.Mobile ? ListView : TableView)(this.app);
		this.Filters = new FilterListView(this.app);
		if (!this.app.Mobile)
			this.UnitsDetailView = new CorridaMRPWDetailView(this.app);

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Corrida MRP"},
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb,
				{
					view: "icon",
					icon: "mdi mdi-plus",
					localId: "addButton",
					click: () => {
						this.show("corrida_mrp.actions.add");
					}
				},
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					{
						view: "accordion",
						rows: [
							this.Filters,
							search,
							this.Table,
							{$subview: true, popup: true}
						]
					}
				]
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						this.Table,
					]
				},
				{
					width: 384,
					view: "accordion",
					rows: [
						this.Filters,
						this.UnitsDetailView
					]
				},
			]
		};
	}

	ready(view) {
		const datagrid = view.$scope.Table.$$("data");
		this.TableHelper.pagerParams(view, datagrid,"filters_corrida_mrp", this);

		if(this.app.Mobile) {
			this.Filters.$$("form_header").collapse();
		} else this.Filters.$$("form_header").expand();

		this.on(this.app.config.state.$changes, "filters_corrida_mrp",  () => {
			this.dataLoading();
		});

		if(this.corrida_mrp_id) {
			const request_options = {
				url: this.app.MasterUrl + "api/mrp/mrp-articulo-corrida/" + this.corrida_mrp_id + "/procesar_detalles/",
				get_values: {},
				validate: true,
				message: "Procesando detalles",
				context: this,
				method: "post",
				path: "corrida_mrp"
			};
			this.SaveService = new SaveService(this.app);
			this.SaveService.save(request_options);
		}
	}

	dataLoading() {
		const url = this.app.MasterUrl + "api/mrp/mrp-articulo-corrida/wlist/?order_by=-id";
		const table = this.Table.$$("data");
		const filters = this.Filters;
		let selected_corrida_mrp = this.app.config.state.selected_corrida_mrp;
		let filter_default = "";
		this.TableHelper.dataLoading(table, url, this, filters, selected_corrida_mrp, "filters_corrida_mrp", filter_default);
	}
}
