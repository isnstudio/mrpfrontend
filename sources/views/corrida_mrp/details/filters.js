import {JetView} from "webix-jet";
import Filters from "../../../helpers/filters";
import "webix/text_field";
import "webix/server_autocomplete";
import "webix/combo_field";

export default class FilterDetailsListView extends JetView {
	config() {
		this.Filters = new Filters(this.app);

		return {
			view: "form",
			localId: "filterform",
			id: "filter_form",
			autoheight: true,
			rows: [
				{
					view: "checkbox",
					name: "errores",
					id: "input_errores",
					label: "¿Tiene errores?",
					labelPosition: "top"
				},
				{
					view: "server_autocomplete",
					name: "articulo",
					label: "Artículo",
					url: this.app.MasterUrl + "api/mrp/articulo/select_list/"
				},
				{
					view: "button",
					value: "submit",
					label: "Buscar",
					type: "icon", icon: "mdi mdi-filter",
					height: 40,
					click: () => {
						this.Filters.filterParams(this, "filters_corrida_mrp_detalle");
					}
				},
				{
					id: "cleanfilter",
					view: "button",
					value: "submit",
					label: "Limpiar filtro",
					type: "icon",
					icon: "mdi mdi-sync",
					height: 40,
					click: () => {
						this.Filters.cleanFilter(this, "filters_corrida_mrp_detalle");
						this._parent.dataLoading();
					}
				},
				{}
			]
		};
	}
}