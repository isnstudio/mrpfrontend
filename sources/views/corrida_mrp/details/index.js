import {JetView, plugins} from "webix-jet";
import CorridaMRPDetailsTableView from "./table";
import Table from "../../../helpers/table";
import "webix/breadcrumb";
import FilterDetailsListView from "./filters";

export default class CorridaMRPDetailView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		this.use(plugins.UrlParam, ["id"]);
		this.corrida_mrp_id = this.getParam("id");
		this.back_url = "/home/corrida_mrp";
		this.Filters = new FilterDetailsListView(this.app);

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Corrida MRP", link: "corrida_mrp"},
				{value: "Folio #" + this.corrida_mrp_id},
				{value: "Detalle"},
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button",
					value: "Regresar",
					css: "webix_secondary",
					height: 40,
					click: () => this.show(this.back_url)
				}
			]
		};

		this.Table = new CorridaMRPDetailsTableView(this.app);

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					this.Table,
				]
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						{
							view: "scrollview",
							scroll: "y",
							body: {
								rows: [
									this.Table
								]
							},
						}
					],
				},
				{
					width: 384,
					rows: [
						{
							view: "template",
							template: "Acciones",
							type: "header",
						},
						buttons,
						{
							view: "template",
							template: "Filtros",
							type: "header",
						},
						this.Filters,
					]
				}
			]
		};
	}

	ready(view) {
		const datagrid  = view.$scope.Table.$$("data");
		this.TableHelper.pagerParams(view, datagrid, "filters_corrida_mrp_detalle", this);

		this.on(this.app.config.state.$changes, "filters_corrida_mrp_detalle",  () => {
			this.dataLoading();
		});
	}

	dataLoading() {
		const url = this.app.MasterUrl +
			"api/mrp/mrp-articulo-corrida-detalle/wlist/?mrp_articulo_corrida=" + this.corrida_mrp_id;
		const table = this.Table.$$("data");
		const filters = this.Filters;
		let filter_default = "";
		let selected_corrida_mrp_detalle = this.app.config.state.selected_corrida_mrp_detalle;
		this.TableHelper.dataLoading(table, url, this, filters, selected_corrida_mrp_detalle, "filters_corrida_mrp_detalle", filter_default);
	}
}