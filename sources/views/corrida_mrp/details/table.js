import {JetView} from "webix-jet";
import Table from "../../../helpers/table";

export default class CorridaMRPDetailsTableView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		const datatable = {
			view: "datatable",
			id: "data",
			pager: "pager",
			scroll: "xy",
			rowHeight: 40,
			columns: [
				{
					id: "articulo__display_webix",
					header: "Artículo",
					minWidth: 50,
					adjust: true
				},
				{
					id: "processed",
					header: "Procesado",
					minWidth: 120,
					fillspace: true
				},
				{
					id: "text_error",
					header: "Text error",
					minWidth: 100,
					adjust: true
				},
			],
			on: {
				onAfterRender() {
					this.$scope.TableHelper.afterRender(this, true);
				}
			}
		};


		return {
			rows: [
				datatable,
				{
					view: "pager",
					id: "pager",
					size: 20,
					group: 20,
					count: 20,
					template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
				},
			],
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}
}
