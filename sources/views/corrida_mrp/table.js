import {JetView} from "webix-jet";
import Table from "../../helpers/table";

export default class CorridaMRPTableView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		const datatable = {
			view: "datatable",
			id: "grid",
			localId: "data",
			select: true,
			pager: "pager",
			scroll: "xy",
			rowHeight: 40,
			columns: [
				{
					id: "id",
					header: "Folio",
					minWidth: 50,
					fillspace: true
				},
				{
					id: "get_estatus_display",
					header: "Estatus",
					minWidth: 100,
					fillspace: true
				},
				{
					id: "fecha_corrida",
					header: "Fecha corrida",
					minWidth: 100,
					fillspace: true
				},
				{
					id: "fecha_termino",
					header: "Fecha termino",
					minWidth: 100,
					fillspace: true
				},
				{
					id: "articulos_totales",
					header: "Articulos totales",
					minWidth: 100,
					fillspace: true
				},
				{
					id: "articulos_procesados",
					header: "Articulos procesados",
					minWidth: 100,
					fillspace: true
				},
				{
					id: "errores_message",
					header: "Errores",
					minWidth: 100,
					fillspace: true
				}
			],
			on: {
				onAfterRender() {
					this.$scope.TableHelper.afterRender(this, true);
				},
				onAfterSelect: id => this.app.config.state.selected_corrida_mrp = id
			}
		};


		return {
			rows: [
				datatable,
				{
					view: "pager",
					id: "pager",
					size: 20,
					group: 20,
					count: 20,
					template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
				},
			],
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}
}