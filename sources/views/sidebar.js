import {JetView, plugins} from "webix-jet";
import AuthService from "../services/auth";

export default class SidebarView extends JetView {
	config() {
		const user_service = this.app.getService("auth");

		const menu_mapping = [
			{id: "articulos", value: "Artículos", icon: "mdi mdi-archive-star-outline"},
			{id: "demandas_calculadas", value: "Demandas calculadas", icon: "mdi mdi-file-chart-outline"},
			{id: "demandas_manuales", value: "Demandas manuales", icon: "mdi mdi-chart-box-plus-outline"},
			{id: "analisis_articulos", value: "Análisis por artículo", icon: "mdi mdi-archive-star-outline"},
			{id: "requisiciones_material", value: "Requisiciones de material", icon: "mdi mdi-file-document-multiple-outline"},
			{id: "admin_partidas", value: "Administración de partidas", icon: "mdi mdi-file-document-multiple-outline"},
			{id: "corrida_mrp", value: "Corrida MRP", icon: "mdi mdi-chart-timeline"},
			{id: "parametros", value: "Parametros", icon: "mdi mdi-store-check"},
			{id: "reportes", value: "Reportes", icon: "mdi mdi-chart-bar"},
			{id: "file_export", value: "File Export", icon: "mdi mdi-database-export"},
		];

		const sidebar_menu = {
			localId: "nav",
			view: "sidebar",
			css: "webix_dark",
			collapsed: true,
			minWidth: 200,
			data: [],
			on: {
				onItemClick: function (id) {
					if (id === "login") {
						const auth = new AuthService();
						auth.logout();
					}
					if (isNaN(id) && this.$scope.app.Mobile) {
						this.hide();
					}
				}
			}
		};

		for (let i = 0; i < menu_mapping.length; i++) {
			const menu_id = menu_mapping[i].id;
			if (user_service.hasAccess(menu_id)) {
				sidebar_menu.data.push(menu_mapping[i]);
			}
		}

		const logout = {id: "login", value: "Cerrar sesión", icon: "mdi mdi-logout-variant"};
		sidebar_menu.data.push(logout);

		return sidebar_menu;
	}

	init() {
		this.use(plugins.Menu, {
			id: "nav",
			urls: {
				reports: "reports"
			}
		});
	}
}
