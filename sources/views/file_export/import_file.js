import {JetView, plugins} from "webix-jet";
import "webix/breadcrumb";
import "webix/button_service";
import Errors from "../../helpers/errors";

export default class ArticuloAddView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["format", "reporte"]);
		this.format = this.getParam("format");
		this.reporte = this.getParam("reporte");

		this.back_url = "/home/file_export";

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "File Export", link: this.back_url},
				{value: "Importar formato"}
			],
		};

		const that = this;

		const file = {
			view: "uploader",
			value: "Seleccionar Archivo",
			id: "file_uploader",
			multiple: false,
			autosend: true,
			name: "documento",
			height: 40,
			link: "file_list",
			required: true,
			upload: this.app.MasterUrl + "mrp/import_export/preview_import_file/?resource_file=" + this.reporte + "&format_file=" + this.format,
			on: {
				onFileUploadError: function (file) {
					if(file.status === "error")
						webix.message({
							text: "Error al subir archivo",
							type: "error",
							expire: -1,
						});
					const view =  that.getSubView().getRoot();
					const top = view.getTopParentView();
					top.hideProgress();
				},
				onFileUpload: function (res) {
					const view =  that.getSubView().getRoot();
					const top = view.getTopParentView();
					top.hideProgress();

					if(!res.errors) {
						var table_html = "";
						if(res.fields){
							table_html += "<table><thead><tr>";
							res.fields.forEach(col => {
								table_html += "<th>"+ col +"</th>";
							});
							table_html += "</tr></thead>";
						}
						if(res.results){
							table_html += "<tbody>";
							res.results.forEach(row => {
								table_html += "<tr>";
								row.forEach(value => {
									table_html += "<td>" + value + "</td>";
								});
								table_html += "</tr>";
							});
							table_html += "</tbody></table>";
						}

						that.$$("form").addView({
							view: "scrollview",
							scroll: "xy",
							body: {
								view: "template",
								template: table_html,
								borderless: true,
								width: 2500,
								autoheight: true
							},
							borderless: true
						});

						that.$$("aplicar_btn").show();

						that.file_name = res.file_name;
					} else {
						var template_error = "";
						res.errors.forEach(e => {
							template_error +="<p style='text-align: center; color: #A93226; background-color: #FFA07A;'>"+ e +"</p>";
						});
						that.$$("form").addView({
							view: "scrollview",
							scroll: "xy",
							body: {
								view: "template",
								template: template_error,
								borderless: true,
								autoheight: true
							},
							borderless: true
						});
					}
				}
			}
		};

		const file_list = {
			view: "list",
			id: "file_list",
			type: "uploader",
			autoheight: true,
			borderless: true
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			id: "buttons",
			rows: [
				{
					view: "button",
					value: "Aplicar",
					css: "webix_primary",
					height: 40,
					id: "aplicar_btn",
					click: () => {
						this.aplicar();
					},
					hidden: true,
				},
				{
					view: "button",
					value: "Regresar",
					css: "webix_secondary",
					height: 40,
					click: () => {
						this.show(this.back_url);
					}
				}
			]
		};

		this.form = {
			view: "form",
			id: "form",
			padding: 24,
			rows: [
				file,
				file_list
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					this.form,
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							this.form,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384
							},
							buttons
						]
					}
				]
			}
		};
	}

	ready() {
		const that = this;
		webix.attachEvent("onBeforeAjax", function (mode, url, params, xhr, headers, files, defer) {
			if(that.app) {
				setTimeout(function () {
					const view =  that.getSubView().getRoot();
					const top = view.getTopParentView();
					that.webix.extend(top, webix.OverlayBox);
					that.webix.extend(top, webix.ProgressBar);
					top.showProgress();
				}, 200);
			}
		});
	}

	aplicar() {
		const that = this;
		const ajax = webix.ajax().headers({
			"Content-type":"application/json"
		}).post(this.app.MasterUrl + "mrp/import_export/confirm_import_file/?resource_file=" +
			this.reporte + "&format_file=" + this.format, {"file_name": this.file_name});
		ajax.then(function () {
			const view =  that.getSubView().getRoot();
			const top = view.getTopParentView();
			top.hideProgress();
			webix.message("Aplicado");
			that.show(that.back_url);
		}).fail(function (res) {
			const error = new Errors();
			error.show_error(res);
		});
	}
}