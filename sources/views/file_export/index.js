import {JetView} from "webix-jet";
import FormFieldsView from "./fields.js";
import "webix/breadcrumb";
import Download_csv from "helpers/download_csv";

export default class FileExportView extends JetView {
	config() {
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "File Export"}
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button",
					value: "Descargar formato",
					css: "webix_primary",
					height: 40,
					id: "save",
					click: () => {
						this.action();
					}
				},
				{
					view: "button",
					value: "Importar formato",
					id: "button_import",
					css: "webix_primary",
					height: 40,
					click: () => {
						this.showParams();
					}
				}
			]
		};

		this.fields = new FormFieldsView(this.app);

		this.form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 20,
			rows: [
				this.fields,
				{}
			],
			rules: {}
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					rows: [
						search,
						buttons,
						this.form,
					]
				}
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							this.form,
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}

	ready() {
		const that = this;
		this.fields.$$("resource_file").attachEvent("onChange", function (value) {
			if(value) {
				showFields(["resource_file", "format_file"]);
			}
		});

		function showFields(fields) {
			const form = that.$$("form");
			for (var element in form.elements) {
				const find = fields.find(value => value !== element);
				if(!find){
					form.elements[element].disable();
				}
				form.elements[element].hide();
			}
			fields.forEach(value => {
				that.fields.$$(value).show();
			});
		}
	}

	action() {
		const modelo = this.fields.$$("resource_file").getValue();
		const that = this;
		const format = that.fields.$$("format_file").getText();
		var name = "";
		switch (modelo) {
			case "1": {
				name = "Formato lead times";
				break;
			}
			case "2": {
				name = "Formato Articulo";

				break;
			}
			case "3": {
				name = "Formato Manual Demand";
				break;
			}
			default: break;
		}
		const download_csv = new Download_csv();
		const get_values = this.$$("form").getValues();
		const url = this.app.MasterUrl + "mrp/import_export/export_file/?format_file=" +
			get_values.format_file + "&resource_file=" + get_values.resource_file;
		const validate = this.$$("form").validate();
		download_csv.download_file(url, get_values, this, validate, name, format);
	}

	showParams() {
		const validate = this.$$("form").validate();
		const get_values = this.$$("form").getValues();
		const format = this.fields.$$("format_file").getText();
		if(validate)
			this.show("/home/file_export.import_file/" + format +"/" + get_values.resource_file);
		else {
			webix.message({
				text: "Faltan campos por llenar",
				type: "error",
				expire: -1,
				id: "messageLead"
			});
		}
	}
}