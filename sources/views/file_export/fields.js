import {JetView,} from "webix-jet";
import "webix/datepicker_iso";
import "webix/choice_field";

export default class FormFieldsView extends JetView {
	config() {
		var xhr = webix.ajax().sync().get(this.app.MasterUrl + "mrp/import_export/get_file_format/");
		const formats = JSON.parse(xhr.response);
		formats.forEach(format => {
			format.id = format.value;
		});

		const reporte = {
			view: "choice_field",
			id: "resource_file",
			name: "resource_file",
			label: "Modelo",
			labelPosition: "top",
			placeholder: "Modelo",
			suggest: {url: this.app.MasterUrl + "mrp/import_export/get_model_resource/"},
			required: true
		};

		const formats_field = {
			view: "choice_field",
			name: "format_file",
			id: "format_file",
			label: "Formato",
			labelPosition: "top",
			placeholder: "Formato",
			suggest: formats,
			hidden: true,
			required: true
		};

		const responsive_view = {
			margin: 10,
			rows: [
				reporte,
				formats_field,
			]
		};

		const standard_view = {
			margin: 10,
			rows: [
				reporte,
				formats_field,
			],
		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);
		return {
			margin: 10,
			rows: [
				main_section
			]
		};
	}
}