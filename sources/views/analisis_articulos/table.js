import {JetView} from "webix-jet";
import Table from "../../helpers/table";

export default class ArticuloTableView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		const datatable = {
			view: "datatable",
			id: "data",
			select: true,
			scroll: "xy",
			rowHeight: 40,
			columns: [
				{
					id: "folio",
					header: "Folio",
					fillspace: true,
					minWidth: 60
				},
				{
					id: "week",
					header: "Semana",
					adjust: true,
				},
				{
					id: "date",
					header: "Fecha",
					adjust: true,
				},
				{
					id: "movement_type",
					header: "Tipo",
					adjust: true,
					minWidth: 90,
				},
				{
					id: "info",
					header: "Info",
					adjust: true,
				},
				{
					id: "fecha_confirmada",
					header: "Fecha confirmada",
					adjust: true,
					minWidth: 160,
					template: function (obj) {
						switch (obj.fecha_confirmada) {
							case "0": return "<span class='webix_icon mdi mdi-close-circle red'></span>";
							case "1": return "<span class='webix_icon mdi mdi-check-circle green'></span>";
							default: return "";
						}
					}
				},
				{
					id: "requisition_date",
					header: "Fecha requisición",
					minWidth: 180,
					adjust: true
				},
				{
					id: "expedition_date",
					header: "Expeditar",
					minWidth: 180,
					adjust: true,
					template: function(obj){
						if (obj.quantity < 0 && obj.expedition_date !== "-")
							return `<span style='color:orange'>${obj.expedition_date}</span>`;
						return obj.expedition_date;
					}
				},
				{
					id: "exception_message",
					header: "Mensaje de excepción",
					minWidth: 180,
					adjust: true
				},
				{
					id: "quantity",
					header: {"text": "Cantidad", css: "right"},
					adjust: true,
					css: "right",
				},
				{
					id: "balance",
					header: {"text": "Balance", css: "right"},
					adjust: true,
					css: "right",
				},
				{
					id: "proveedor",
					header: "Proveedor",
					adjust: true,
					minWidth: 100
				}
			],
			scheme:{
				$change:function(item){
					if (item.color !== null)
						item.$css = {"background-color": item.color};
				}
			},
			on: {
				onAfterRender() {
					this.$scope.TableHelper.afterRender(
						this,
						"No hay información para mostrar, verifique los filtros o genere un análisis.",
						"filters_articulo"
					);
				},
			}
		};


		return {
			rows: [
				datatable
			],
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}
}