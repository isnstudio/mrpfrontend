import {JetView} from "webix-jet";
import Download_csv from "../../helpers/download_csv";
import Table from "../../helpers/table";
import FilterListView from "./sidebar/filters";
import ArticuloTableView from "./table";
import "webix/breadcrumb";


export default class ArticulosView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		this.Table = new (ArticuloTableView)(this.app);
		this.Filters = new FilterListView(this.app);

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Analisis por artículo"},
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb,
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					{
						view: "accordion",
						rows: [
							this.Filters,
							search,
							this.Table,
						]
					}
				]
			};



		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						this.Table,
					]
				},
				{
					width: 384,
					rows: [
						{
							view: "template",
							template: "Filters",
							type: "header",
							width: 384
						},
						this.Filters,
						{
							view: "template",
							id: "detail_header",
							template: "Detalles",
							type: "header",
							width: 384
						},
						{
							view: "scrollview",
							scroll: "y",
							id: "detail_analisis_body",
							body: {
								rows: [
									{
										view: "property",
										id: "detail_analisis",
										disabled: true,
										nameWidth: 240,
										height: 362,
										elements: [
											{label: "Fecha de cálculo", type: "text", id: "date"},
											{label: "Usuario", type: "text",id: "created_by"},
											{label: "Comprador", type: "text", id: "comprador"},
											{label: "Unidad de medida", type: "text", id: "unidad_medida"},
											{label: "Semanas inventario", type: "text", id: "inventory_weeks"},
											{label: "Lead time", type: "text", id: "lead_time__display_webix"},
											{label: "Tamaño de lote", type: "text", id: "lot_size"},
											{label: "Artículo conversión", type: "text", id: "articulo_conversion",
												format: function (value) {
													return value ? "Si" : "No";
												}
											},
											{label: "Stock consolidado", type: "text", id: "stock"},
											{label: "Punto de pedido", type: "text", id: "reorder_point"},
											{label: "Lead time consumption", type: "text", id: "lead_time_consumption"},
											{label: "Stock máximo", type: "text", id: "max_stock"},
											{label: "Clave artículo", type: "text", id: "articulo_clave"}
										],
									},
								]
							}
						},
						{
							view: "template",
							id: "ordenes_header",
							template: "Ordenes de compra sin fecha",
							type: "header",
							width: 384
						},
						{
							view: "datatable",
							id: "ordenes_datatable",
							select: true,
							scroll: "xy",
							rowHeight: 40,
							columns: [
								{
									id: "folio",
									header: "Folio",
									fillspace: true,
									minWidth: 60
								},
								{
									id: "unidad_medida",
									header: {"text": "UM", css: "right"},
									minWidth: 180,
									template: "#cantidad# #unidad_medida#",
									css: "right",
									adjust: true
								},
								{
									id: "estatus",
									header: "Estatus",
									minWidth: 120,
									adjust: true,
								},
							],
						},
						{
							id: "action_ordenes",
							hidden: true,
							rows: [
								{
									height: 45,
									view: "button",
									id: "save",
									css: "webix_primary",
									value: "Descargar reporte OC csv",
								}
							]
						},
						{
							id: "empty_row"
						}
					]
				},
			]
		};
	}

	ready() {
		this.on(this.app.config.state.$changes, "selected_analisis_articulo",  () => {
			this.dataLoading(this.app.config.state.selected_analisis_articulo);
		});

		if(this.app.config.state.selected_analisis_articulo) {
			this.dataLoading(this.app.config.state.selected_analisis_articulo);
			this.Filters.$$("btn_generate").show();
			this.Filters.$$("cleanfilter").show();
			this.Filters.$$("input_articulo").disable();
		}
	}

	dataLoading(selected_articulo) {
		const that = this;
		if(selected_articulo) {
			const url = this.app.MasterUrl + "api/mrp/articulo/"+ selected_articulo +"/mrp_articulo_details/";
			const table = this.Table.$$("data");
			var xhr = webix.ajax().sync().get(url);
			const json_response = JSON.parse(xhr.response);
			table.clearAll();
			table.parse(json_response);

			const url_details = this.app.MasterUrl + "api/mrp/articulo/"+ selected_articulo +"/mrp_articulo_date/";
			var xhr_details = webix.ajax().sync().get(url_details);
			const details = JSON.parse(xhr_details.response);

			this.$$("detail_analisis").setValues(details[0]);
			this.$$("detail_analisis_body").show();
			this.$$("detail_header").show();

			const url_ordenes = this.app.MasterUrl + "api/mrp/articulo/"+ selected_articulo +"/orden_compra_detalles_view/";
			var xhr_ordenes = webix.ajax().sync().get(url_ordenes);
			const ordenes = JSON.parse(xhr_ordenes.response);

			this.$$("ordenes_datatable").clearAll();
			this.$$("ordenes_datatable").parse(ordenes);
			this.$$("ordenes_header").show();
			this.$$("ordenes_datatable").show();

			this.$$("save").attachEvent("onItemClick", function () {
				const url = "api/mrp/articulo/"+ selected_articulo +"/report_from_orden_compra_detalles/";
				const download_csv = new Download_csv();
				download_csv.download_csv(url, {}, this, that, true, "Reporte ordenes de compra");
			});
			this.$$("action_ordenes").show();

			this.$$("empty_row").hide();
		} else {
			this.Table.$$("data").clearAll();
			this.$$("detail_analisis_body").hide();
			this.$$("detail_header").hide();

			this.$$("ordenes_header").hide();
			this.$$("ordenes_datatable").hide();

			this.$$("action_ordenes").hide();

			this.$$("empty_row").show();
		}
	}
}
