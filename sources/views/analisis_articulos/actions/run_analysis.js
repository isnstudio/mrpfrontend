import {JetView, plugins} from "webix-jet";
import AnalysisFieldsView from "./fields";
import "webix/breadcrumb";
import "webix/button_service";

export default class RunAnalysisView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["id"]);
		this.articulo_id = this.getParam("id");
		this.back_url = "/home/analisis_articulos/" + this.articulo_id;

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Analisis por artículo", link: "analisis_articulos"},
				{value: "Generar análisis"},
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button_service",
					label: "Generar análisis",
					height: 40,
					path: this.back_url,
					method: "post",
					message: "Artículo guardado",
					url:  this.app.MasterUrl + "api/mrp/articulo/" + this.articulo_id + "/run_analysis/",
				},
				{
					view: "button",
					value: "Regresar",
					css: "webix_secondary",
					height: 40,
					click: () => {
						this.show(this.back_url);
					}
				}
			]
		};

		this.fields = new AnalysisFieldsView(this.app);

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 24,
			rows: [
				this.fields,
				{
					id: "empty_chart",
					borderless: true,
					height: 50,
					template: "<p style='text-align: center; color: #9F6000; background-color: #FEEFB3;'>Los valores modificados en este formulario serán actualizados en el maestro del artículo.</p>"
				}
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					form,
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					},
					{
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
								width: 384
							},
							buttons
						]
					}
				]
			}
		};
	}

	ready() {
		const form = this.$$("form");
		if (this.articulo_id) {
			var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/mrp-articulo/init/?articulo=" + this.articulo_id);
			this.articulo = JSON.parse(xhr.response);
			form.setValues(this.articulo);
		}

		this.fields.$$("input_lead_time").define("url", this.app.MasterUrl + "api/mrp/lead-time/select_list/?param=" + this.articulo_id);
		this.fields.$$("input_lead_time").refresh();
	}
}