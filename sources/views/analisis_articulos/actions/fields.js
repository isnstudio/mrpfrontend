import {JetView,} from "webix-jet";
import "webix/text_field";
import "webix/number_field";
import "webix/server_autocomplete";

export default class MrpArticuloFormFieldsView extends JetView {
	config() {
		const lead_time = {
			view: "server_autocomplete",
			name: "lead_time",
			label: "Lead time",
			labelPosition: "top",
			placeholder: "Lead time",
			url: this.app.MasterUrl + "api/mrp/lead-time/select_list/",
		};

		const articulo_conversion = {
			view: "checkbox",
			name: "articulo_conversion",
			id: "input_articulo_conversion",
			label: "Artículo conversión",
			labelPosition: "top",
		};

		const lot_size = {
			view: "number_field",
			name: "lot_size",
			label: "Tamaño de lote"
		};

		const reorden = {
			view: "checkbox",
			name: "es_reorden",
			id: "input_es_reorden",
			label: "Reorden",
			labelPosition: "top",
			value: true,
			disabled: true
		};

		const responsive_view = {
			margin: 10,
			rows: [
				lead_time,
				articulo_conversion,
				lot_size,
				reorden
			]
		};

		const standard_view = {
			margin: 10,
			rows: [
				{
					cols: [
						{
							margin: 10,
							rows: [
								lead_time,
								articulo_conversion,
							]
						},
						{
							margin: 10,
							rows: [
								lot_size,
								reorden
							]
						}
					]
				},
			],

		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);

		return {
			margin: 10,
			rows: [
				main_section,
			]
		};
	}
}