import {JetView, plugins} from "webix-jet";
import Filters from "../../../helpers/filters";
import "webix/text_field";
import "webix/server_autocomplete";

export default class FilterListView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["id"]);
		this.articulo_id = this.getParam("id");
		this.Filters = new Filters(this.app);

		var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/articulo/init/");
		this.user_data = JSON.parse(xhr.response);

		var url_comprador = this.app.MasterUrl + "api/cat/user/select_list/";
		var url_articulo = this.app.MasterUrl + "api/mrp/articulo/select_list/?filter=comprador==" + this.user_data.id;

		if(this.app.config.state.selected_analisis_articulo_text)
			url_articulo =	this.app.MasterUrl + "api/mrp/articulo/select_list/?param=" +
				this.app.config.state.selected_analisis_articulo_text;

		return {
			view: "form",
			localId: "filterform",
			id: "filter_form",
			autoheight: true,
			rows: [
				{
					view: "server_autocomplete",
					id: "input_comprador",
					name: "comprador",
					label: "Comprador",
					url: url_comprador,
				},
				{
					view: "server_autocomplete",
					id: "input_articulo",
					name: "articulo",
					label: "Artículo",
					url: url_articulo,
				},
				{
					view: "button",
					value: "submit",
					label: "Buscar",
					type: "icon", icon: "mdi mdi-filter",
					height: 40,
					click: () => {
						this.app.config.state.selected_analisis_articulo_text = this.$$("input_articulo").getText();
						this.app.config.state.selected_analisis_articulo = this.$$("input_articulo").getValue();
						const selected_articulo = this.app.config.state.selected_analisis_articulo;
						if (selected_articulo) {
							this.$$("btn_generate").show();
							this.$$("cleanfilter").show();
							this.$$("input_articulo").disable();
						}
					}
				},
				{
					id: "cleanfilter",
					view: "button",
					value: "submit",
					label: "Limpiar filtro",
					type: "icon",
					icon: "mdi mdi-sync",
					height: 40,
					hidden: true,
					click: () => {
						this.$$("filter_form").clear();
						this.app.config.state.selected_analisis_articulo = 0;
						this.articulo_id = 0;
						this.$$("btn_generate").hide();
						this.$$("cleanfilter").hide();
						this.$$("input_articulo").enable();
						this.show("/home/analisis_articulos/");
					}
				},
				{
					view: "button",
					value: "submit",
					id: "btn_generate",
					label: "Generar análisis",
					css: "webix_primary",
					height: 40,
					hidden: true,
					click: () => {
						this.Filters.filterParams(this, "filters_analisis_articulo");
						const selected_articulo = this.articulo_id ? this.articulo_id : this.app.config.state.selected_analisis_articulo;
						this.show("/home/analisis_articulos.actions.run_analysis/" + selected_articulo);
					}
				},
				{}
			]
		};
	}

	ready() {
		var that = this;
		if(this.articulo_id)
			this.$$("input_articulo").setValue(this.articulo_id);

		this.$$("input_comprador").attachEvent("onChange", function(comprador_id){
			if(comprador_id.id)
				comprador_id = comprador_id.id;
			var url_articulo = that.app.MasterUrl + "api/mrp/articulo/select_list/?filter=comprador==" + comprador_id;
			that.$$("input_articulo").define("url", url_articulo);
			that.$$("input_articulo").refresh();
		});

		if(this.user_data){
			that.user_data.comprador = that.user_data.id;
			that.user_data.comprador__display_webix = that.user_data.value;
			that.$$("filter_form").setValues(that.user_data);
			that.$$("cleanfilter").show();
		}
	}
}