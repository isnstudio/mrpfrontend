import {JetView} from "webix-jet";


export default class ModalView extends JetView {
	constructor(app, config) {
		super(app, config);
		this.Header = config.header;
		this.Body = config.body;
		this.Modal = config.modal;
	}

	config() {
		let body = this.Body;

		let head = this.Header || false;

		const win = {
			view: "window", borderless: true, css: "edit_window",
			modal: this.Modal || false,
			head,
			body
		};

		if (this.app.Mobile) {
			win.fullscreen = true;
		} else {
			win.width = 396;
			win.height = document.body.offsetHeight;
			win.position = function (state) {
				state.top = 0;
				state.left = document.body.offsetWidth - win.width;
			};
		}

		return win;
	}

	init() {
		this._resizeHandler = this.webix.event(window, "resize", () => {
			const win = this.getRoot();
			win.config.height = document.body.offsetHeight;
			win.resize();
		});
	}

	destroy() {
		if (this._resizeHandler) {
			this.webix.eventRemove(this._resizeHandler);
			delete this._resizeHandler;
		}
	}

	Hide() {
		// [IMPROVE] probably we need a more sane API to hide the popup
		this.show("../");
	}
}