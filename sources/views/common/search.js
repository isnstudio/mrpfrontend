import {JetView} from "webix-jet";

export default class PagerView extends JetView {
	config() {
		return {
			cols: [
				{
					label: "Search",
					view: "search",
					height: 40,
					width: 700,
				},
			],
		};
	}
}
