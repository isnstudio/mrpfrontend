import {JetView} from "webix-jet";

export default class PagerView extends JetView {
	config() {
		return {
			id: "commonPager",
			view: "pager",
			template:
				" {common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
			size: 10,
			group: 5,
		};
	}
}
