import {JetView} from "webix-jet";
import Table from "../../helpers/table";

export default class DemandaManualTableView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		const datatable = {
			view: "datatable",
			id: "grid",
			localId: "data",
			select: true,
			pager: "pager",
			scroll: "xy",
			rowHeight: 40,
			columns: [
				{
					id: "id",
					header: "Folio",
					width: 50
				},
				{
					id: "reason__display_webix",
					header: "Motivo",
					adjust: true,
				},
				{
					id: "get_status_display",
					header: "Estatus",
					adjust: true,
				},
				{
					id: "created_on",
					header: "Fecha",
					adjust: true,
				},
				{
					id: "articulo__display_webix",
					header: "Artículo",
					fillspace: true
				},
				{
					id: "unidad_medida__display_webix",
					header: "UM",
					adjust: true
				},
				{
					id: "average_weekly_demand",
					header: {"text": "Cantidad", css: "right"},
					adjust: true,
					css: "right"
				},
			],
			on: {
				onAfterRender() {
					this.$scope.TableHelper.afterRender(this, true);
				},
				onAfterSelect: id => this.app.config.state.selected_demanda_manual = id
			}
		};


		return {
			rows: [
				datatable,
				{
					view: "pager",
					id: "pager",
					size: 20,
					group: 20,
					count: 20,
					template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
				},
			],
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}
}