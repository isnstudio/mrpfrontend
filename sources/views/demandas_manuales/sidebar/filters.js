import {JetView} from "webix-jet";
import Filters from "../../../helpers/filters";
import "webix/text_field";
import "webix/server_autocomplete";
import "webix/combo_field";

export default class FilterListView extends JetView {
	config() {
		this.Filters = new Filters(this.app);

		const form = {
			view: "form",
			localId: "filterform",
			id: "filter_form",
			autoheight: true,
			rows: [
				{
					view: "text_field",
					name: "search",
					label: "Consulta",
					placeholder: "Consulta"
				},
				{
					view: "combo_field",
					name: "reason",
					label: "Motivo",
					suggest: {url: this.app.MasterUrl + "api/mrp/manual-demand-reason/select_list/"}
				},
				{
					view: "server_autocomplete",
					name: "articulo",
					label: "Artículo",
					url: this.app.MasterUrl + "api/mrp/articulo/select_list/"
				},
				{
					view: "checkbox",
					name: "es_reorden",
					id: "input_es_reorden",
					label: "Reorden",
					labelPosition: "top"
				},
				{
					view: "button",
					value: "submit",
					label: "Buscar",
					type: "icon", icon: "mdi mdi-filter",
					height: 40,
					click: () => {
						this.Filters.filterParams(this, "filters_demandas_manuales");
					}
				},
				{
					id: "cleanfilter",
					view: "button",
					value: "submit",
					label: "Limpiar filtro",
					type: "icon",
					icon: "mdi mdi-sync",
					height: 40,
					click: () => {
						this.Filters.cleanFilter(this, "filters_demandas_manuales");
						cleanFilterAfter();
					}
				},
				{}
			]
		};

		return {
			view: "accordionitem",
			id: "form_header",
			template: "Filtros",
			header: "Filtros",
			type: "header",
			collapsed: false,
			body: form,
		};

		function cleanFilterAfter() {
			setTimeout(function(){
				this.$$("input_es_reorden").setValue(1);
			},100);
		}
	}

	urlChange() {
		var filters = this.app.config.state.filters_demandas_manuales;
		if(!("es_reorden" in this.app.config.state.filters_demandas_manuales))
			filters = {es_reorden: true};
		this.$$("filter_form").setValues(filters);
	}
}