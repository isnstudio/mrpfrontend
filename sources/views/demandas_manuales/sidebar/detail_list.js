import {JetView} from "webix-jet";

export default class DemandaManualWDetailView extends JetView {
	config() {
		const details = {
			rows: [
				{
					view: "property",
					id: "detail_info",
					disabled: true,
					autoheight: true,
					nameWidth: 140,
					elements: [
						{label: "Folio", type: "text", id: "id"},
						{label: "Motivo", type: "text", id: "reason__display_webix"},
						{label: "Estatus", type: "text", id: "get_status_display"},
						{label: "Fecha", type: "text", id: "created_on"},
						{label: "Artículo", type: "text", id: "articulo__display_webix"},
						{label: "UM", type: "text", id: "unidad_medida__display_webix"},
						{label: "Cantidad", type: "text", id: "average_weekly_demand"}
					]
				},
				{
					localId: "detail_components",
					autoheight: true,
					css: "wbackground",
					rows: []
				},
				{
					localId: "spacer",
					css: "template-scroll-y",
					borderless: true,
				}
			]
		};

		if (this.app.Mobile)
			return details;

		return {
			view: "accordionitem",
			template: "Detalle",
			header: "Detalle",
			type: "header",
			id: "detalle_header",
			collapsed: false,
			hidden: true,
			body: details
		};
	}

	showDetail(id) {
		this.demanda_manual_id = id ? id : null;
		const content = this.$$("detail_components");
		if (content) {
			let childs = content.getChildViews();
			let child_ids = [];
			for (let i = 0; i < childs.length; i++) {
				let child_id = childs[i].config.id;
				child_ids.push(child_id);
			}
			for (let childIdsKey in child_ids) {
				content.removeView(child_ids[childIdsKey]);
			}
		}
		if(this.demanda_manual_id && this.getRoot()) {
			this.showContents(this.demanda_manual_id);
		}
	}

	hideContents() {
		if(!this.app.Mobile) {
			this.$$("detalle_header").hide();
			this.$$("detail_components").hide();
			this.$$("detail_info").hide();
		}
		this.RefreshDetails(undefined);
	}

	showContents(id) {
		if(!this.app.Mobile) {
			this.$$("detalle_header").show();
			this.$$("detail_components").show();
			this.$$("detail_info").show();
		}
		this.RefreshDetails(id);
	}

	ready() {
		this.on(this.app.config.state.$changes, "selected_demanda_manual", id => {
			if (id) {
				this.showDetail(id);
			} else {
				this.hideContents();
			}
		});
	}

	RefreshDetails(id) {
		if (id) {
			var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/manual-demand/" + id + "/wdetails/");
			const json_response = JSON.parse(xhr.response);
			if ("actions_detail" in json_response && "id" in json_response) {
				const content = this.$$("detail_components");
				if (content) {
					for (const action_key in json_response["actions_detail"]) {
						const action = json_response["actions_detail"][action_key];
						let url = "/home/demandas_manuales.actions." + action["action"] + "/" + json_response["id"];
						if (action["action"] === "details") {
							url = "/home/demandas_manuales." + action["action"] + "/" + json_response["id"];
						}
						content.addView({
							view: "button",
							label: action["label"],
							height: 40,
							click: () => {
								this.show(url);
							}
						}, 0);
					}
				}
			}
			if ("id" in json_response) {
				const sets = this.$$("detail_info");
				if (sets) {
					sets.setValues(json_response);
					this._parent.Filters.$$("form_header").collapse();
				}
			}
		}
	}
}

