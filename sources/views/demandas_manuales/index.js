import {JetView} from "webix-jet";

import TableView from "./table";
import ListView from "./responsive/table";
import FilterListView from "./sidebar/filters";
import DemandasManualesWDetailView from "./sidebar/detail_list";
import Table from "../../helpers/table";
import "webix/breadcrumb";

export default class DemandasManualesView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		this.Table = new (this.app.Mobile ? ListView : TableView)(this.app);
		this.Filters = new FilterListView(this.app);
		if (!this.app.Mobile)
			this.UnitsDetailView = new DemandasManualesWDetailView(this.app);

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Demandas manuales"},
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb,
				{
					view: "icon",
					icon: "mdi mdi-plus",
					localId: "addButton",
					click: () => {
						this.show("demandas_manuales.actions.add");
					}
				},
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					{
						view: "accordion",
						rows: [
							this.Filters,
							search,
							this.Table,
							{$subview: true, popup: true}
						]
					}
				]
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						this.Table,
					]
				},
				{
					width: 384,
					view: "accordion",
					rows: [
						this.Filters,
						this.UnitsDetailView
					]
				},
			]
		};
	}

	ready(view) {
		const datagrid = view.$scope.Table.$$("data");
		this.TableHelper.pagerParams(view, datagrid,"filters_demandas_manuales", this);

		if(this.app.Mobile) {
			this.Filters.$$("form_header").collapse();
		} else this.Filters.$$("form_header").expand();

		this.on(this.app.config.state.$changes, "filters_demandas_manuales",  () => {
			this.dataLoading();
		});
	}

	dataLoading() {
		const url = this.app.MasterUrl + "api/mrp/manual-demand/wlist/?order_by=-id";
		const table = this.Table.$$("data");
		const filters = this.Filters;
		let selected_demanda_manual = this.app.config.state.selected_demanda_manual;
		let filter_default = "";
		if(!("es_reorden" in this.app.config.state.filters_demandas_manuales))
			filter_default = "es_reorden==1;";
		this.TableHelper.dataLoading(table, url, this, filters, selected_demanda_manual, "filters_demandas_manuales", filter_default);
	}
}
