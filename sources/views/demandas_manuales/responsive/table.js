import {JetView} from "webix-jet";

export default class ListView extends JetView {
	config() {
		return {
			rows: [
				{
					view: "list",
					localId: "data",
					css: "multi-line-box",
					select: true,
					pager: "pager",
					type: {
						height: "auto",
						template: obj =>
							`
								<span style="float:right; text-transform: uppercase"><b>${obj.id}</b></span>
								<span style="font-weight: 500;">${obj.nombre}</span>
								<br>
								<span style="font-weight: 500;"><b>Tipo</b>: ${obj.tipo}</span>
								<br>
								<span style="font-weight: 500;"><b>Sistema</b>: ${obj.sistema}</span>
								<br>
								<span style="font-weight: 500;"><b>Factor</b>: ${obj.factor}</span>`
					},
					on: {
						onItemClick: (id) => {
							this.show("/home/components/components.popups.details/" + id);
						}
					}
				},
				{
					view: "pager",
					id: "pager",
					size: 10,
					group: 10,
					count: 10,
					template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
				}
			]
		};
	}
}