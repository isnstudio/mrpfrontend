import ComponenteWDetailView from "../sidebar/detail_list";
import ModalView from "../../common/modal";

export default class DetailsPopupView extends ModalView {
	constructor(app) {
		super(app, {
			header: {
				view: "toolbar", height: 56, padding: {left: 6, right: 14},
				cols: [
					{view: "icon", icon: "wxi-close", click: () => this.Hide()},
					{view: "label", id: "folio", css: "details_popup_header_label"},
				]
			},
			body: ComponenteWDetailView
		});
	}

	init() {
		super.init();
		this.on(this.app, "onDetailsRefresh", data => {
			this.$$("folio").setValue(data.folio);
		});
	}
}