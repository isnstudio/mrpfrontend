import {JetView, plugins} from "webix-jet";
import "webix/breadcrumb";
import "webix/button_service";

export default class DemandaManualApproveView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["id"]);
		this.demanda_manual_id = this.getParam("id");
		this.back_url = "/home/demandas_manuales.details/" + this.demanda_manual_id;

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Demandas manuales", link: "demandas_manuales"},
				{value: "Folio #" + this.demanda_manual_id, link: this.back_url},
				{value: "Aprobar"},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb
			]
		};

		const form = {
			view: "form",
			localId: "form",
			padding: 20,
			rows: [
				{
					template: "¿Estás seguro de aprobar la demanda manual?",
					type: "label",
					borderless: true,
					css: "question"
				},
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button_service",
					label: "Aprobar",
					path: "/home/demandas_manuales.details/" + this.demanda_manual_id,
					method: "put",
					message: "Demanda manual #" + this.demanda_manual_id + " aprobada",
					url: this.app.MasterUrl + "api/mrp/manual-demand/" + this.demanda_manual_id + "/approve_manual_demand/",
				},
				{
					view: "button",
					value: "Regresar",
					css: "webix_secondary",
					height: 40,
					click: () => this.show(this.back_url)
				}
			]
		};


		if (this.app.Mobile) {
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					cols: [
						{
							rows: [
								search,
								buttons,
								form,
							]
						}
					]
				}
			};
		}

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}
}