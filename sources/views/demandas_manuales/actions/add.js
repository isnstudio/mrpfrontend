import {JetView} from "webix-jet";
import DemandaManualFormFieldsView from "../fields";
import "webix/breadcrumb";
import "webix/button_service";

export default class DemandaManualAddView extends JetView {
	config() {
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Demandas manuales", link: "demandas_manuales"},
				{value: "Agregar"},
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		const buttons = {
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button_service",
					label: "Guardar",
					height: 40,
					path: "/home/demandas_manuales.details/",
					method: "post",
					message: "Demanda manual guardada",
					url:  this.app.MasterUrl + "api/mrp/manual-demand/",
				},
				{
					view: "button",
					value: "Regresar",
					css: "webix_secondary",
					height: 40,
					click: () => {
						this.show("demandas_manuales");
					}
				}
			]
		};

		const fields = DemandaManualFormFieldsView;

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 24,
			rows: [
				fields,
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					form,
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form,
							{
								localId: "content",
								css: "template-scroll-y",
								borderless: true,
							}
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header",
							},
							buttons
						]
					}
				]
			}
		};
	}
}
