import {JetView, plugins} from "webix-jet";
import MaterialFieldsView from "../fields";
import "webix/breadcrumb";
import "webix/button_service";

export default class MaterialEditView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["id"]);
		this.demanda_manual_id = this.getParam("id");
		this.back_url = "/home/demandas_manuales.details/" + this.demanda_manual_id;

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Demandas manuales", link: "demandas_manuales"},
				{value: "Folio #" + this.demanda_manual_id, link: this.back_url},
				{value: "Editar"},
			],
		};

		const search = {
			view: "toolbar",
			localId: "toolbar",
			paddingX: 10,
			height: 44,
			visibleBatch: "default",
			cols: [
				breadcrumb,
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button_service",
					label: "Guardar",
					path: "/home/demandas_manuales.details/",
					method: "put",
					message: "Demanda manual guardada",
					url: this.app.MasterUrl + "api/mrp/manual-demand/" + this.demanda_manual_id + "/",
				},
				{
					view: "button",
					value: "Regresar",
					id: "btn_return",
					css: "webix_secondary",
					height: 40,
					click: () => this.show(this.back_url)
				}
			]
		};

		this.fields = new MaterialFieldsView(this.app);

		this.form = {
			view: "form",
			localId: "form",
			id: "form",
			paddingX: 24,
			rows: [
				this.fields,
				{}
			],
		};

		if (this.app.Mobile)
			return {
				view: "scrollview",
				scroll: "y",
				body: {
					rows: [
						search,
						buttons,
						{
							view: "template",
							template: "Material",
							type: "header"
						},
						this.form
					]
				}
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						{
							view: "scrollview",
							body: this.form
						}
					]
				},
				{
					width: 384,
					rows: [
						{
							view: "template",
							template: "Acciones",
							type: "header",
						},
						buttons,
						{}
					]
				}
			]
		};
	}

	ready() {
		const form = this.$$("form");
		var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/manual-demand/" + this.demanda_manual_id + "/");
		this.material = JSON.parse(xhr.response);
		form.setValues(this.material);
	}
}