import {JetView,} from "webix-jet";
import "webix/text_field";
import "webix/datepicker_iso";
import "webix/server_autocomplete";
import "webix/textarea_field";
import "webix/combo_field";
import "webix/price_field";
import "webix/number_field";

export default class MaterialFieldsView extends JetView {
	config() {
		const motivo = {
			view: "combo_field",
			name: "reason",
			label: "Motivo",
			suggest: {url: this.app.MasterUrl + "api/mrp/manual-demand-reason/select_list/"},
			required: true
		};

		const fecha_inicio = {
			view: "datepicker_iso",
			name: "start_date",
			id: "input_start_date",
			label: "Fecha inicio",
			labelPosition: "top",
			placeholder: "Fecha inicio",
			required: true
		};

		const fecha_fin = {
			view: "datepicker_iso",
			name: "end_date",
			id: "input_end_date",
			label: "Fecha fin",
			labelPosition: "top",
			placeholder: "Fecha fin",
			required: true
		};

		const articulo = {
			view: "server_autocomplete",
			name: "articulo",
			label: "Artículo",
			url: this.app.MasterUrl + "api/mrp/articulo/select_list/",
			required: true
		};

		const um = {
			view: "server_autocomplete",
			name: "unidad_medida",
			label: "Unidad de medida",
			url: this.app.MasterUrl + "api/cat/unidad-medida/select_list/",
			hidden: true
		};

		const cantidad = {
			view: "number_field",
			name: "average_weekly_demand",
			label: "Cantidad",
		};

		const nota = {
			view: "text_field",
			name: "nota",
			label: "Nota",
		};

		const responsive_view = {
			margin: 10,
			rows: [
				motivo,
				fecha_inicio,
				fecha_fin,
				articulo,
				um,
				cantidad,
				nota
			]
		};

		const standard_view = {
			margin: 10,
			rows: [
				{
					cols: [
						{
							margin: 10,
							rows: [
								motivo,
								fecha_inicio,
								fecha_fin
							]
						},
						{
							margin: 10,
							rows: [
								articulo,
								um,
								cantidad,
								nota
							]
						}
					]
				},
			],
		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);
		return {
			margin: 10,
			rows: [
				main_section,
			]
		};
	}
}
