import {JetView, plugins} from "webix-jet";
import CreatePurchaseFieldsView from "./fields";
import "webix/breadcrumb";
import "webix/button_service";

export default class RequisicionMaterialCreatePurchaceOrdersView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["query_string"]);
		this.query_string = this.getParam("query_string");
		this.use(plugins.UrlParam, ["ids"]);
		this.back_url = "/home/requisiciones_material.actions.administrar_requisiciones_material/";
		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Requisiciones de material", link: "requisiciones_material"},
				{value: "Administrar requisiciones de material", link: this.back_url},
				{value: "Generar ordenes de compra"}
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/material-requisition/spreadsheet-list/", {filter: this.query_string});
		this.data = JSON.parse(xhr.response);

		const buttons = {
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button_service",
					label: "Guardar",
					height: 40,
					path: "/home/requisiciones_material/",
					method: "post",
					message: "Ordenes de compra creadas",
					url:  this.app.MasterUrl + "api/mrp/material-requisition/create-purchase-orders/",
					new_values: {
						data: this.data.data
					}
				},
				{
					view: "button",
					value: "Regresar",
					css: "webix_secondary",
					height: 40,
					click: () => {
						this.show(this.back_url + this.query_string);
					}
				}
			]
		};

		this.Fields = new CreatePurchaseFieldsView(this.app);

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 24,
			rows: [
				this.Fields
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					form
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							form
						]
					},
					{
						width: 384,
						rows: [
							{
								view: "template",
								template: "Acciones",
								type: "header"
							},
							buttons
						]
					}
				]
			}
		};
	}
}