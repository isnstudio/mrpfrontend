import {JetView, plugins,} from "webix-jet";
import "webix/server_autocomplete";
import "webix/multi_server_autocomplete";
import "webix/text_field";
import Errors from "../../../../helpers/errors";

export default class CreatePurchaseFieldsView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["query_string"]);
		this.query_string = this.getParam("query_string");
		const query_string = this.query_string.split(";");
		var new_query_string = "";
		query_string.forEach(function (q, index) {
			const values = q.split("==");
			if(values[0] !== ""){
				if(index === 0)
					new_query_string += "?" + values[0] + "=" + values[1];
				else new_query_string += "&" + values[0] + "=" + values[1];
			}
		});
		var url = "api/cxp/orden-compra/init/";

		var xhr_init = webix.ajax().sync().get(this.app.MasterUrl + url + new_query_string);
		if(xhr_init.status === 200){
			this.init_data = JSON.parse(xhr_init.response);
		}
		else {
			const error = new Errors();
			error.show_error(xhr_init);
			history.back();
		}

		if(this.init_data)
			Object.keys(this.init_data).forEach(k => {
				var label = k.replace("_", " ");
				label = label[0].toUpperCase() + label.slice(1);
				if(this.init_data[k].length) {
					if(k === "tipo_impuestos")
						this[k] = {
							view: "multicombo",
							name: k + "_ids",
							label: label,
							labelPosition: "top",
							id: "input_" + k,
							options: this.init_data[k]
						};
					else this[k] = {
						view: "combo",
						name: k + "_id",
						label: label,
						labelPosition: "top",
						id: "input_" + k,
						options: this.init_data[k]
					};
				}
				else if(typeof (this.init_data[k]) === "boolean")
					this[k] = {
						view: "checkbox",
						name: k,
						labelPosition: "top",
						label: label,
						id: "input_" + k,
						options: this.init_data[k]
					};
			});

		return {
			margin: 10,
			id: "main_view",
			rows: []
		};
	}

	ready() {
		const that = this;
		if(this.init_data)
			Object.keys(this.init_data).forEach(k => {
				that.$$("main_view").addView(that[k]);
			});
	}
}