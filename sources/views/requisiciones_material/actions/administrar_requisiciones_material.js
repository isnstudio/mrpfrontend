import {JetView, plugins} from "webix-jet";
import SaveService from "../../../services/save";
import "webix/breadcrumb";
import "webix/button_service";
import "webix/server_autocomplete";

const COLUMN_ACTION = 6;
const COLUMN_COSTO = 7;
const COLUMN_MONEDA = 8;
const COLUMN_FECHA_REQUI = 10;
const COLUMN_FECHA_LLEGADA = 11;
const COLUMN_PLAZA = 9;

const COLUMN_CANTIDAD = 5;

export default class RequisicionMaterialAdministrarView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["query_string"]);
		this.query_string = this.getParam("query_string");
		this.back_url = "/home/requisiciones_material";

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Requisiciones de material", link: this.back_url},
				{value: "Administrar requisiciones de material"},
			],
		};

		var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/material-requisition/spreadsheet-list/",
			{filter: this.query_string});
		this.data = JSON.parse(xhr.response);
		this.master_url = this.app.MasterUrl;

		var xhr_actions = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/material-requisition/choice_field/orden_compra_accion/");
		this.acciones = JSON.parse(xhr_actions.response);

		var xhr_moneda = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/lead-time/choice_field/moneda/");
		this.monedas = JSON.parse(xhr_moneda.response);

		var xhr_plaza = webix.ajax().sync().get(this.app.MasterUrl + "api/cat/plaza/select_list/");
		this.plazas = JSON.parse(xhr_plaza.response);

		const that = this;
		function getPlazaParsed(plaza) {
			if(plaza === "-")
				return "";
			const found = that.plazas.find(o => o.value === plaza);
			return found.id;
		}

		function getMonedaParsed(moneda) {
			if(moneda === "-")
				return "";
			const found = that.monedas.find(o => o.value === moneda);
			return found.id;
		}

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};
		const data = this.data.data;
		const row = data[data.length - 1][0];
		const col = data[data.length - 1][1];
		var base_data = {
			data: data,
			"sizes": [
				[0,2,550],
				[0,3,280],
				[0,4,160],
				[0,5,160],
				[0,6,160],
				[0,9,270],
				[0,10,170],
				[0,11,190],
				[0,12,190],
			],
		};

		const spreadsheet = {
			view: "spreadsheet",
			id: "ssheet",
			data: base_data,
			columnCount: col,
			rowCount: row,
			toolbar: [
				{
					rows: [{ $button: "excel-export" }],
				},
				{view: "ssheet-separator"},
				{
					cols:[
						{
							view: "button",
							label: "Generar ordenes de compra",
							width: 250,
							click: () => {
								this.show("requisiciones_material.actions.create_purchase_orders/" + this.query_string);
							}
						},
					]
				},
			],
			on:{
				onChange: function(mode){
					switch (mode) {
						case "update":
							const data = this.serialize().data;
							data.forEach(function (row) {
								if (row["0"] !== 1) {
									if(row["1"] === COLUMN_PLAZA)
										row["2"] = getPlazaParsed(row["2"]);
									if(row["1"] === COLUMN_MONEDA)
										row["2"] = getMonedaParsed(row["2"]);
								}
							});
							const request_options = {
								url: this.$scope.master_url + "api/mrp/material-requisition/spreadsheet-update/",
								get_values: {data: data},
								validate: true,
								message: "guardado",
								context: this.$scope,
								path: "",
								method: "put",
								prevent_reload: true
							};
							this.SaveService = new SaveService(this.app);
							this.SaveService.save(request_options);
							break;
					}
				},
				onBeforeValueChange: function (row, column, value) {
					if(column === COLUMN_ACTION)
						if(this.$scope.accion_en_acciones(value))
							return value;

					if(column === COLUMN_MONEDA) {
						return value;
					}

					if(column === COLUMN_COSTO) {
						if(!isNaN(value))
							return value;
					}

					if(column === COLUMN_CANTIDAD) {
						if(!isNaN(value))
							return value;
					}

					if(column === COLUMN_PLAZA) {
						return value;
					}
					return false;
				}
			}
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					spreadsheet
				]
			};

		return {
			view: "scrollview",
			scroll: "y",
			body: {
				type: "space",
				cols: [
					{
						rows: [
							search,
							spreadsheet
						]
					},
				]
			}
		};
	}

	ready() {
		var text_highlight = document.getElementsByClassName("webix_text_highlight_value");
		if (text_highlight.length) {
			text_highlight[0].style.zIndex = 2;
		}
		const last_row = this.data.data[this.data.data.length - 1][0];
		const last_col = this.data.data[this.data.data.length - 1][1];
		this.$$("ssheet").lockCell({row:1, column:1}, {row: last_row, column:COLUMN_ACTION - 1}, true);
		this.$$("ssheet").lockCell({row:1, column:COLUMN_FECHA_REQUI}, {row: last_row, column:COLUMN_FECHA_REQUI});
		this.$$("ssheet").lockCell({row:1, column:COLUMN_FECHA_LLEGADA}, {row: last_row, column:COLUMN_FECHA_LLEGADA});
		this.$$("ssheet").lockCell({row:1, column:1}, {row: 1, column:last_col});

		this.$$("ssheet").lockCell({row:1, column:last_col}, {row: last_row, column:last_col}, true);
		this.$$("ssheet").lockCell(1, 2, false);
		this.$$("ssheet").lockCell(1, 3, false);

		const options_plaza = [];
		this.plazas.forEach(o => {
			options_plaza.push(o.value);
		});

		const options_moneda = [];
		this.monedas.forEach(o => {
			options_moneda.push(o.value);
		});

		console.log(options_moneda);

		for (let i = 2; i < last_row + 1; i++) {
			this.$$("ssheet").setCellEditor(i, COLUMN_ACTION, { editor:"ss_richselect", options:this.acciones});
			this.$$("ssheet").setCellEditor(i, COLUMN_MONEDA, { editor:"ss_richselect", options:options_moneda});
			this.$$("ssheet").setCellEditor(i, COLUMN_PLAZA, { editor:"ss_richselect", options:options_plaza});
		}

		this.$$("ssheet").lockCell({row: 2, column: COLUMN_CANTIDAD}, {row: last_row, column: COLUMN_CANTIDAD}, false);

		this.$$("ssheet").setCellFilter(1,2, "B2:B" + last_row);
		this.$$("ssheet").setCellFilter(1,3, "C2:C" + last_row);
	}

	accion_en_acciones (accion_id) {
		return this.acciones.some(accion => accion.id === accion_id);
	}
}