import {JetView,} from "webix-jet";
import "webix/text_field";
import "webix/datepicker_iso";
import "webix/server_autocomplete";
import "webix/textarea_field";
import "webix/combo_field";
import "webix/price_field";
import "webix/decimal_field";

export default class MaterialFieldsView extends JetView {
	config() {
		const motivo = {
			view: "combo_field",
			name: "reason",
			label: "Motivo",
			suggest: {url: this.app.MasterUrl + "api/mrp/manual-demand-reason/select_list/"},
			required: true
		};

		const proveedor = {
			view: "server_autocomplete",
			name: "proveedor",
			label: "Proveedor",
			url: this.app.MasterUrl + "api/cxp/proveedor/select_list/",
			required: true
		};

		const fecha_inicio = {
			view: "datepicker_iso",
			name: "start_date",
			id: "input_start_date",
			label: "Fecha inicio",
			labelPosition: "top",
			placeholder: "Fecha inicio",
			required: true
		};

		const fecha_fin = {
			view: "datepicker_iso",
			name: "end_date",
			id: "input_end_date",
			label: "Fecha fin",
			labelPosition: "top",
			placeholder: "Fecha fin",
			required: true
		};

		const articulo = {
			view: "server_autocomplete",
			name: "articulo",
			label: "Artículo",
			url: this.app.MasterUrl + "api/mrp/articulo/select_list/",
			required: true
		};

		const um = {
			view: "server_autocomplete",
			name: "unidad_medida",
			label: "Unidad de medida",
			url: this.app.MasterUrl + "api/cat/unidad-medida/select_list/",
			hidden: true
		};

		const cantidad = {
			view: "decimal_field",
			name: "average_weekly_demand",
			label: "Cantidad",
		};

		const comprador_asignado = {
			view: "server_autocomplete",
			name: "assigned_buyer",
			label: "Comprador asignado",
			url: this.app.MasterUrl + "api/cat/user/select_list/",
			required: true
		};

		const costo_aprox = {
			view: "price_field",
			name: "approx_cost_me",
			label: "Costo aproximado",
		};

		const responsive_view = {
			margin: 10,
			rows: [
				motivo,
				proveedor,
				comprador_asignado,
				fecha_inicio,
				fecha_fin,
				costo_aprox,
				articulo,
				um,
				cantidad,
			]
		};

		const standard_view = {
			margin: 10,
			rows: [
				{
					cols: [
						{
							margin: 10,
							rows: [
								motivo,
								proveedor,
								comprador_asignado,
								fecha_inicio,
							]
						},
						{
							margin: 10,
							rows: [
								costo_aprox,
								articulo,
								um,
								cantidad,
								fecha_fin,
							]
						}
					]
				},
			],
		};

		const main_section = (this.app.Mobile ? responsive_view : standard_view);
		return {
			margin: 10,
			rows: [
				main_section,
			]
		};
	}
}
