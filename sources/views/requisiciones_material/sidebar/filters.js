import {JetView} from "webix-jet";
import Filters from "../../../helpers/filters";
import "webix/text_field";
import "webix/datepicker_iso";
import "webix/server_autocomplete";
import "webix/combo_field";
import "webix/choice_field";

const DEFAULT_STATUS = "1";

export default class FilterListView extends JetView {
	config() {
		this.Filters = new Filters(this.app);

		const form = {
			view: "form",
			localId: "filterform",
			id: "filter_form",
			autoheight: true,
			rows: [
				{
					view: "datepicker_iso",
					name: "fecha_hasta",
					label: "Fecha hasta",
				},
				{
					view: "server_autocomplete",
					name: "buyer",
					label: "Comprador",
					placeholder: "Comprador",
					url: this.app.MasterUrl + "api/cat/user/select_list/",
				},
				{
					view: "server_autocomplete",
					name: "articulo",
					label: "Artículo",
					url: this.app.MasterUrl + "api/mrp/articulo/select_list/"
				},
				{
					view: "server_autocomplete",
					name: "proveedor",
					label: "Proveedor",
					url: this.app.MasterUrl + "api/cxp/proveedor/select_list/",
				},
				{
					view: "choice_field",
					name: "status",
					label: "Estatus",
					labelPosition: "top",
					placeholder: "Estatus",
					suggest: {url: this.app.MasterUrl + "api/mrp/material-requisition/choice_field/status/"},
					value: DEFAULT_STATUS
				},
				{
					view: "checkbox",
					name: "local",
					id: "input_local",
					label: "¿Es Local?"
				},
				{
					view: "server_autocomplete",
					name: "interfaz_externa",
					label: "Intercompañia",
					url: this.app.MasterUrl + "api/mrp/interfaz-externa/select_list/"
				},
				{
					view: "button",
					value: "submit",
					label: "Buscar",
					type: "icon", icon: "mdi mdi-filter",
					height: 40,
					click: () => {
						this.Filters.filterParams(this, "filters_requisiciones_material");
						const interfaz_externa = this.$$("input_interfaz_externa").getValue();
						const local = this.$$("input_local").getValue();
						if(!this._parent.Table.$$("data").count())
							this.$$("administrar_button").hide();
						else if(interfaz_externa !== "" || local  !== 0)this.$$("administrar_button").show();
						const buyer = this.$$("input_buyer").getText();
						if(buyer)
							this.app.config.state.filters_requisiciones_material.buyer__display_webix = buyer;
						const articulo = this.$$("input_articulo").getText();
						if(buyer)
							this.app.config.state.filters_requisiciones_material.articulo__display_webix = articulo;
					}
				},
				{
					id: "cleanfilter",
					view: "button",
					value: "submit",
					label: "Limpiar filtro",
					type: "icon",
					icon: "mdi mdi-sync",
					height: 40,
					click: () => {
						this.Filters.cleanFilter(this, "filters_requisiciones_material");
						this.$$("administrar_button").hide();
					}
				},
				{
					id: "administrar_button",
					view: "button",
					value: "submit",
					label: "Administrar requisiciones material",
					height: 40,
					hidden: true,
					click: () => {
						this.show("/home/requisiciones_material.actions.administrar_requisiciones_material/" + this.app.config.state.filters_requisiciones_material.querystring);
					}
				},
				{}
			]
		};

		return {
			view: "accordionitem",
			id: "form_header",
			template: "Filtros",
			header: "Filtros",
			type: "header",
			collapsed: false,
			body: form,
		};
	}

	ready() {
		const that = this;
		this.$$("input_local").attachEvent("onChange", function (value) {
			if(value){
				that.$$("input_interfaz_externa").hide();
				that.$$("input_interfaz_externa").setValue("");
			} else {
				that.$$("input_interfaz_externa").show();
			}
		});
	}

	urlChange() {
		if(Object.keys(this.app.config.state.filters_requisiciones_material).length)
			this.$$("administrar_button").show();
		this.$$("filter_form").setValues(this.app.config.state.filters_requisiciones_material);
	}
}