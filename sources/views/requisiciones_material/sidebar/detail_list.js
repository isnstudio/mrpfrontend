import {JetView} from "webix-jet";

export default class RequisicionesMaterialWDetailView extends JetView {
	config() {
		const details = {
			rows: [
				{
					view: "property",
					id: "detail_info",
					disabled: true,
					autoheight: true,
					nameWidth: 180,
					elements: [
						{label: "Folio", type: "text", id: "id"},
						{label: "Fecha", type: "text", id: "requisition_date"},
						{label: "Fecha llegada", type: "text", id: "arrival_date"},
						{label: "Estatus", type: "text", id: "get_status_display"},
						{label: "Prioridad", type: "text", id: "get_priority_display"},
						{label: "Proveedor", type: "text", id: "proveedor__display_webix"},
						{label: "Orden de compra", type: "text", id: "orden_compra__display_webix"},
						{label: "Comprador asignado", type: "text", id: "buyer__display_webix"},
						{label: "Estatus OC", type: "text", id: "orden_compra__estatus__display_webix"},
						{label: "Fecha confirmada", type: "checkbox", id: "oc_detalle__fecha_confirmada__display_webix"}
					]
				},
				{
					localId: "detail_components",
					autoheight: true,
					css: "wbackground",
					rows: []
				},
				{
					localId: "spacer",
					css: "template-scroll-y",
					borderless: true,
				}
			]
		};

		if (this.app.Mobile)
			return details;

		return {
			view: "accordionitem",
			template: "Detalle",
			header: "Detalle",
			type: "header",
			id: "detalle_header",
			collapsed: false,
			hidden: true,
			body: details
		};
	}

	showDetail(id) {
		this.requisicion_material_id = id ? id : null;
		const content = this.$$("detail_components");
		if (content) {
			let childs = content.getChildViews();
			let child_ids = [];
			for (let i = 0; i < childs.length; i++) {
				let child_id = childs[i].config.id;
				child_ids.push(child_id);
			}
			for (let childIdsKey in child_ids) {
				content.removeView(child_ids[childIdsKey]);
			}
		}
		if(this.requisicion_material_id && this.getRoot()) {
			this.showContents(this.requisicion_material_id);
		}
	}

	hideContents() {
		if(!this.app.Mobile) {
			this.$$("detalle_header").hide();
			this.$$("detail_components").hide();
			this.$$("detail_info").hide();
		}
		this.RefreshDetails(undefined);
	}

	showContents(id) {
		if(!this.app.Mobile) {
			this.$$("detalle_header").show();
			this.$$("detail_components").show();
			this.$$("detail_info").show();
		}
		this.RefreshDetails(id);
	}

	ready() {
		this.on(this.app.config.state.$changes, "selected_requisicion_material", id => {
			if (id) {
				this.showDetail(id);
			} else {
				this.hideContents();
			}
		});
	}

	RefreshDetails(id) {
		if (id) {
			var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/material-requisition/" + id + "/wdetails/");
			const json_response = JSON.parse(xhr.response);
			if ("actions_detail" in json_response && "id" in json_response) {
				const content = this.$$("detail_components");
				if (content) {
					for (const action_key in json_response["actions_detail"]) {
						const action = json_response["actions_detail"][action_key];
						let url = "/home/requisiciones_material.actions." + action["action"] + "/" + json_response["id"];
						if (action["action"] === "details") {
							url = "/home/requisiciones_material." + action["action"] + "/" + json_response["id"];
						}
						content.addView({
							view: "button",
							label: action["label"],
							height: 40,
							click: () => {
								this.show(url);
							}
						}, 0);
					}
				}
			}
			if ("id" in json_response) {
				const sets = this.$$("detail_info");
				if (sets) {
					sets.setValues(json_response);
					this._parent.Filters.$$("form_header").collapse();
				}
			}
		}
	}
}

