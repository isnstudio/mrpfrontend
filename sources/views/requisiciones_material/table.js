import {JetView} from "webix-jet";
import Table from "../../helpers/table";

export default class RequisicionMaterialTableView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		const datatable = {
			view: "datatable",
			id: "grid",
			localId: "data",
			select: true,
			pager: "pager",
			scroll: "xy",
			rowHeight: 40,
			columns: [
				{
					id: "id",
					header: "Folio",
					width: 80
				},
				{
					id: "articulo_clave__display_webix",
					header: "Material",
					adjust: true
				},
				{
					id: "quantity",
					header: "Cantidad",
					width: 150
				},
				{
					id: "requisition_date",
					header: "Fecha",
					adjust: true,
				},
				{
					id: "arrival_date",
					header: "Fecha llegada material",
					adjust: true,
				},
				{
					id: "exception_message",
					header: "Mensajes de excepción",
					fillspace: true,
					minWidth: 200
				},
				{
					id: "expedition_date",
					header: "Fecha de expeditación",
					adjust: true,
					minWidth: 190
				},
				{
					id: "get_status_display",
					header: "Estatus",
					fillspace: true,
					minWidth: 120
				},
				{
					id: "orden_compra__display_webix",
					header: "Orden de compra",
					adjust: true,
					minWidth: 160
				},
				{
					id: "get_priority_display",
					header: {"text": "Prioridad", css: "right"},
					adjust: true,
					css: "right"
				},
			],
			on: {
				onAfterRender() {
					this.$scope.TableHelper.afterRender(this, true);
				},
				onAfterSelect: (id) => {
					this.app.config.state.selected_requisicion_material = id;
					this._parent.UnitsDetailView.$$("detail_components").hide();
				}
			}
		};


		return {
			rows: [
				datatable,
				{
					view: "pager",
					id: "pager",
					size: 20,
					group: 20,
					count: 20,
					template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
				},
			],
		};
	}

	init() {
		const datatable = this.$$("data");
		this.webix.extend(datatable, webix.OverlayBox);
		this.webix.extend(datatable, webix.ProgressBar);
	}
}