import {JetView, plugins} from "webix-jet";
import DemandaManualFieldsView from "./fields";
import "webix/breadcrumb";

export default class DemandaManualDetailView extends JetView {
	config() {
		this.use(plugins.UrlParam, ["id"]);
		this.requisicion_material_id = this.getParam("id");
		this.back_url = "/home/requisiciones_material";

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Requisiciones de material", link: this.back_url},
				{value: "Folio #" + this.requisicion_material_id},
				{value: "Detalle"},
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		const buttons = {
			localId: "buttons",
			autoheight: true,
			css: "wbackground",
			rows: [
				{
					view: "button",
					value: "Regresar",
					css: "webix_secondary",
					height: 40,
					click: () => this.show(this.back_url)
				}
			]
		};

		this.fields = new DemandaManualFieldsView(this.app);

		const form = {
			view: "form",
			localId: "form",
			id: "form",
			padding: 24,
			rows: [
				this.fields
			],
		};

		if (this.app.Mobile)
			return {
				rows: [
					search,
					buttons,
					form,
				]
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						{
							view: "scrollview",
							scroll: "y",
							body: {
								rows: [
									form
								]
							},
						}
					],
				},
				{
					width: 384,
					rows: [
						{
							view: "template",
							template: "Acciones",
							type: "header",
						},
						buttons,
						{
							localId: "content",
							css: "template-scroll-y",
							borderless: true
						}
					]
				}
			]
		};
	}

	urlChange() {
		const form = this.$$("form");
		for (var element in form.elements) {
			form.elements[element].disable();
		}
		this.fields.$$("input_unidad_medida").show();
		var xhr = webix.ajax().sync().get(this.app.MasterUrl + "api/mrp/manual-demand/" + this.requisicion_material_id + "/");
		this.demanda_manual = JSON.parse(xhr.response);
		form.setValues(this.demanda_manual);

		const content = this.$$("buttons");
		if (content) {
			for (const action_key in this.demanda_manual["actions"]) {
				const action = this.demanda_manual["actions"][action_key];
				let url = "/home/demandas_manuales.actions." + action["action"] + "/" + this.requisicion_material_id;
				content.addView({
					view: "button",
					label: action["label"],
					height: 40,
					click: () => this.show(url)
				}, 0);
			}
		}
	}
}