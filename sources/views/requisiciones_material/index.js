import {JetView} from "webix-jet";

import TableView from "./table";
import ListView from "./responsive/table";
import FilterListView from "./sidebar/filters";
import RequisicionesMaterialWdetailView from "./sidebar/detail_list";
import Table from "../../helpers/table";
import "webix/breadcrumb";

export default class RequisicionesMaterialView extends JetView {
	config() {
		this.TableHelper = new Table(this.app);
		this.Table = new (this.app.Mobile ? ListView : TableView)(this.app);
		this.Filters = new FilterListView(this.app);
		if (!this.app.Mobile)
			this.UnitsDetailView = new RequisicionesMaterialWdetailView(this.app);

		const breadcrumb = {
			view: "breadcrumb",
			data: [
				{value: "Requisiciones de material"},
			],
		};

		const search = {
			view: "toolbar",
			paddingX: 10,
			height: 44,
			cols: [
				breadcrumb
			]
		};

		if (this.app.Mobile)
			return {
				rows: [
					{
						view: "accordion",
						rows: [
							this.Filters,
							search,
							this.Table,
							{$subview: true, popup: true}
						]
					}
				]
			};

		return {
			type: "space",
			cols: [
				{
					rows: [
						search,
						this.Table,
					]
				},
				{
					width: 384,
					view: "accordion",
					rows: [
						this.Filters,
						this.UnitsDetailView
					]
				},
			]
		};
	}

	ready(view) {
		const datagrid = view.$scope.Table.$$("data");
		this.TableHelper.pagerParams(view, datagrid,"filters_requisiciones_material", this);

		if(this.app.Mobile) {
			this.Filters.$$("form_header").collapse();
		} else this.Filters.$$("form_header").expand();

		this.on(this.app.config.state.$changes, "filters_requisiciones_material",  () => {
			this.dataLoading();
		});
	}

	dataLoading() {
		const url = this.app.MasterUrl + "api/mrp/material-requisition/wlist/?order_by=-id";
		const table = this.Table.$$("data");
		const filters = this.Filters;
		let selected_requisicion_material = this.app.config.state.selected_requisicion_material;
		this.TableHelper.dataLoading(table, url, this, filters, selected_requisicion_material, "filters_requisiciones_material");
	}
}
