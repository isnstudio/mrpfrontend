import {JetView} from "webix-jet";
import Sidebar from "./sidebar";
import AuthService from "../services/auth";

export default class HomeView extends JetView {
	config() {
		const header = {
			view: "toolbar",
			height: 56,
			css: "main_header",
			padding: {right: 12},
			cols: [
				{view: "icon", icon: "mdi mdi-menu", click: () => this.ToggleSidebar()},
				{
					view: "button",
					type: "image",
					css: "header_logo",
					image: "data/images/logo.png",
					responsive: "hide",
					// width: 175,
					click: () => {
						this.show("/home");
					}
				},
				{},
				// {
				// 	template: "<image class='mainphoto main_user_avatar' src='data/images/user.png'>",
				// 	css: "main_user_avatar_template",
				// 	width: 50,
				// 	borderless: true,
				// 	localId: "avatar",
				// },
				{
					cols: [
						{
							view: "label",
							label: "MRP",
							width: 300,
							css: "main_user_name_label right",
						},
						{
							view: "label", height: 18,
							label: VERSION,
							borderless: true,
							width: 75,
							css: "main_user_name_label right",
							popup: "menu",
						}
					]
				},
			]
		};

		const header_responsive = {
			view: "toolbar",
			css: "main_header",
			cols: [
				{
					cols: [
						{view: "icon", icon: "mdi mdi-menu", click: () => this.ToggleSidebar()},
						{
							view: "button",
							type: "image",
							css: "navlogo",
							image: "data/images/logo.png",
							responsive: "hide",
							width: 175,
							click: () => {
								this.show("/home");
							}
						},
						{
							view: "label",
							height: 18,
							label: VERSION,
							popup: "menu",
							borderless: true,
							css: "main_user_name_label right",
						}
					],
				},
			]
		};


		if (this.app.Mobile)
			return {rows: [header_responsive, {$subview: true}]};

		return {
			rows: [
				header,
				{
					cols: [
						Sidebar,
						{$subview: true}
					]
				}
			]
		};
	}

	init() {
		if (!$$("list") || !$$("menu")) {
			webix.ui({
				view: "popup",
				id: "menu",
				width: 200,
				body: {
					view: "list",
					id: "list",
					data: [
						{id: "logout", name: "Cerrar sesión"}
					],
					template: "#name#",
					autoheight: true
				}
			});
		}

		if(!$$("webix:debugmenu").data.order.includes("add_hint"))
			$$("webix:debugmenu").data.add({
				id: "add_hint",
				value: "Add Hint"
			});

		if(!$$("webix:debugmenu").data.order.includes("start_hints"))
			$$("webix:debugmenu").data.add({
				id: "start_hints",
				value: "Start Hints"
			});

		$$("webix:debugmenu").attachEvent("OnItemClick", function (id, ev){
			//mixing two object result in confusion
			var obj = $$(this.config.lastTarget);

			if (id === "add_hint") {
				let view_hash = ev.view.location.hash.replace("#!", "");
				let url = MASTER_URL_HINTS + "admin/cat/hint/add/?el=" + this.config.lastTarget + "&view=" + view_hash;
				window.open(url);
			}
			if (id === "start_hints") {
				let view_hash = ev.view.location.hash.replace("#!", "");
				let url = MASTER_URL_HINTS + "api/cat/hint/?view=" + view_hash;


				webix.ajax().get(url).then(function(data){
					let data_steps = data.json();
					let hint = $$("hint");
					if (hint){
						hint.destructor();
					}
					webix.ui({
						view: "hint",
						id: "hint",
						steps: data_steps
					});

					$$("hint").start();
				});
			}
		});

		$$("list").attachEvent("onItemClick", function (value) {
			const auth = new AuthService();
			if (value === "logout") {
				auth.logout();
				$$("list").hide();
			}
		});

		if (this.app.Mobile) {
			this.Sidemenu = this.ui({
				view: "sidemenu", position: "left",
				body: Sidebar
			});

			this.on(this.app, "SidebarNavigation", () => {
				if (this.Sidemenu.isVisible())
					this.Sidemenu.hide();
			});
		}
	}

	ToggleSidebar() {
		if (this.app.Mobile)
			if (this.Sidemenu.isVisible())
				this.Sidemenu.hide();
			else
				this.Sidemenu.show();
		else
			this.getRoot().queryView({view: "sidebar"}).toggle();
	}

	ready() {
		const that = this;
		webix.attachEvent("onBeforeAjax", function (mode, url, params, xhr, headers, files, defer) {
			if(that.app) {
				setTimeout(function () {
					const view =  that.getSubView().getRoot();
					const top = view.getTopParentView();
					that.webix.extend(view, webix.OverlayBox);
					that.webix.extend(view, webix.ProgressBar);
					that.webix.extend(top, webix.OverlayBox);
					that.webix.extend(top, webix.ProgressBar);
					view.showProgress();
					if(defer) {
						defer.then(function(){
							setTimeout(function (){
								view.hideProgress();
							}, 200);
						}, function(){
							setTimeout(function (){
								view.hideProgress();
							}, 200);
						});
					}
					else setTimeout(function (){
						view.hideProgress();
					}, 200);
				}, 200);
			}
		});
	}
}
