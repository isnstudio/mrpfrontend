const demandas_calculadas = new webix.DataCollection({
	data: [
		{
			"id": 1,
			"tendencia": "+1",
			"proveedor": 1,
			"costo_aprox": 75433,
			"fecha": "2022-01-12",
			"articulo": 1,
			"um": 1,
			"cantidad": 43,
			"comprador_asignado": 5,
		},
		{
			"id": 2,
			"tendencia": "+8",
			"proveedor": 2,
			"costo_aprox": 1758,
			"fecha": "2022-01-29",
			"articulo": 2,
			"um": 2,
			"cantidad": 98,
			"comprador_asignado": 4,
		},
		{
			"id": 3,
			"tendencia": "-3",
			"proveedor": 2,
			"costo_aprox": 1758,
			"fecha": "2022-01-29",
			"articulo": 2,
			"um": 2,
			"cantidad": 98,
			"comprador_asignado": 4,
		},
	]
});

export default demandas_calculadas;

export function getDemandaCalculada(id) {
	return demandas_calculadas.getItem(id);
}
