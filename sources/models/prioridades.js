const prioridades = new webix.DataCollection({
	data:[
		{"id": 1, "value": "Baja"},
		{"id": 2, "value": "Media"},
		{"id": 3, "value": "Alta"},
	]
});

export default prioridades;

export function getPrioridadValue(id) {
	return prioridades.getItem(id).value;
}
