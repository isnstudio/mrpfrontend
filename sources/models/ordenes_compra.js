const ordenes_compra = new webix.DataCollection({
	data:[
		{"id": 1, "value": "OC 1"},
		{"id": 2, "value": "OC 2"},
		{"id": 3, "value": "OC 3"},
	]
});

export default ordenes_compra;

export function getOrdenCompraValue(id) {
	return ordenes_compra.getItem(id).value;
}
