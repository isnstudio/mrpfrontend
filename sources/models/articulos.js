const articulos = new webix.DataCollection({
	data: [
		{
			"id": 1,
			"num_parte": "Lamina Industrial 4x20",
			"value": "Lamina Industrial 4x20",
			"descripcion": "Lamina galvanizada para techo rectangular en calibre 26 r101",
			"categoria": "Acanalada",
			"marca": "TERMIUM",
			"um": 1,
			"demanda_diaria": 1,
			"demanda_mensual": 3,
			"reorden": true,
			"tamano_lote": 43,
			"clase": "Crítico",
			"lead_time": 8,
			"demanda_semanal": 10,
			"ltc": 80,
			"cantidad": 40,
			"cantidad_reorden": 120,
			"nivel_maximo": 200,
			"detalles": [
				{
					"id": 1,
					"proveedor": 1,
					"aduana": "Aduana 1",
					"lead_time": 11,
					"lt1": 3,
					"lt2": 2,
					"lt3": 2,
					"lt4": 1,
					"lt5": 1,
					"lt6": 1,
					"lt7": 1,
					"cantidad": 19,
				}
			]
		},
		{
			"id": 2,
			"num_parte": "Uv Stop Aislante",
			"value": "Uv Stop Aislante",
			"descripcion": "Evita corrosión y oxidación en láminas y armaduras de acero.",
			"categoria": "Disipador de calor",
			"marca": "UV Stop",
			"um": 2,
			"demanda_diaria": 1,
			"demanda_semanal": 2,
			"demanda_mensual": 3,
			"reorden": false,
			"tamano_lote": 109,
			"clase": "No crítico",
			"cantidad": 137,
			"nivel_maximo": 183,
			"cantidad_reorden": 120,
			"ltc": 80,
			"lead_time": 8,
			"detalles": [
				{
					"id": 1,
					"proveedor": 2,
					"aduana": "Aduana 2",
					"lead_time": 11,
					"lt1": 3,
					"lt2": 2,
					"lt3": 2,
					"lt4": 1,
					"lt5": 1,
					"lt6": 1,
					"lt7": 1,
					"cantidad": 345,
				}
			]
		},
		{
			"id": 3,
			"num_parte": "MXCRA-008",
			"value": "MXCRA-008",
			"descripcion": "Aislante De Calor En Rollo Casas, Mxcra-008, 1.22m X 1m, Es",
			"categoria": "MXCRA-008-2",
			"marca": "VentDepot",
			"um": 1,
			"demanda_diaria": 1,
			"demanda_semanal": 2,
			"demanda_mensual": 3,
			"reorden": true,
			"tamano_lote": 56,
			"clase": "Crítico",
			"cantidad": 126,
			"nivel_maximo": 623,
			"cantidad_reorden": 120,
			"ltc": 80,
			"lead_time": 8,
			"detalles": [
				{
					"id": 1,
					"proveedor": 3,
					"aduana": "Aduana 3",
					"lead_time": 11,
					"lt1": 3,
					"lt2": 2,
					"lt3": 2,
					"lt4": 1,
					"lt5": 1,
					"lt6": 1,
					"lt7": 1,
					"cantidad": 567,
				}
			]
		},
	]
});

export default articulos;

export function getArticulo(id) {
	return articulos.getItem(id);
}

export function getArticuloPartNumber(id) {
	return articulos.getItem(id).num_parte;
}
