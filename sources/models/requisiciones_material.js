const requisiciones_material = new webix.DataCollection({
	data: [
		{
			"id": 1,
			"fecha": "2022-01-01",
			"fecha_llegada": "2022-01-21",
			"estatus": "Abierta",
			"material": "Lamina Industrial 4x20",
			"prioridad": 1,
			"cantidad": 10,
			"proveedor": 1,
			"orden_compra": 1,
			"comprador_asignado": 1,
			"detalles":[
				{
					"id": 1,
					"articulo": 1,
					"um": 1,
					"cantidad": 34,
				},
				{
					"id": 2,
					"articulo": 2,
					"um": 2,
					"cantidad": 76,
				}
			]
		},
		{
			"id": 2,
			"fecha": "2022-02-06",
			"fecha_llegada": "2022-03-29",
			"estatus": "Cerrada",
			"material": "Guante de nitrilo",
			"prioridad": 2,
			"cantidad": 8,
			"proveedor": 2,
			"orden_compra": 2,
			"comprador_asignado": 2,
			"detalles":[
				{
					"id": 1,
					"articulo": 2,
					"um": 2,
					"cantidad": 34,
				},
				{
					"id": 2,
					"articulo": 1,
					"um": 3,
					"cantidad": 76,
				}
			]
		},
	]
});

export default requisiciones_material;

export function getRequisicionMaterial(id) {
	return requisiciones_material.getItem(id);
}
