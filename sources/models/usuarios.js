const usuarios = new webix.DataCollection({
	data:[
		{"id": 1, "value": "Leopoldo Fuertes"},
		{"id": 2, "value": "Ibai Gabarri"},
		{"id": 3, "value": "Gracia Egea"},
		{"id": 4, "value": "Paulino Jurado"},
		{"id": 5, "value": "Elvira Monzon"},
	]
});

export default usuarios;

export function getUsuarioValue(id) {
	return usuarios.getItem(id).value;
}
