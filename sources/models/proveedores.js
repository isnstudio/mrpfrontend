const proveedores = new webix.DataCollection({
	data:[
		{"id": 1, "value": "ALPREMA S.A. de C.V."},
		{"id": 2, "value": "Climas S.A. de C.V."},
		{"id": 3, "value": "CODIFLEX, S.A. de C.V."},
	]
});

export default proveedores;

export function getProveedorValue(id) {
	return proveedores.getItem(id).value;
}
