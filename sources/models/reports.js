const reports = new webix.DataCollection({
	data:[
		{
			"id": 1,
			"nombre": "Backorder material",
			"descripcion": "Pending material on material requisitions",
		},
		{
			"id": 2,
			"nombre": "Stock levels",
			"descripcion": "Materials compared to minimum and maximum levels",
		},
		{
			"id": 3,
			"nombre": "Stock tracking",
			"descripcion": "The recording(logs) of stock levels for each material",
		},
		{
			"id": 4,
			"nombre": "Inventory",
			"descripcion": "Excel report containing stock levels(available, hold, on hand)",
		},
	]
});

export default reports;

export function getReport(id) {
	return reports.getItem(id);
}
