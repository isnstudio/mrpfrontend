const demandas_manuales = new webix.DataCollection({
	data: [
		{
			"id": 1,
			"motivo": 1,
			"estatus": "Borrador",
			"proveedor": 1,
			"fecha": "2022-01-12",
			"articulo": 1,
			"um": 1,
			"cantidad": 43,
			"comprador_asignado": 5,
			"costo_aprox": 75433,
		},
		{
			"id": 2,
			"motivo": 2,
			"estatus": "Aprobada",
			"proveedor": 3,
			"fecha": "2022-02-13",
			"articulo": 2,
			"um": 2,
			"cantidad": 98,
			"comprador_asignado": 4,
			"costo_aprox": 2973,
		},
		{
			"id": 3,
			"motivo": 3,
			"estatus": "Cancelada",
			"proveedor": 2,
			"fecha": "2022-01-29",
			"articulo": 3,
			"um": 3,
			"cantidad": 16,
			"comprador_asignado": 2,
			"costo_aprox": 1758,
		},
	]
});

export default demandas_manuales;

export function getDemandaManual(id) {
	return demandas_manuales.getItem(id);
}
