const ums = new webix.DataCollection({
	data:[
		{"id": 1, "value": "PZ"},
		{"id": 2, "value": "BOX"},
		{"id": 3, "value": "BAG"},
	]
});

export default ums;

export function getUMValue(id) {
	return ums.getItem(id).value;
}
