const motivos = new webix.DataCollection({
	data: [
		{
			"id": 1,
			"value": "Venta extraordinaria",
		},
		{
			"id": 2,
			"value": "Nuevos productos",
		},
		{
			"id": 3,
			"value": "Ajuste de promedios",
		},
	]
});

export default motivos;

export function getMotivoValue(id) {
	return motivos.getItem(id).value;
}
