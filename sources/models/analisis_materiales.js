const analisis_materiales = new webix.DataCollection({
	"data": [
    {
      "id": 1,
      "fecha": "2022-01-06T07:00:00.000Z",
      "semana": "W1",
      "tipo_movimiento": "Stock",
      "cantidad": 200,
      "balance": 200
    },
    {
      "id": 2,
      "fecha": "2022-01-06T07:00:00.000Z",
      "semana": "W1",
      "tipo_movimiento": "Demand",
      "cantidad": 10,
      "balance": 190
    },
    {
      "id": 3,
      "fecha": "2022-01-13T07:00:00.000Z",
      "semana": "W2",
      "tipo_movimiento": "Demand",
      "cantidad": 10,
      "balance": 180
    },
    {
      "id": 4,
      "fecha": "2022-01-20T07:00:00.000Z",
      "semana": "W3",
      "tipo_movimiento": "Demand",
      "cantidad": 10,
      "balance": 170
    },
    {
      "id": 5,
      "fecha": "2022-01-27T07:00:00.000Z",
      "semana": "W4",
      "tipo_movimiento": "Demand",
      "cantidad": 10,
      "balance": 160
    },
    {
      "id": 6,
      "fecha": "2022-02-04T07:00:00.000Z",
      "semana": "W5",
      "tipo_movimiento": "Demand",
      "cantidad": 10,
      "balance": 150
    },
    {
      "id": 7,
      "fecha": "2022-02-12T07:00:00.000Z",
      "semana": "W6",
      "tipo_movimiento": "Demand",
      "cantidad": 10,
      "balance": 140
    },
    {
      "id": 8,
      "fecha": "2022-02-19T07:00:00.000Z",
      "semana": "W7",
      "tipo_movimiento": "Demand",
      "cantidad": 10,
      "balance": 130
    },
    {
      "id": 9,
      "fecha": "2022-02-27T07:00:00.000Z",
      "semana": "W8",
      "tipo_movimiento": "Demand",
      "cantidad": 10,
      "balance": 120
    },
    {
      "id": 10,
      "fecha": "2022-03-06T07:00:00.000Z",
      "semana": "W9",
      "tipo_movimiento": "PR",
      "cantidad": 80,
      "balance": 120
    },
    {
      "id": 11,
      "fecha": "2022-03-06T07:00:00.000Z",
      "semana": "W9",
      "tipo_movimiento": "PO Receipt",
      "cantidad": 80,
      "balance": 200
    },
    {
      "id": 12,
      "fecha": "2022-03-06T07:00:00.000Z",
      "semana": "W9",
      "tipo_movimiento": "Demand",
      "cantidad": 10,
      "balance": 190
    },
    {
      "id": 13,
      "fecha": "2022-03-13T07:00:00.000Z",
      "semana": "W10",
      "tipo_movimiento": "Demand",
      "cantidad": 10,
      "balance": 180
    }
  ]
});

export default analisis_materiales;

export function getAnalisisArticulo(id) {
	return analisis_materiales.getItem(id);
}
