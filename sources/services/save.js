import Errors from "../helpers/errors";

export default class SaveService {
	save(request_options) {
		var prevent_reload = false;
		if(("prevent_reload" in request_options))
			prevent_reload = request_options.prevent_reload;
		if (request_options.validate) {
			if(request_options.button) {
				webix.extend(request_options.context.$$("btn_save"), webix.ProgressBar);
				request_options.button.disable();
			}
			const ajax = request_options.method === "post" ?
				webix.ajax().headers({
					"Content-type":"application/json"
				}).post(request_options.url, request_options.get_values) :
				webix.ajax().headers({
					"Content-type":"application/json"
				}).put(request_options.url, request_options.get_values);
			ajax.then(function (response) {
				const json_response = response.json();
				if(request_options.path)
					if(json_response && json_response.id && request_options.show_detail === true)
						request_options.context.show(request_options.path + json_response.id);
					else
						request_options.context.show(request_options.path);
				else if(!prevent_reload)
					request_options.context.refresh();
				webix.message(request_options.message);
			}).fail(function (res) {
				const error = new Errors();
				error.show_error(res);
			}).finally(function () {
				if(request_options.button) {
					request_options.button.enable();
				}
				return true;
			});
		} else {
			webix.message({
				text: "Faltan campos por llenar",
				type: "error",
				expire: -1,
				id: "messageLead"
			});
		}
	}

	save_sync(request_options) {
		var prevent_reload = false;
		if(("prevent_reload" in request_options))
			prevent_reload = request_options.prevent_reload;
		if (request_options.validate) {
			if(request_options.button) {
				webix.extend(request_options.context.$$("btn_save"), webix.ProgressBar);
				request_options.button.disable();
			}
			const ajax_sync = request_options.method === "post" ?
				webix.ajax().headers({
					"Content-type":"application/json"
				}).sync().post(request_options.url, request_options.get_values) :
				webix.ajax().headers({
					"Content-type":"application/json"
				}).sync().put(request_options.url, request_options.get_values);

			if(ajax_sync.status === 200) {
				const json_response = JSON.parse(ajax_sync.response);
				if(request_options.path)
					if(json_response && json_response.id && request_options.show_detail === true)
						request_options.context.show(request_options.path + json_response.id);
					else
						request_options.context.show(request_options.path);
				else if(!prevent_reload)
					request_options.context.refresh();
				webix.message(request_options.message);
			} else {
				const error = new Errors();
				error.show_error(ajax_sync);
			}

			if(request_options.button) {
				request_options.button.enable();
			}
		} else {
			webix.message({
				text: "Faltan campos por llenar",
				type: "error",
				expire: -1,
				id: "messageLead"
			});
		}
	}
}