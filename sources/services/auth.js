export default class AuthService {
	constructor(rights, user, context) {
		this._rights = rights;
		this._context = context;

		this.setRights(user);
	}

	setRights(user) {
		this.rights = user;
		this._allowed = {};
		if (this.rights) {
			this.rights.forEach(a => this._allowed[a] = true);
		} else if(!this.rights) {
			const rights = webix.storage.local.get("Rights");
			this.rights = rights ? rights : [""];
			this.rights.forEach(a => this._allowed[a] = true);
		}
		return this.rights;
	}

	getRights() {
		const res = [];
		for (var name in this._rights)
			res.push({id: this._rights[name], name});
		return res;
	}

	getUser() {
		return this.user;
	}

	login(username, password) {
		const that = this;
		return new Promise(resolve => {
			var xhr = webix.ajax().sync().post(this._context.MasterUrl + "api-token-auth/", {username, password}, function (res) {
				res = JSON.parse(res);
				if (res && res.token) {
					webix.storage.local.put("Token", res.token);
					webix.attachEvent("onBeforeAjax", function (mode, url, params, x, headers) {
						headers["Authorization"] = "Token " + res.token;
					});

					webix.storage.local.put("Rights", res.rights);
					that.setRights(res.rights);

					resolve("resolved");
				}
			});
			if(xhr.status !== 200)
				throw new TypeError("IncorrectLogin");
		});
	}



	register(name, email, pass) {
		return webix.ajax().post("/auth/register", {name, email, pass})
			.then(res => {
				res = res.json();
				if (res && res.user)
					this.setRights(res.user);

				return this._reset();
			})
			.then(() => this.getUser());
	}

	logout() {
		webix.attachEvent("onBeforeAjax", function (mode, url, params, x, headers) {
			headers["Authorization"] = "";
		});
		webix.storage.local.clear();
		window.location.replace("/#!/login");
	}

	hasAccess(right) {
		const code = this._rights[right];
		if (!code)
			throw "Unknow access right name: " + right;

		return !!this._allowed[code];
	}

	guardAccess(right) {
		if (!this.hasRight(right)) throw "Access denied";
	}
}