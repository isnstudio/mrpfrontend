import Errors from "../helpers/errors";

export default class RemoveService {
	remove(request_options) {
		if (request_options.validate) {
			if(request_options.button) {
				request_options.button.disable();
			}
			const ajax = webix.ajax().del(request_options.url);
			ajax.then(function () {
				webix.message(request_options.message);
				request_options.context.show(request_options.path);
			}).fail(function (res) {
				const error = new Errors();
				error.show_error(res);
			}).finally(function () {
				if(request_options.button) {
					request_options.button.enable();
				}
			});
		} else {
			webix.message({
				text: "Faltan campos por llenar",
				type: "error",
				expire: -1,
				id: "messageLead"
			});
		}
	}
}