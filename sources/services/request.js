import Errors from "../helpers/errors";

export default class RequestService {
	request(request_options) {
		if(request_options.button) {
			webix.extend(request_options.context.$$("request"), webix.ProgressBar);
			request_options.button.disable();
		}
		request_options.ajax.then(function () {
			webix.message(request_options.message);
			if(request_options.path)
				request_options.context.show(request_options.path);
		}).fail(function (res) {
			const error = new Errors();
			error.show_error(res);
		}).finally(function () {
			if(request_options.button) {
				request_options.button.enable();
			}
		});
	}
}