# FROM ubuntu:18.04
FROM nginx:1.20

# Replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# make sure apt is up to date
RUN apt-get update --fix-missing
RUN apt-get install -y curl build-essential libssl-dev rsync

RUN mkdir -p /usr/local/nvm

ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 14.17.0

# Install nvm with node and npm
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash \
    && source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

RUN mkdir /code/
WORKDIR /code/
COPY . /code/
ARG NPM_TOKEN  
ARG MASTER_URL
COPY .npmrc .npmrc 

RUN npm config set strict-ssl false
RUN npm install
RUN rm -f .npmrc
RUN npm run build

RUN mkdir /webapp/
RUN mkdir /webapp/node_modules/
RUN mkdir /webapp/data/
RUN mkdir /webapp/codebase/

RUN rsync index.html /webapp/index.html
RUN rsync -avu node_modules/@xbs /webapp/node_modules/
RUN rsync -avu data/ /webapp/data/
RUN rsync -avu codebase/ /webapp/codebase/

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d
